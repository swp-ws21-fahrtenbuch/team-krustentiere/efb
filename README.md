# efb

* [Mattermost][mattermost]
* [Group Wiki][wiki]
* [KVV Page][kvv]
* [Issue Board for the current Sprint][board]

## Project Description

> In cooperation with UniSport, a digital logbook for their water sports center is to 
> be designed and developed. Since the logbook has to be easily accessible for students,
> it will be developed as a web application.



[mattermost]: https://mattermost.imp.fu-berlin.de/swp-ws21-fahrtenbuch/channels/team-krustentiere
[wiki]: https://git.imp.fu-berlin.de/groups/swp-ws21-fahrtenbuch/team-krustentiere/-/wikis/home
[kvv]: https://mycampus.imp.fu-berlin.de/portal/site/6d54deec-c787-4a76-ae82-e1274b89d41d
[board]: https://git.imp.fu-berlin.de/swp-ws21-fahrtenbuch/team-krustentiere/efb/-/boards/983?milestone_title=Sprint%201%3A%20Projekt%20Skelett

## Project Setup




### Docker

First, install [`docker`][install-docker], [`docker-compose`][install-docker-compose]

Then start the whole thing (remove the -d, if you want the logs):

```sudo docker-compose up -d```

Rebuild backend image:
```sudo docker-compose build```

Stop and remove containers, and remove networks and volumes (without -v, if you want to keep the volumes)
```sudo docker-compose down -v```

## Project Architecture

![Image shows the project architecture](../docs/resources/architecture.png)

## Frontend Description

[Details](./frontend/README.md)

## Backend Description

[Details](./backend/README.MD)

## TODO

### Discussed during end presentation, needs to be done

- Statistics by discipline
- Statistics by weekdays
- Generally optimise statistics page (filter e.g.)
- 11 p.m. automatic return of boats

### Other TODOs (seems a lot but is a very detailed description)

- Improve password changing mechanism for admin + boat attendant [](https://git.imp.fu-berlin.de/swp-ws21-fahrtenbuch/team-krustentiere/efb/-/issues/146)
- After adding boat_type or discipline jump to the end of the list.
- Fix security gaps with the following packages [Issue](https://git.imp.fu-berlin.de/swp-ws21-fahrtenbuch/team-krustentiere/efb/-/issues/145)
- general code cleanup (delete printlines, etc.) [BE](https://git.imp.fu-berlin.de/swp-ws21-fahrtenbuch/team-krustentiere/efb/-/issues/67) [FE](https://git.imp.fu-berlin.de/swp-ws21-fahrtenbuch/team-krustentiere/efb/-/issues/147)
- [This](https://git.imp.fu-berlin.de/swp-ws21-fahrtenbuch/team-krustentiere/efb/-/issues/109) would be a useful feature, but it needs agreement with WSZ first
- change test email account efbtestetunseremails@gmail.com to proper mail server
