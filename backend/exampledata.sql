-- Personen: ein Admin (Koordinator roleflag = 1) und 10 Bootswarte; Passwort test1234
--  test1234

INSERT INTO person(surname, name, username, email, password, role_flag) (SELECT 'Mustermann699', 'Doreen699', 'Doreen699584', 'efbtestetunseremails@gmail.com', '$2b$12$qGLM6MmDgisp405MP.WahOpMRwm5BOv0YCEfVKBlcH45q7xzpWA8u', '2' WHERE NOT EXISTS (SELECT id FROM person WHERE username = 'Doreen699584'));

INSERT INTO person(surname, name, username, email, password, role_flag) (SELECT 'Mueller322', 'Rudi322', 'Rudi322994', 'efbtestetunseremails@gmail.com', '$2b$12$qGLM6MmDgisp405MP.WahOpMRwm5BOv0YCEfVKBlcH45q7xzpWA8u', '2' WHERE NOT EXISTS (SELECT id FROM person WHERE username = 'Rudi322994'));

INSERT INTO person(surname, name, username, email, password, role_flag) (SELECT 'Musterfrau582', 'Max582', 'Max582729', 'efbtestetunseremails@gmail.com', '$2b$12$qGLM6MmDgisp405MP.WahOpMRwm5BOv0YCEfVKBlcH45q7xzpWA8u', '2' WHERE NOT EXISTS (SELECT id FROM person WHERE username = 'Max582729'));

INSERT INTO person(surname, name, username, email, password, role_flag) (SELECT 'Mustermann851', 'Hannelore851', 'Hannelore851709', 'efbtestetunseremails@gmail.com', '$2b$12$qGLM6MmDgisp405MP.WahOpMRwm5BOv0YCEfVKBlcH45q7xzpWA8u', '2' WHERE NOT EXISTS (SELECT id FROM person WHERE username = 'Hannelore851709'));

INSERT INTO person(surname, name, username, email, password, role_flag) (SELECT 'Mueller233', 'Doreen233', 'Doreen233259', 'efbtestetunseremails@gmail.com', '$2b$12$qGLM6MmDgisp405MP.WahOpMRwm5BOv0YCEfVKBlcH45q7xzpWA8u', '2' WHERE NOT EXISTS (SELECT id FROM person WHERE username = 'Doreen233259'));

INSERT INTO person(surname, name, username, email, password, role_flag) (SELECT 'Musterfrau335', 'Rudi335', 'Rudi335527', 'efbtestetunseremails@gmail.com', '$2b$12$qGLM6MmDgisp405MP.WahOpMRwm5BOv0YCEfVKBlcH45q7xzpWA8u', '2' WHERE NOT EXISTS (SELECT id FROM person WHERE username = 'Rudi335527'));

INSERT INTO person(surname, name, username, email, password, role_flag) (SELECT 'Mustermann94', 'Max94', 'Max94295', 'efbtestetunseremails@gmail.com', '$2b$12$qGLM6MmDgisp405MP.WahOpMRwm5BOv0YCEfVKBlcH45q7xzpWA8u', '2' WHERE NOT EXISTS (SELECT id FROM person WHERE username = 'Max94295'));

INSERT INTO person(surname, name, username, email, password, role_flag) (SELECT 'Mueller109', 'Hannelore109', 'Hannelore109190', 'efbtestetunseremails@gmail.com', '$2b$12$qGLM6MmDgisp405MP.WahOpMRwm5BOv0YCEfVKBlcH45q7xzpWA8u', '2' WHERE NOT EXISTS (SELECT id FROM person WHERE username = 'Hannelore109190'));

INSERT INTO person(surname, name, username, email, password, role_flag) (SELECT 'Musterfrau574', 'Doreen574', 'Doreen574945', 'efbtestetunseremails@gmail.com', '$2b$12$qGLM6MmDgisp405MP.WahOpMRwm5BOv0YCEfVKBlcH45q7xzpWA8u', '2' WHERE NOT EXISTS (SELECT id FROM person WHERE username = 'Doreen574945'));

INSERT INTO person(surname, name, username, email, password, role_flag) (SELECT 'Mustermann173', 'Rudi173', 'Rudi173538', 'efbtestetunseremails@gmail.com', '$2b$12$qGLM6MmDgisp405MP.WahOpMRwm5BOv0YCEfVKBlcH45q7xzpWA8u', '2' WHERE NOT EXISTS (SELECT id FROM person WHERE username = 'Rudi173538'));

INSERT INTO person(surname, name, username, email, password, role_flag) (SELECT 'Wichtig', 'Sehr', 'Wichtig1', 'efbtestetunseremails@gmail.com', '$2b$12$qGLM6MmDgisp405MP.WahOpMRwm5BOv0YCEfVKBlcH45q7xzpWA8u', '1' WHERE NOT EXISTS (SELECT id FROM person WHERE username = 'Wichtig1'));



-- Personenanzahl geraten ; Quelle: https://www.hochschulsport.fu-berlin.de/sportprogramm/wassersportzentrum/Vermietung_ws/index.html

--Kanus und Kanupolo ? 
INSERT INTO boat_type(min_persons, max_persons, name) 
(SELECT 1, 2, 'Necky Amaruk' WHERE NOT EXISTS (SELECT id FROM boat_type WHERE name = 'Necky Amaruk'));  
INSERT INTO boat_type(min_persons, max_persons, name) 
(SELECT 1, 2, 'Prijon Excursion' WHERE NOT EXISTS (SELECT id FROM boat_type WHERE name = 'Prijon Excursion'));
INSERT INTO boat_type(min_persons, max_persons, name) 
(SELECT 1 , 3, 'Necky Alsek' WHERE NOT EXISTS (SELECT id FROM boat_type WHERE name = 'Necky Alsek'));
INSERT INTO boat_type(min_persons, max_persons, name) 
(SELECT 1 , 8, 'Lettmann Infinity' WHERE NOT EXISTS (SELECT id FROM boat_type WHERE name = 'Lettmann Infinity'));
INSERT INTO boat_type(min_persons, max_persons, name) 
(SELECT 1 , 2, 'Prijon Capri' WHERE NOT EXISTS (SELECT id FROM boat_type WHERE name = 'Prijon Capri'));
INSERT INTO boat_type(min_persons, max_persons, name) 
(SELECT 1 , 3, 'Prijon CL 430' WHERE NOT EXISTS (SELECT id FROM boat_type WHERE name = 'Prijon CL 430'));
INSERT INTO boat_type(min_persons, max_persons, name) 
(SELECT 7, 8, 'Prijon Invader' WHERE NOT EXISTS (SELECT id FROM boat_type WHERE name = 'Prijon Invader'));
INSERT INTO boat_type(min_persons, max_persons, name) 
(SELECT 1 , 1, 'Tornado' WHERE NOT EXISTS (SELECT id FROM boat_type WHERE name = 'Tornado'));
INSERT INTO boat_type(min_persons, max_persons, name) 
(SELECT 1 ,3 , 'Gattino' WHERE NOT EXISTS (SELECT id FROM boat_type WHERE name = 'Gattino'));
INSERT INTO boat_type(min_persons, max_persons, name) 
(SELECT 1 ,1,'Kirton 1er' WHERE NOT EXISTS (SELECT id FROM boat_type WHERE name = 'Kirton 1er'));
INSERT INTO boat_type(min_persons, max_persons, name) 
(SELECT 1 , 3, 'Kanadier Klinkerboot Love' WHERE NOT EXISTS (SELECT id FROM boat_type WHERE name = 'Kanadier Klinkerboot Love'));
INSERT INTO boat_type(min_persons, max_persons, name) 
(SELECT 1 , 164, 'Kanadier Old Town Penobscot 164' WHERE NOT EXISTS (SELECT id FROM boat_type WHERE name = 'Kanadier Old Town Penobscot 164'));
INSERT INTO boat_type(min_persons, max_persons, name) 
(SELECT 2,2, 'Skiffs' WHERE NOT EXISTS (SELECT id FROM boat_type WHERE name = 'Skiffs'));
INSERT INTO boat_type(min_persons, max_persons, name) 
(SELECT 2 ,2, '2-/2' WHERE NOT EXISTS (SELECT id FROM boat_type WHERE name = '2-/2'));
INSERT INTO boat_type(min_persons, max_persons, name) 
(SELECT 1 ,1, '1x-Gig' WHERE NOT EXISTS (SELECT id FROM boat_type WHERE name = '1x-Gig'));
INSERT INTO boat_type(min_persons, max_persons, name) 
(SELECT 3 ,3, '2x+ Gig Mannschaftsboot' WHERE NOT EXISTS (SELECT id FROM boat_type WHERE name = '2x+ Gig Mannschaftsboot'));
INSERT INTO boat_type(min_persons, max_persons, name) 
(SELECT 2, 2, '2x-Gig Mannschaftsboot' WHERE NOT EXISTS (SELECT id FROM boat_type WHERE name = '2x-Gig Mannschaftsboot'));
INSERT INTO boat_type(min_persons, max_persons, name) 
(SELECT 3, 3,'2x-, 3x Gig Mannschaftsboot'   WHERE NOT EXISTS (SELECT id FROM boat_type WHERE name = '2x-, 3x Gig Mannschaftsboot'));
INSERT INTO boat_type(min_persons, max_persons, name) 
(SELECT 5, 5,'4 x+ Gig Mannschaftsboot'   WHERE NOT EXISTS (SELECT id FROM boat_type WHERE name = '4 x+ Gig Mannschaftsboot'));
INSERT INTO boat_type(min_persons, max_persons, name) 
(SELECT 9, 9,'8 x+ Gig Mannschaftsboot'   WHERE NOT EXISTS (SELECT id FROM boat_type WHERE name = '8 x+ Gig Mannschaftsboot'));
INSERT INTO boat_type(min_persons, max_persons, name) 
(SELECT 2, 2,'RS Vision'   WHERE NOT EXISTS (SELECT id FROM boat_type WHERE name = 'RS Vision'));
INSERT INTO boat_type(min_persons, max_persons, name) 
(SELECT 2, 2,'RS Quest'   WHERE NOT EXISTS (SELECT id FROM boat_type WHERE name = 'RS Quest'));
INSERT INTO boat_type(min_persons, max_persons, name) 
(SELECT 2, 4,'Uni-Jolle'   WHERE NOT EXISTS (SELECT id FROM boat_type WHERE name = 'Uni-Jolle'));
INSERT INTO boat_type(min_persons, max_persons, name) 
(SELECT 3, 3,'Lis-Jolle'   WHERE NOT EXISTS (SELECT id FROM boat_type WHERE name = 'Lis-Jolle'));
INSERT INTO boat_type(min_persons, max_persons, name) 
(SELECT 2, 2,'XY-Jolle'   WHERE NOT EXISTS (SELECT id FROM boat_type WHERE name = 'XY-Jolle'));
INSERT INTO boat_type(min_persons, max_persons, name) 
(SELECT 3, 8,'Laser Radial (Fortgeschrittenen-Berechtig.)'   WHERE NOT EXISTS (SELECT id FROM boat_type WHERE name = 'Laser Radial (Fortgeschrittenen-Berechtig.)'));
INSERT INTO boat_type(min_persons, max_persons, name) 
(SELECT 9, 12,'Topcat K1/ K2 (Fortgeschrittenen-Berechtig.)'   WHERE NOT EXISTS (SELECT id FROM boat_type WHERE name = 'Topcat K1/ K2 (Fortgeschrittenen-Berechtig.)'));
INSERT INTO boat_type(min_persons, max_persons, name) 
(SELECT 10, 25,'Laser Stratos (Fortgeschrittenen-Berechtig.)'   WHERE NOT EXISTS (SELECT id FROM boat_type WHERE name = 'Laser Stratos (Fortgeschrittenen-Berechtig.)'));
INSERT INTO boat_type(min_persons, max_persons, name) 
(SELECT 1, 1,'BIC 11’6 ACE-TEC SUP Wind 2.0'   WHERE NOT EXISTS (SELECT id FROM boat_type WHERE name = 'BIC 11’6 ACE-TEC SUP Wind 2.0'));
INSERT INTO boat_type(min_persons, max_persons, name) 
(SELECT 1, 1,'Adventure Allround 11` 5 - Inflattable'   WHERE NOT EXISTS (SELECT id FROM boat_type WHERE name = 'Adventure Allround 11` 5 - Inflattable'));
INSERT INTO boat_type(min_persons, max_persons, name) 
(SELECT 1, 1,'Wide'   WHERE NOT EXISTS (SELECT id FROM boat_type WHERE name = 'Wide'));
INSERT INTO boat_type(min_persons, max_persons, name) 
(SELECT 1, 1,'Revo 335'   WHERE NOT EXISTS (SELECT id FROM boat_type WHERE name = 'Revo 335'));
INSERT INTO boat_type(min_persons, max_persons, name) 
(SELECT 1, 1,'Primo'   WHERE NOT EXISTS (SELECT id FROM boat_type WHERE name = 'Primo'));
INSERT INTO boat_type(min_persons, max_persons, name) 
(SELECT 1, 1,'Style'   WHERE NOT EXISTS (SELECT id FROM boat_type WHERE name = 'Style'));
INSERT INTO boat_type(min_persons, max_persons, name) 
(SELECT 1, 1,'Maxx 2'   WHERE NOT EXISTS (SELECT id FROM boat_type WHERE name = 'Maxx 2'));

--------- Sportarten (Disciplines)
INSERT INTO discipline(name) 
(SELECT 'Kanupolo' WHERE NOT EXISTS (SELECT id FROM discipline WHERE name = 'Kanupolo'));  
INSERT INTO discipline(name) 
(SELECT 'Kanu' WHERE NOT EXISTS (SELECT id FROM discipline WHERE name = 'Kanu'));  
INSERT INTO discipline(name) 
(SELECT 'Rudern' WHERE NOT EXISTS (SELECT id FROM discipline WHERE name = 'Rudern'));  
INSERT INTO discipline(name) 
(SELECT 'Segeln' WHERE NOT EXISTS (SELECT id FROM discipline WHERE name = 'Segeln'));  
INSERT INTO discipline(name) 
(SELECT 'Stand-Up-Paddling' WHERE NOT EXISTS (SELECT id FROM discipline WHERE name = 'Stand-Up-Paddling'));  
INSERT INTO discipline(name) 
(SELECT 'Windsurfen' WHERE NOT EXISTS (SELECT id FROM discipline WHERE name = 'Windsurfen'));  

--------- Boote
INSERT INTO boat(name, damage_reports, boat_type_id)   -- Default broken and deleted flag
(SELECT 'Maxi2000', '', (SELECT id FROM boat_type WHERE name = 'Maxx 2') 
WHERE NOT EXISTS (SELECT id FROM boat WHERE name = 'Maxi2000')); 

INSERT INTO boat(broken, deleted, name, damage_reports, boat_type_id) 
(SELECT false, false, 'Maxi1', '', (SELECT id FROM boat_type WHERE name = 'Maxx 2') 
WHERE NOT EXISTS (SELECT id FROM boat WHERE name = 'Maxi1'));  
INSERT INTO boat(broken, deleted, name, damage_reports, boat_type_id) 
(SELECT false, false, 'Der Erste', '', (SELECT id FROM boat_type WHERE name = 'Primo') 
WHERE NOT EXISTS (SELECT id FROM boat WHERE name = 'Der Erste'));  
INSERT INTO boat(broken, deleted, name, damage_reports, boat_type_id) 
(SELECT false, false, 'GummiPaddel', '', (SELECT id FROM boat_type WHERE name = 'Adventure Allround 11` 5 - Inflattable') 
WHERE NOT EXISTS (SELECT id FROM boat WHERE name = 'GummiPaddel'));  
INSERT INTO boat(broken, deleted, name, damage_reports, boat_type_id) 
(SELECT false, false, 'Der einsame Windfang', '', (SELECT id FROM boat_type WHERE name = 'RS Vision') 
WHERE NOT EXISTS (SELECT id FROM boat WHERE name = 'Der einsame Windfang'));  
INSERT INTO boat(broken, deleted, name, damage_reports, boat_type_id) 
(SELECT false, false, 'RadialSystem', '', (SELECT id FROM boat_type WHERE name = 'Laser Radial (Fortgeschrittenen-Berechtig.)') 
WHERE NOT EXISTS (SELECT id FROM boat WHERE name = 'RadialSystem'));  
INSERT INTO boat(broken, deleted, name, damage_reports, boat_type_id) 
(SELECT true, false, 'Rudi1', '', (SELECT id FROM boat_type WHERE name = '2x-, 3x Gig Mannschaftsboot') 
WHERE NOT EXISTS (SELECT id FROM boat WHERE name = 'Rudi1'));  
INSERT INTO boat(broken, deleted, name, damage_reports, boat_type_id) 
(SELECT false, false, 'Rudi2', '', (SELECT id FROM boat_type WHERE name = '2-/2') 
WHERE NOT EXISTS (SELECT id FROM boat WHERE name = 'Rudi2'));  
INSERT INTO boat(broken, deleted, name, damage_reports, boat_type_id) 
(SELECT false, true, 'PrisonBreak', '', (SELECT id FROM boat_type WHERE name = 'Prijon Invader') 
WHERE NOT EXISTS (SELECT id FROM boat WHERE name = 'PrisonBreak'));  
INSERT INTO boat(broken, deleted, name, damage_reports, boat_type_id) 
(SELECT true, true, 'Stürmchen', '', (SELECT id FROM boat_type WHERE name = 'Tornado') 
WHERE NOT EXISTS (SELECT id FROM boat WHERE name = 'Stürmchen'));  
INSERT INTO boat(broken, deleted, name, damage_reports, boat_type_id) 
(SELECT false,false, 'FiddelKatzo', '', (SELECT id FROM boat_type WHERE name = 'Gattino') 
WHERE NOT EXISTS (SELECT id FROM boat WHERE name = 'FiddelKatzo'));  

-----Kunden
INSERT INTO boat_user(surname, name, phone_number, email, street, street_number, plz, city, borrow_entry_id) 
(SELECT 'Müller', 'Max', '0331-1234567', 'maxismailadresse@mailadresse.de', 'Hauptstrasse' ,'1', '12345', 'Potsdam', (SELECT id FROM borrow_entry WHERE destination = 'Alaska') 
WHERE NOT EXISTS (SELECT name FROM boat_user WHERE surname = 'Müller' AND name = 'Max'));  
INSERT INTO boat_user(surname, name, borrow_entry_id) 
(SELECT 'Sparow', 'Jack', (SELECT id FROM borrow_entry WHERE destination = 'Alaska') 
WHERE NOT EXISTS (SELECT name FROM boat_user WHERE surname = 'Müller' AND name = 'Max'));  

--- BoatType to Discipline - Zuordnung der Bootstypen zu Sportarten (natürlich erfunden)
-- Kanu /-polo
INSERT INTO boat_type_to_discipline(boat_type_id, discipline_id) 
(SELECT (SELECT id FROM boat_type WHERE name = 'Necky Amaruk'), (SELECT id FROM discipline WHERE name = 'Kanu') 
WHERE NOT EXISTS (SELECT boat_type_id, discipline_id FROM boat_type_to_discipline WHERE boat_type_id = (SELECT id FROM boat_type WHERE name = 'Necky Amaruk') AND discipline_id = (SELECT id FROM discipline WHERE name = 'Kanu')));
INSERT INTO boat_type_to_discipline(boat_type_id, discipline_id) 
(SELECT (SELECT id FROM boat_type WHERE name = 'Prijon Excursion'), (SELECT id FROM discipline WHERE name = 'Kanu') 
WHERE NOT EXISTS (SELECT boat_type_id, discipline_id FROM boat_type_to_discipline WHERE boat_type_id = (SELECT id FROM boat_type WHERE name = 'Prijon Excursion') AND discipline_id = (SELECT id FROM discipline WHERE name = 'Kanu')));
INSERT INTO boat_type_to_discipline(boat_type_id, discipline_id) 
(SELECT (SELECT id FROM boat_type WHERE name = 'Necky Alsek'), (SELECT id FROM discipline WHERE name = 'Kanu') 
WHERE NOT EXISTS (SELECT boat_type_id, discipline_id FROM boat_type_to_discipline WHERE boat_type_id = (SELECT id FROM boat_type WHERE name = 'Necky Alsek') AND discipline_id = (SELECT id FROM discipline WHERE name = 'Kanu')));
INSERT INTO boat_type_to_discipline(boat_type_id, discipline_id) 
(SELECT (SELECT id FROM boat_type WHERE name = 'Lettmann Infinity'), (SELECT id FROM discipline WHERE name = 'Kanu') 
WHERE NOT EXISTS (SELECT boat_type_id, discipline_id FROM boat_type_to_discipline WHERE boat_type_id = (SELECT id FROM boat_type WHERE name = 'Lettmann Infinity') AND discipline_id = (SELECT id FROM discipline WHERE name = 'Kanu')));
INSERT INTO boat_type_to_discipline(boat_type_id, discipline_id) 
(SELECT (SELECT id FROM boat_type WHERE name = 'Prijon Capri'), (SELECT id FROM discipline WHERE name = 'Kanu') 
WHERE NOT EXISTS (SELECT boat_type_id, discipline_id FROM boat_type_to_discipline WHERE boat_type_id = (SELECT id FROM boat_type WHERE name = 'Prijon Capri') AND discipline_id = (SELECT id FROM discipline WHERE name = 'Kanu')));
INSERT INTO boat_type_to_discipline(boat_type_id, discipline_id) 
(SELECT (SELECT id FROM boat_type WHERE name = 'Prijon CL 430'), (SELECT id FROM discipline WHERE name = 'Kanu') 
WHERE NOT EXISTS (SELECT boat_type_id, discipline_id FROM boat_type_to_discipline WHERE boat_type_id = (SELECT id FROM boat_type WHERE name = 'Prijon CL 430') AND discipline_id = (SELECT id FROM discipline WHERE name = 'Kanu')));
INSERT INTO boat_type_to_discipline(boat_type_id, discipline_id) 
(SELECT (SELECT id FROM boat_type WHERE name = 'Prijon Invader'), (SELECT id FROM discipline WHERE name = 'Kanu') 
WHERE NOT EXISTS (SELECT boat_type_id, discipline_id FROM boat_type_to_discipline WHERE boat_type_id = (SELECT id FROM boat_type WHERE name = 'Prijon Invader') AND discipline_id = (SELECT id FROM discipline WHERE name = 'Kanu')));
INSERT INTO boat_type_to_discipline(boat_type_id, discipline_id) 
(SELECT (SELECT id FROM boat_type WHERE name = 'Kanadier Old Town Penobscot 164'), (SELECT id FROM discipline WHERE name = 'Kanu') 
WHERE NOT EXISTS (SELECT boat_type_id, discipline_id FROM boat_type_to_discipline WHERE boat_type_id = (SELECT id FROM boat_type WHERE name = 'Kanadier Old Town Penobscot 164') AND discipline_id = (SELECT id FROM discipline WHERE name = 'Kanu')));
INSERT INTO boat_type_to_discipline(boat_type_id, discipline_id) 
(SELECT (SELECT id FROM boat_type WHERE name = 'Tornado'), (SELECT id FROM discipline WHERE name = 'Kanu') 
WHERE NOT EXISTS (SELECT boat_type_id, discipline_id FROM boat_type_to_discipline WHERE boat_type_id = (SELECT id FROM boat_type WHERE name = 'Tornado') AND discipline_id = (SELECT id FROM discipline WHERE name = 'Kanu')));
INSERT INTO boat_type_to_discipline(boat_type_id, discipline_id) 
(SELECT (SELECT id FROM boat_type WHERE name = 'Gattino'), (SELECT id FROM discipline WHERE name = 'Kanu') 
WHERE NOT EXISTS (SELECT boat_type_id, discipline_id FROM boat_type_to_discipline WHERE boat_type_id = (SELECT id FROM boat_type WHERE name = 'Gattino') AND discipline_id = (SELECT id FROM discipline WHERE name = 'Kanu')));
INSERT INTO boat_type_to_discipline(boat_type_id, discipline_id) 
(SELECT (SELECT id FROM boat_type WHERE name = 'Kirton 1er'), (SELECT id FROM discipline WHERE name = 'Kanu') 
WHERE NOT EXISTS (SELECT boat_type_id, discipline_id FROM boat_type_to_discipline WHERE boat_type_id = (SELECT id FROM boat_type WHERE name = 'Kirton 1er') AND discipline_id = (SELECT id FROM discipline WHERE name = 'Kanu')));
INSERT INTO boat_type_to_discipline(boat_type_id, discipline_id) 
(SELECT (SELECT id FROM boat_type WHERE name = 'Kanadier Klinkerboot Love'), (SELECT id FROM discipline WHERE name = 'Kanu') 
WHERE NOT EXISTS (SELECT boat_type_id, discipline_id FROM boat_type_to_discipline WHERE boat_type_id = (SELECT id FROM boat_type WHERE name = 'Kanadier Klinkerboot Love') AND discipline_id = (SELECT id FROM discipline WHERE name = 'Kanu')));

-- Rudern
INSERT INTO boat_type_to_discipline(boat_type_id, discipline_id) 
(SELECT (SELECT id FROM boat_type WHERE name = 'Skiffs'), (SELECT id FROM discipline WHERE name = 'Rudern') 
WHERE NOT EXISTS (SELECT boat_type_id, discipline_id FROM boat_type_to_discipline WHERE boat_type_id = (SELECT id FROM boat_type WHERE name = 'Skiffs') AND discipline_id = (SELECT id FROM discipline WHERE name = 'Rudern')));
INSERT INTO boat_type_to_discipline(boat_type_id, discipline_id) 
(SELECT (SELECT id FROM boat_type WHERE name = '2-/2'), (SELECT id FROM discipline WHERE name = 'Rudern') 
WHERE NOT EXISTS (SELECT boat_type_id, discipline_id FROM boat_type_to_discipline WHERE boat_type_id = (SELECT id FROM boat_type WHERE name = '2-/2') AND discipline_id = (SELECT id FROM discipline WHERE name = 'Rudern')));
INSERT INTO boat_type_to_discipline(boat_type_id, discipline_id) 
(SELECT (SELECT id FROM boat_type WHERE name = '1x-Gig'), (SELECT id FROM discipline WHERE name = 'Rudern') 
WHERE NOT EXISTS (SELECT boat_type_id, discipline_id FROM boat_type_to_discipline WHERE boat_type_id = (SELECT id FROM boat_type WHERE name = '1x-Gig') AND discipline_id = (SELECT id FROM discipline WHERE name = 'Rudern')));
INSERT INTO boat_type_to_discipline(boat_type_id, discipline_id) 
(SELECT (SELECT id FROM boat_type WHERE name = '2x+ Gig Mannschaftsboot'), (SELECT id FROM discipline WHERE name = 'Rudern') 
WHERE NOT EXISTS (SELECT boat_type_id, discipline_id FROM boat_type_to_discipline WHERE boat_type_id = (SELECT id FROM boat_type WHERE name = '2x+ Gig Mannschaftsboot') AND discipline_id = (SELECT id FROM discipline WHERE name = 'Rudern')));
INSERT INTO boat_type_to_discipline(boat_type_id, discipline_id) 
(SELECT (SELECT id FROM boat_type WHERE name = '2x-Gig Mannschaftsboot'), (SELECT id FROM discipline WHERE name = 'Rudern') 
WHERE NOT EXISTS (SELECT boat_type_id, discipline_id FROM boat_type_to_discipline WHERE boat_type_id = (SELECT id FROM boat_type WHERE name = '2x-Gig Mannschaftsboot') AND discipline_id = (SELECT id FROM discipline WHERE name = 'Rudern')));
INSERT INTO boat_type_to_discipline(boat_type_id, discipline_id) 
(SELECT (SELECT id FROM boat_type WHERE name = '2x-, 3x Gig Mannschaftsboot'), (SELECT id FROM discipline WHERE name = 'Rudern') 
WHERE NOT EXISTS (SELECT boat_type_id, discipline_id FROM boat_type_to_discipline WHERE boat_type_id = (SELECT id FROM boat_type WHERE name = '2x-, 3x Gig Mannschaftsboot') AND discipline_id = (SELECT id FROM discipline WHERE name = 'Rudern')));
INSERT INTO boat_type_to_discipline(boat_type_id, discipline_id) 
(SELECT (SELECT id FROM boat_type WHERE name = '4 x+ Gig Mannschaftsboot'), (SELECT id FROM discipline WHERE name = 'Rudern') 
WHERE NOT EXISTS (SELECT boat_type_id, discipline_id FROM boat_type_to_discipline WHERE boat_type_id = (SELECT id FROM boat_type WHERE name = '4 x+ Gig Mannschaftsboot') AND discipline_id = (SELECT id FROM discipline WHERE name = 'Rudern')));
INSERT INTO boat_type_to_discipline(boat_type_id, discipline_id) 
(SELECT (SELECT id FROM boat_type WHERE name = '8 x+ Gig Mannschaftsboot'), (SELECT id FROM discipline WHERE name = 'Rudern') 
WHERE NOT EXISTS (SELECT boat_type_id, discipline_id FROM boat_type_to_discipline WHERE boat_type_id = (SELECT id FROM boat_type WHERE name = '8 x+ Gig Mannschaftsboot') AND discipline_id = (SELECT id FROM discipline WHERE name = 'Rudern')));

-- Segeln
INSERT INTO boat_type_to_discipline(boat_type_id, discipline_id) 
(SELECT (SELECT id FROM boat_type WHERE name = 'RS Vision'), (SELECT id FROM discipline WHERE name = 'Segeln') 
WHERE NOT EXISTS (SELECT boat_type_id, discipline_id FROM boat_type_to_discipline WHERE boat_type_id = (SELECT id FROM boat_type WHERE name = 'RS Vision') AND discipline_id = (SELECT id FROM discipline WHERE name = 'Segeln')));
INSERT INTO boat_type_to_discipline(boat_type_id, discipline_id) 
(SELECT (SELECT id FROM boat_type WHERE name = 'RS Quest'), (SELECT id FROM discipline WHERE name = 'Segeln') 
WHERE NOT EXISTS (SELECT boat_type_id, discipline_id FROM boat_type_to_discipline WHERE boat_type_id = (SELECT id FROM boat_type WHERE name = 'RS Quest') AND discipline_id = (SELECT id FROM discipline WHERE name = 'Segeln')));
INSERT INTO boat_type_to_discipline(boat_type_id, discipline_id) 
(SELECT (SELECT id FROM boat_type WHERE name = 'Uni-Jolle'), (SELECT id FROM discipline WHERE name = 'Segeln') 
WHERE NOT EXISTS (SELECT boat_type_id, discipline_id FROM boat_type_to_discipline WHERE boat_type_id = (SELECT id FROM boat_type WHERE name = 'Uni-Jolle') AND discipline_id = (SELECT id FROM discipline WHERE name = 'Segeln')));
INSERT INTO boat_type_to_discipline(boat_type_id, discipline_id) 
(SELECT (SELECT id FROM boat_type WHERE name = 'Lis-Jolle'), (SELECT id FROM discipline WHERE name = 'Segeln') 
WHERE NOT EXISTS (SELECT boat_type_id, discipline_id FROM boat_type_to_discipline WHERE boat_type_id = (SELECT id FROM boat_type WHERE name = 'Lis-Jolle') AND discipline_id = (SELECT id FROM discipline WHERE name = 'Segeln')));
INSERT INTO boat_type_to_discipline(boat_type_id, discipline_id) 
(SELECT (SELECT id FROM boat_type WHERE name = 'XY-Jolle'), (SELECT id FROM discipline WHERE name = 'Segeln') 
WHERE NOT EXISTS (SELECT boat_type_id, discipline_id FROM boat_type_to_discipline WHERE boat_type_id = (SELECT id FROM boat_type WHERE name = 'XY-Jolle') AND discipline_id = (SELECT id FROM discipline WHERE name = 'Segeln')));
INSERT INTO boat_type_to_discipline(boat_type_id, discipline_id) 
(SELECT (SELECT id FROM boat_type WHERE name = 'Laser Radial (Fortgeschrittenen-Berechtig.)'), (SELECT id FROM discipline WHERE name = 'Segeln') 
WHERE NOT EXISTS (SELECT boat_type_id, discipline_id FROM boat_type_to_discipline WHERE boat_type_id = (SELECT id FROM boat_type WHERE name = 'Laser Radial (Fortgeschrittenen-Berechtig.)') AND discipline_id = (SELECT id FROM discipline WHERE name = 'Segeln')));
INSERT INTO boat_type_to_discipline(boat_type_id, discipline_id) 
(SELECT (SELECT id FROM boat_type WHERE name = 'Topcat K1/ K2 (Fortgeschrittenen-Berechtig.)'), (SELECT id FROM discipline WHERE name = 'Segeln') 
WHERE NOT EXISTS (SELECT boat_type_id, discipline_id FROM boat_type_to_discipline WHERE boat_type_id = (SELECT id FROM boat_type WHERE name = 'Topcat K1/ K2 (Fortgeschrittenen-Berechtig.)') AND discipline_id = (SELECT id FROM discipline WHERE name = 'Segeln')));
INSERT INTO boat_type_to_discipline(boat_type_id, discipline_id) 
(SELECT (SELECT id FROM boat_type WHERE name = 'Laser Stratos (Fortgeschrittenen-Berechtig.)'), (SELECT id FROM discipline WHERE name = 'Segeln') 
WHERE NOT EXISTS (SELECT boat_type_id, discipline_id FROM boat_type_to_discipline WHERE boat_type_id = (SELECT id FROM boat_type WHERE name = 'Laser Stratos (Fortgeschrittenen-Berechtig.)') AND discipline_id = (SELECT id FROM discipline WHERE name = 'Segeln')));

--Stand-Up-Paddling
INSERT INTO boat_type_to_discipline(boat_type_id, discipline_id) 
(SELECT (SELECT id FROM boat_type WHERE name = 'BIC 11’6 ACE-TEC SUP Wind 2.0'), (SELECT id FROM discipline WHERE name = 'Stand-Up-Paddling') 
WHERE NOT EXISTS (SELECT boat_type_id, discipline_id FROM boat_type_to_discipline WHERE boat_type_id = (SELECT id FROM boat_type WHERE name = 'BIC 11’6 ACE-TEC SUP Wind 2.0') AND discipline_id = (SELECT id FROM discipline WHERE name = 'Stand-Up-Paddling')));
INSERT INTO boat_type_to_discipline(boat_type_id, discipline_id) 
(SELECT (SELECT id FROM boat_type WHERE name = 'Adventure Allround 11` 5 - Inflattable'), (SELECT id FROM discipline WHERE name = 'Stand-Up-Paddling') 
WHERE NOT EXISTS (SELECT boat_type_id, discipline_id FROM boat_type_to_discipline WHERE boat_type_id = (SELECT id FROM boat_type WHERE name = 'Adventure Allround 11` 5 - Inflattable') AND discipline_id = (SELECT id FROM discipline WHERE name = 'Stand-Up-Paddling')));

--Windsurfen
INSERT INTO boat_type_to_discipline(boat_type_id, discipline_id) 
(SELECT (SELECT id FROM boat_type WHERE name = 'Revo 335'), (SELECT id FROM discipline WHERE name = 'Windsurfen') 
WHERE NOT EXISTS (SELECT boat_type_id, discipline_id FROM boat_type_to_discipline WHERE boat_type_id = (SELECT id FROM boat_type WHERE name = 'Revo 335') AND discipline_id = (SELECT id FROM discipline WHERE name = 'Windsurfen')));
INSERT INTO boat_type_to_discipline(boat_type_id, discipline_id) 
(SELECT (SELECT id FROM boat_type WHERE name = 'Primo'), (SELECT id FROM discipline WHERE name = 'Windsurfen') 
WHERE NOT EXISTS (SELECT boat_type_id, discipline_id FROM boat_type_to_discipline WHERE boat_type_id = (SELECT id FROM boat_type WHERE name = 'Primo') AND discipline_id = (SELECT id FROM discipline WHERE name = 'Windsurfen')));
INSERT INTO boat_type_to_discipline(boat_type_id, discipline_id) 
(SELECT (SELECT id FROM boat_type WHERE name = 'Wide'), (SELECT id FROM discipline WHERE name = 'Windsurfen') 
WHERE NOT EXISTS (SELECT boat_type_id, discipline_id FROM boat_type_to_discipline WHERE boat_type_id = (SELECT id FROM boat_type WHERE name = 'Wide') AND discipline_id = (SELECT id FROM discipline WHERE name = 'Windsurfen')));
INSERT INTO boat_type_to_discipline(boat_type_id, discipline_id) 
(SELECT (SELECT id FROM boat_type WHERE name = 'Style'), (SELECT id FROM discipline WHERE name = 'Windsurfen') 
WHERE NOT EXISTS (SELECT boat_type_id, discipline_id FROM boat_type_to_discipline WHERE boat_type_id = (SELECT id FROM boat_type WHERE name = 'Style') AND discipline_id = (SELECT id FROM discipline WHERE name = 'Windsurfen')));
INSERT INTO boat_type_to_discipline(boat_type_id, discipline_id) 
(SELECT (SELECT id FROM boat_type WHERE name = 'Maxx 2'), (SELECT id FROM discipline WHERE name = 'Windsurfen') 
WHERE NOT EXISTS (SELECT boat_type_id, discipline_id FROM boat_type_to_discipline WHERE boat_type_id = (SELECT id FROM boat_type WHERE name = 'Maxx 2') AND discipline_id = (SELECT id FROM discipline WHERE name = 'Windsurfen')));

--------- Ausleiheeinträge
INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2022-1-29 04:00:01+01','2022-1-29 05:00:01+01', '962', (SELECT id FROM boat WHERE name = 'FiddelKatzo'), (SELECT id FROM discipline WHERE name = 'Kanupolo') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '962'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2022-1-27 04:00:01+01','2022-1-27 05:00:01+01', '164', (SELECT id FROM boat WHERE name = 'Maxi2000'), (SELECT id FROM discipline WHERE name = 'Windsurfen') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '164'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2022-1-9 04:00:01+01','2022-1-9 05:00:01+01', '424', (SELECT id FROM boat WHERE name = 'Maxi1'), (SELECT id FROM discipline WHERE name = 'Stand-Up-Paddling') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '424'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2022-1-13 04:00:01+01','2022-1-13 05:00:01+01', '198', (SELECT id FROM boat WHERE name = 'Der Erste'), (SELECT id FROM discipline WHERE name = 'Segeln') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '198'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2022-1-15 04:00:01+01','2022-1-15 05:00:01+01', '361', (SELECT id FROM boat WHERE name = 'GummiPaddel'), (SELECT id FROM discipline WHERE name = 'Rudern') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '361'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2022-1-7 04:00:01+01','2022-1-7 05:00:01+01', '449', (SELECT id FROM boat WHERE name = 'Der einsame Windfang'), (SELECT id FROM discipline WHERE name = 'Kanu') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '449'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2022-1-27 04:00:01+01','2022-1-27 05:00:01+01', '532', (SELECT id FROM boat WHERE name = 'RadialSystem'), (SELECT id FROM discipline WHERE name = 'Kanupolo') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '532'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2022-1-9 04:00:01+01','2022-1-9 05:00:01+01', '589', (SELECT id FROM boat WHERE name = 'Rudi2'), (SELECT id FROM discipline WHERE name = 'Windsurfen') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '589'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2022-1-25 04:00:01+01','2022-1-25 05:00:01+01', '962', (SELECT id FROM boat WHERE name = 'FiddelKatzo'), (SELECT id FROM discipline WHERE name = 'Stand-Up-Paddling') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '962'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2022-1-24 04:00:01+01','2022-1-24 05:00:01+01', '184', (SELECT id FROM boat WHERE name = 'Maxi2000'), (SELECT id FROM discipline WHERE name = 'Segeln') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '184'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2022-1-9 04:00:01+01','2022-1-9 05:00:01+01', '484', (SELECT id FROM boat WHERE name = 'Maxi1'), (SELECT id FROM discipline WHERE name = 'Rudern') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '484'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2022-1-2 04:00:01+01','2022-1-2 05:00:01+01', '448', (SELECT id FROM boat WHERE name = 'Der Erste'), (SELECT id FROM discipline WHERE name = 'Kanu') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '448'));


INSERT INTO borrow_entry(begintime, destination, boat_id, discipline_id) 
(SELECT '2020-08-30 04:00:01+01', 'Alaska', (SELECT id FROM boat WHERE name = 'Maxi1'), (SELECT id FROM discipline WHERE name = 'Windsurfen') 
WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = 'Alaska'));  

INSERT INTO borrow_entry(begintime, destination, boat_id, discipline_id) 
(SELECT '2021-08-30 04:00:01+01', 'DahlemDorf', (SELECT id FROM boat WHERE name = 'Maxi1'), (SELECT id FROM discipline WHERE name = 'Windsurfen') 
WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = 'DahlemDorf')); 

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) 
(SELECT '2022-01-14 04:00:01+01', '2022-01-14 05:00:01+01', 'Mitte', (SELECT id FROM boat WHERE name = 'Maxi1'), (SELECT id FROM discipline WHERE name = 'Windsurfen') 
WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = 'Mitte')); 

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-5-12 04:00:01+01','2021-5-12 05:00:01+01', '937', (SELECT id FROM boat WHERE name = 'FiddelKatzo'), (SELECT id FROM discipline WHERE name = 'Kanupolo') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '937'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-1-10 04:00:01+01','2021-1-10 05:00:01+01', '731', (SELECT id FROM boat WHERE name = 'Maxi2000'), (SELECT id FROM discipline WHERE name = 'Windsurfen') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '731'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-1-7 04:00:01+01','2021-1-7 05:00:01+01', '136', (SELECT id FROM boat WHERE name = 'Maxi1'), (SELECT id FROM discipline WHERE name = 'Stand-Up-Paddling') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '136'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-12-18 04:00:01+01','2021-12-18 05:00:01+01', '776', (SELECT id FROM boat WHERE name = 'Der Erste'), (SELECT id FROM discipline WHERE name = 'Segeln') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '776'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-10-23 04:00:01+01','2021-10-23 05:00:01+01', '946', (SELECT id FROM boat WHERE name = 'GummiPaddel'), (SELECT id FROM discipline WHERE name = 'Rudern') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '946'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-3-2 04:00:01+01','2021-3-2 05:00:01+01', '490', (SELECT id FROM boat WHERE name = 'Der einsame Windfang'), (SELECT id FROM discipline WHERE name = 'Kanu') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '490'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-11-26 04:00:01+01','2021-11-26 05:00:01+01', '509', (SELECT id FROM boat WHERE name = 'RadialSystem'), (SELECT id FROM discipline WHERE name = 'Kanupolo') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '509'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-3-22 04:00:01+01','2021-3-22 05:00:01+01', '141', (SELECT id FROM boat WHERE name = 'Rudi2'), (SELECT id FROM discipline WHERE name = 'Windsurfen') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '141'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-11-19 04:00:01+01','2021-11-19 05:00:01+01', '580', (SELECT id FROM boat WHERE name = 'FiddelKatzo'), (SELECT id FROM discipline WHERE name = 'Stand-Up-Paddling') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '580'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-5-7 04:00:01+01','2021-5-7 05:00:01+01', '214', (SELECT id FROM boat WHERE name = 'Maxi2000'), (SELECT id FROM discipline WHERE name = 'Segeln') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '214'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-8-27 04:00:01+01','2021-8-27 05:00:01+01', '120', (SELECT id FROM boat WHERE name = 'Maxi1'), (SELECT id FROM discipline WHERE name = 'Rudern') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '120'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-12-11 04:00:01+01','2021-12-11 05:00:01+01', '338', (SELECT id FROM boat WHERE name = 'Der Erste'), (SELECT id FROM discipline WHERE name = 'Kanu') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '338'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-8-17 04:00:01+01','2021-8-17 05:00:01+01', '696', (SELECT id FROM boat WHERE name = 'GummiPaddel'), (SELECT id FROM discipline WHERE name = 'Kanupolo') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '696'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-12-17 04:00:01+01','2021-12-17 05:00:01+01', '490', (SELECT id FROM boat WHERE name = 'Der einsame Windfang'), (SELECT id FROM discipline WHERE name = 'Windsurfen') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '490'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-1-6 04:00:01+01','2021-1-6 05:00:01+01', '521', (SELECT id FROM boat WHERE name = 'RadialSystem'), (SELECT id FROM discipline WHERE name = 'Stand-Up-Paddling') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '521'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-8-16 04:00:01+01','2021-8-16 05:00:01+01', '726', (SELECT id FROM boat WHERE name = 'Rudi2'), (SELECT id FROM discipline WHERE name = 'Segeln') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '726'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-12-13 04:00:01+01','2021-12-13 05:00:01+01', '951', (SELECT id FROM boat WHERE name = 'FiddelKatzo'), (SELECT id FROM discipline WHERE name = 'Rudern') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '951'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-8-12 04:00:01+01','2021-8-12 05:00:01+01', '937', (SELECT id FROM boat WHERE name = 'Maxi2000'), (SELECT id FROM discipline WHERE name = 'Kanu') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '937'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-12-21 04:00:01+01','2021-12-21 05:00:01+01', '838', (SELECT id FROM boat WHERE name = 'Maxi1'), (SELECT id FROM discipline WHERE name = 'Kanupolo') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '838'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-3-22 04:00:01+01','2021-3-22 05:00:01+01', '245', (SELECT id FROM boat WHERE name = 'Der Erste'), (SELECT id FROM discipline WHERE name = 'Windsurfen') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '245'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-3-15 04:00:01+01','2021-3-15 05:00:01+01', '282', (SELECT id FROM boat WHERE name = 'GummiPaddel'), (SELECT id FROM discipline WHERE name = 'Stand-Up-Paddling') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '282'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-7-7 04:00:01+01','2021-7-7 05:00:01+01', '592', (SELECT id FROM boat WHERE name = 'Der einsame Windfang'), (SELECT id FROM discipline WHERE name = 'Segeln') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '592'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-12-25 04:00:01+01','2021-12-25 05:00:01+01', '585', (SELECT id FROM boat WHERE name = 'RadialSystem'), (SELECT id FROM discipline WHERE name = 'Rudern') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '585'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-10-23 04:00:01+01','2021-10-23 05:00:01+01', '228', (SELECT id FROM boat WHERE name = 'Rudi2'), (SELECT id FROM discipline WHERE name = 'Kanu') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '228'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-10-12 04:00:01+01','2021-10-12 05:00:01+01', '450', (SELECT id FROM boat WHERE name = 'FiddelKatzo'), (SELECT id FROM discipline WHERE name = 'Kanupolo') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '450'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-7-23 04:00:01+01','2021-7-23 05:00:01+01', '156', (SELECT id FROM boat WHERE name = 'Maxi2000'), (SELECT id FROM discipline WHERE name = 'Windsurfen') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '156'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-12-8 04:00:01+01','2021-12-8 05:00:01+01', '672', (SELECT id FROM boat WHERE name = 'Maxi1'), (SELECT id FROM discipline WHERE name = 'Stand-Up-Paddling') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '672'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-1-11 04:00:01+01','2021-1-11 05:00:01+01', '707', (SELECT id FROM boat WHERE name = 'Der Erste'), (SELECT id FROM discipline WHERE name = 'Segeln') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '707'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-4-2 04:00:01+01','2021-4-2 05:00:01+01', '531', (SELECT id FROM boat WHERE name = 'GummiPaddel'), (SELECT id FROM discipline WHERE name = 'Rudern') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '531'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-7-4 04:00:01+01','2021-7-4 05:00:01+01', '852', (SELECT id FROM boat WHERE name = 'Der einsame Windfang'), (SELECT id FROM discipline WHERE name = 'Kanu') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '852'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-9-4 04:00:01+01','2021-9-4 05:00:01+01', '585', (SELECT id FROM boat WHERE name = 'RadialSystem'), (SELECT id FROM discipline WHERE name = 'Kanupolo') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '585'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-9-18 04:00:01+01','2021-9-18 05:00:01+01', '504', (SELECT id FROM boat WHERE name = 'Rudi2'), (SELECT id FROM discipline WHERE name = 'Windsurfen') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '504'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-4-20 04:00:01+01','2021-4-20 05:00:01+01', '746', (SELECT id FROM boat WHERE name = 'FiddelKatzo'), (SELECT id FROM discipline WHERE name = 'Stand-Up-Paddling') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '746'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-4-17 04:00:01+01','2021-4-17 05:00:01+01', '153', (SELECT id FROM boat WHERE name = 'Maxi2000'), (SELECT id FROM discipline WHERE name = 'Segeln') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '153'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-11-16 04:00:01+01','2021-11-16 05:00:01+01', '352', (SELECT id FROM boat WHERE name = 'Maxi1'), (SELECT id FROM discipline WHERE name = 'Rudern') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '352'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-2-14 04:00:01+01','2021-2-14 05:00:01+01', '552', (SELECT id FROM boat WHERE name = 'Der Erste'), (SELECT id FROM discipline WHERE name = 'Kanu') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '552'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-3-25 04:00:01+01','2021-3-25 05:00:01+01', '832', (SELECT id FROM boat WHERE name = 'GummiPaddel'), (SELECT id FROM discipline WHERE name = 'Kanupolo') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '832'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-6-9 04:00:01+01','2021-6-9 05:00:01+01', '161', (SELECT id FROM boat WHERE name = 'Der einsame Windfang'), (SELECT id FROM discipline WHERE name = 'Windsurfen') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '161'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-8-12 04:00:01+01','2021-8-12 05:00:01+01', '747', (SELECT id FROM boat WHERE name = 'RadialSystem'), (SELECT id FROM discipline WHERE name = 'Stand-Up-Paddling') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '747'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-7-15 04:00:01+01','2021-7-15 05:00:01+01', '609', (SELECT id FROM boat WHERE name = 'Rudi2'), (SELECT id FROM discipline WHERE name = 'Segeln') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '609'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-8-4 04:00:01+01','2021-8-4 05:00:01+01', '701', (SELECT id FROM boat WHERE name = 'FiddelKatzo'), (SELECT id FROM discipline WHERE name = 'Rudern') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '701'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-4-26 04:00:01+01','2021-4-26 05:00:01+01', '189', (SELECT id FROM boat WHERE name = 'Maxi2000'), (SELECT id FROM discipline WHERE name = 'Kanu') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '189'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-11-5 04:00:01+01','2021-11-5 05:00:01+01', '573', (SELECT id FROM boat WHERE name = 'Maxi1'), (SELECT id FROM discipline WHERE name = 'Kanupolo') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '573'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-10-17 04:00:01+01','2021-10-17 05:00:01+01', '714', (SELECT id FROM boat WHERE name = 'Der Erste'), (SELECT id FROM discipline WHERE name = 'Windsurfen') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '714'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-8-25 04:00:01+01','2021-8-25 05:00:01+01', '595', (SELECT id FROM boat WHERE name = 'GummiPaddel'), (SELECT id FROM discipline WHERE name = 'Stand-Up-Paddling') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '595'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-6-11 04:00:01+01','2021-6-11 05:00:01+01', '663', (SELECT id FROM boat WHERE name = 'Der einsame Windfang'), (SELECT id FROM discipline WHERE name = 'Segeln') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '663'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-7-20 04:00:01+01','2021-7-20 05:00:01+01', '723', (SELECT id FROM boat WHERE name = 'RadialSystem'), (SELECT id FROM discipline WHERE name = 'Rudern') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '723'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-7-25 04:00:01+01','2021-7-25 05:00:01+01', '382', (SELECT id FROM boat WHERE name = 'Rudi2'), (SELECT id FROM discipline WHERE name = 'Kanu') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '382'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-4-24 04:00:01+01','2021-4-24 05:00:01+01', '966', (SELECT id FROM boat WHERE name = 'FiddelKatzo'), (SELECT id FROM discipline WHERE name = 'Kanupolo') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '966'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-2-22 04:00:01+01','2021-2-22 05:00:01+01', '434', (SELECT id FROM boat WHERE name = 'Maxi2000'), (SELECT id FROM discipline WHERE name = 'Windsurfen') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '434'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-9-21 04:00:01+01','2021-9-21 05:00:01+01', '951', (SELECT id FROM boat WHERE name = 'Maxi1'), (SELECT id FROM discipline WHERE name = 'Stand-Up-Paddling') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '951'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-9-15 04:00:01+01','2021-9-15 05:00:01+01', '113', (SELECT id FROM boat WHERE name = 'Der Erste'), (SELECT id FROM discipline WHERE name = 'Segeln') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '113'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-3-9 04:00:01+01','2021-3-9 05:00:01+01', '372', (SELECT id FROM boat WHERE name = 'GummiPaddel'), (SELECT id FROM discipline WHERE name = 'Rudern') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '372'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-1-25 04:00:01+01','2021-1-25 05:00:01+01', '274', (SELECT id FROM boat WHERE name = 'Der einsame Windfang'), (SELECT id FROM discipline WHERE name = 'Kanu') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '274'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-5-11 04:00:01+01','2021-5-11 05:00:01+01', '861', (SELECT id FROM boat WHERE name = 'RadialSystem'), (SELECT id FROM discipline WHERE name = 'Kanupolo') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '861'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-9-26 04:00:01+01','2021-9-26 05:00:01+01', '671', (SELECT id FROM boat WHERE name = 'Rudi2'), (SELECT id FROM discipline WHERE name = 'Windsurfen') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '671'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-7-23 04:00:01+01','2021-7-23 05:00:01+01', '290', (SELECT id FROM boat WHERE name = 'FiddelKatzo'), (SELECT id FROM discipline WHERE name = 'Stand-Up-Paddling') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '290'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-4-4 04:00:01+01','2021-4-4 05:00:01+01', '628', (SELECT id FROM boat WHERE name = 'Maxi2000'), (SELECT id FROM discipline WHERE name = 'Segeln') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '628'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-2-23 04:00:01+01','2021-2-23 05:00:01+01', '820', (SELECT id FROM boat WHERE name = 'Maxi1'), (SELECT id FROM discipline WHERE name = 'Rudern') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '820'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-8-21 04:00:01+01','2021-8-21 05:00:01+01', '863', (SELECT id FROM boat WHERE name = 'Der Erste'), (SELECT id FROM discipline WHERE name = 'Kanu') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '863'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-7-11 04:00:01+01','2021-7-11 05:00:01+01', '777', (SELECT id FROM boat WHERE name = 'GummiPaddel'), (SELECT id FROM discipline WHERE name = 'Kanupolo') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '777'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-7-17 04:00:01+01','2021-7-17 05:00:01+01', '357', (SELECT id FROM boat WHERE name = 'Der einsame Windfang'), (SELECT id FROM discipline WHERE name = 'Windsurfen') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '357'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-11-10 04:00:01+01','2021-11-10 05:00:01+01', '450', (SELECT id FROM boat WHERE name = 'RadialSystem'), (SELECT id FROM discipline WHERE name = 'Stand-Up-Paddling') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '450'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-11-4 04:00:01+01','2021-11-4 05:00:01+01', '815', (SELECT id FROM boat WHERE name = 'Rudi2'), (SELECT id FROM discipline WHERE name = 'Segeln') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '815'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-12-24 04:00:01+01','2021-12-24 05:00:01+01', '560', (SELECT id FROM boat WHERE name = 'FiddelKatzo'), (SELECT id FROM discipline WHERE name = 'Rudern') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '560'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-2-24 04:00:01+01','2021-2-24 05:00:01+01', '181', (SELECT id FROM boat WHERE name = 'Maxi2000'), (SELECT id FROM discipline WHERE name = 'Kanu') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '181'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-5-5 04:00:01+01','2021-5-5 05:00:01+01', '864', (SELECT id FROM boat WHERE name = 'Maxi1'), (SELECT id FROM discipline WHERE name = 'Kanupolo') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '864'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-6-12 04:00:01+01','2021-6-12 05:00:01+01', '814', (SELECT id FROM boat WHERE name = 'Der Erste'), (SELECT id FROM discipline WHERE name = 'Windsurfen') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '814'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-7-16 04:00:01+01','2021-7-16 05:00:01+01', '593', (SELECT id FROM boat WHERE name = 'GummiPaddel'), (SELECT id FROM discipline WHERE name = 'Stand-Up-Paddling') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '593'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-12-16 04:00:01+01','2021-12-16 05:00:01+01', '179', (SELECT id FROM boat WHERE name = 'Der einsame Windfang'), (SELECT id FROM discipline WHERE name = 'Segeln') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '179'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-11-24 04:00:01+01','2021-11-24 05:00:01+01', '232', (SELECT id FROM boat WHERE name = 'RadialSystem'), (SELECT id FROM discipline WHERE name = 'Rudern') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '232'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-2-10 04:00:01+01','2021-2-10 05:00:01+01', '607', (SELECT id FROM boat WHERE name = 'Rudi2'), (SELECT id FROM discipline WHERE name = 'Kanu') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '607'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-7-15 04:00:01+01','2021-7-15 05:00:01+01', '181', (SELECT id FROM boat WHERE name = 'FiddelKatzo'), (SELECT id FROM discipline WHERE name = 'Kanupolo') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '181'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-5-14 04:00:01+01','2021-5-14 05:00:01+01', '383', (SELECT id FROM boat WHERE name = 'Maxi2000'), (SELECT id FROM discipline WHERE name = 'Windsurfen') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '383'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-10-15 04:00:01+01','2021-10-15 05:00:01+01', '833', (SELECT id FROM boat WHERE name = 'Maxi1'), (SELECT id FROM discipline WHERE name = 'Stand-Up-Paddling') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '833'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-1-27 04:00:01+01','2021-1-27 05:00:01+01', '723', (SELECT id FROM boat WHERE name = 'Der Erste'), (SELECT id FROM discipline WHERE name = 'Segeln') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '723'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-6-1 04:00:01+01','2021-6-1 05:00:01+01', '910', (SELECT id FROM boat WHERE name = 'GummiPaddel'), (SELECT id FROM discipline WHERE name = 'Rudern') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '910'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-12-7 04:00:01+01','2021-12-7 05:00:01+01', '173', (SELECT id FROM boat WHERE name = 'Der einsame Windfang'), (SELECT id FROM discipline WHERE name = 'Kanu') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '173'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-4-9 04:00:01+01','2021-4-9 05:00:01+01', '958', (SELECT id FROM boat WHERE name = 'RadialSystem'), (SELECT id FROM discipline WHERE name = 'Kanupolo') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '958'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-3-6 04:00:01+01','2021-3-6 05:00:01+01', '929', (SELECT id FROM boat WHERE name = 'Rudi2'), (SELECT id FROM discipline WHERE name = 'Windsurfen') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '929'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-5-27 04:00:01+01','2021-5-27 05:00:01+01', '612', (SELECT id FROM boat WHERE name = 'FiddelKatzo'), (SELECT id FROM discipline WHERE name = 'Stand-Up-Paddling') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '612'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-1-27 04:00:01+01','2021-1-27 05:00:01+01', '578', (SELECT id FROM boat WHERE name = 'Maxi2000'), (SELECT id FROM discipline WHERE name = 'Segeln') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '578'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-10-13 04:00:01+01','2021-10-13 05:00:01+01', '110', (SELECT id FROM boat WHERE name = 'Maxi1'), (SELECT id FROM discipline WHERE name = 'Rudern') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '110'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-2-2 04:00:01+01','2021-2-2 05:00:01+01', '225', (SELECT id FROM boat WHERE name = 'Der Erste'), (SELECT id FROM discipline WHERE name = 'Kanu') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '225'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-3-6 04:00:01+01','2021-3-6 05:00:01+01', '525', (SELECT id FROM boat WHERE name = 'GummiPaddel'), (SELECT id FROM discipline WHERE name = 'Kanupolo') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '525'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-12-4 04:00:01+01','2021-12-4 05:00:01+01', '567', (SELECT id FROM boat WHERE name = 'Der einsame Windfang'), (SELECT id FROM discipline WHERE name = 'Windsurfen') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '567'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2021-6-7 04:00:01+01','2021-6-7 05:00:01+01', '450', (SELECT id FROM boat WHERE name = 'RadialSystem'), (SELECT id FROM discipline WHERE name = 'Stand-Up-Paddling') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '450'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-8-2 04:00:01+01','2020-8-2 05:00:01+01', '972', (SELECT id FROM boat WHERE name = 'FiddelKatzo'), (SELECT id FROM discipline WHERE name = 'Kanupolo') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '972'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-10-13 04:00:01+01','2020-10-13 05:00:01+01', '635', (SELECT id FROM boat WHERE name = 'Maxi2000'), (SELECT id FROM discipline WHERE name = 'Windsurfen') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '635'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-12-17 04:00:01+01','2020-12-17 05:00:01+01', '165', (SELECT id FROM boat WHERE name = 'Maxi1'), (SELECT id FROM discipline WHERE name = 'Stand-Up-Paddling') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '165'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-7-5 04:00:01+01','2020-7-5 05:00:01+01', '126', (SELECT id FROM boat WHERE name = 'Der Erste'), (SELECT id FROM discipline WHERE name = 'Segeln') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '126'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-1-8 04:00:01+01','2020-1-8 05:00:01+01', '107', (SELECT id FROM boat WHERE name = 'GummiPaddel'), (SELECT id FROM discipline WHERE name = 'Rudern') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '107'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-3-20 04:00:01+01','2020-3-20 05:00:01+01', '705', (SELECT id FROM boat WHERE name = 'Der einsame Windfang'), (SELECT id FROM discipline WHERE name = 'Kanu') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '705'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-1-17 04:00:01+01','2020-1-17 05:00:01+01', '398', (SELECT id FROM boat WHERE name = 'RadialSystem'), (SELECT id FROM discipline WHERE name = 'Kanupolo') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '398'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-10-6 04:00:01+01','2020-10-6 05:00:01+01', '193', (SELECT id FROM boat WHERE name = 'Rudi2'), (SELECT id FROM discipline WHERE name = 'Windsurfen') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '193'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-5-13 04:00:01+01','2020-5-13 05:00:01+01', '232', (SELECT id FROM boat WHERE name = 'FiddelKatzo'), (SELECT id FROM discipline WHERE name = 'Stand-Up-Paddling') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '232'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-1-22 04:00:01+01','2020-1-22 05:00:01+01', '555', (SELECT id FROM boat WHERE name = 'Maxi2000'), (SELECT id FROM discipline WHERE name = 'Segeln') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '555'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-4-26 04:00:01+01','2020-4-26 05:00:01+01', '898', (SELECT id FROM boat WHERE name = 'Maxi1'), (SELECT id FROM discipline WHERE name = 'Rudern') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '898'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-7-14 04:00:01+01','2020-7-14 05:00:01+01', '552', (SELECT id FROM boat WHERE name = 'Der Erste'), (SELECT id FROM discipline WHERE name = 'Kanu') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '552'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-8-25 04:00:01+01','2020-8-25 05:00:01+01', '380', (SELECT id FROM boat WHERE name = 'GummiPaddel'), (SELECT id FROM discipline WHERE name = 'Kanupolo') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '380'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-11-20 04:00:01+01','2020-11-20 05:00:01+01', '319', (SELECT id FROM boat WHERE name = 'Der einsame Windfang'), (SELECT id FROM discipline WHERE name = 'Windsurfen') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '319'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-9-15 04:00:01+01','2020-9-15 05:00:01+01', '661', (SELECT id FROM boat WHERE name = 'RadialSystem'), (SELECT id FROM discipline WHERE name = 'Stand-Up-Paddling') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '661'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-12-26 04:00:01+01','2020-12-26 05:00:01+01', '126', (SELECT id FROM boat WHERE name = 'Rudi2'), (SELECT id FROM discipline WHERE name = 'Segeln') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '126'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-9-18 04:00:01+01','2020-9-18 05:00:01+01', '336', (SELECT id FROM boat WHERE name = 'FiddelKatzo'), (SELECT id FROM discipline WHERE name = 'Rudern') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '336'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-6-8 04:00:01+01','2020-6-8 05:00:01+01', '121', (SELECT id FROM boat WHERE name = 'Maxi2000'), (SELECT id FROM discipline WHERE name = 'Kanu') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '121'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-11-24 04:00:01+01','2020-11-24 05:00:01+01', '242', (SELECT id FROM boat WHERE name = 'Maxi1'), (SELECT id FROM discipline WHERE name = 'Kanupolo') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '242'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-1-18 04:00:01+01','2020-1-18 05:00:01+01', '394', (SELECT id FROM boat WHERE name = 'Der Erste'), (SELECT id FROM discipline WHERE name = 'Windsurfen') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '394'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-2-27 04:00:01+01','2020-2-27 05:00:01+01', '183', (SELECT id FROM boat WHERE name = 'GummiPaddel'), (SELECT id FROM discipline WHERE name = 'Stand-Up-Paddling') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '183'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-2-11 04:00:01+01','2020-2-11 05:00:01+01', '511', (SELECT id FROM boat WHERE name = 'Der einsame Windfang'), (SELECT id FROM discipline WHERE name = 'Segeln') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '511'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-7-25 04:00:01+01','2020-7-25 05:00:01+01', '427', (SELECT id FROM boat WHERE name = 'RadialSystem'), (SELECT id FROM discipline WHERE name = 'Rudern') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '427'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-8-7 04:00:01+01','2020-8-7 05:00:01+01', '881', (SELECT id FROM boat WHERE name = 'Rudi2'), (SELECT id FROM discipline WHERE name = 'Kanu') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '881'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-6-5 04:00:01+01','2020-6-5 05:00:01+01', '622', (SELECT id FROM boat WHERE name = 'FiddelKatzo'), (SELECT id FROM discipline WHERE name = 'Kanupolo') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '622'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-4-8 04:00:01+01','2020-4-8 05:00:01+01', '698', (SELECT id FROM boat WHERE name = 'Maxi2000'), (SELECT id FROM discipline WHERE name = 'Windsurfen') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '698'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-12-1 04:00:01+01','2020-12-1 05:00:01+01', '860', (SELECT id FROM boat WHERE name = 'Maxi1'), (SELECT id FROM discipline WHERE name = 'Stand-Up-Paddling') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '860'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-1-3 04:00:01+01','2020-1-3 05:00:01+01', '815', (SELECT id FROM boat WHERE name = 'Der Erste'), (SELECT id FROM discipline WHERE name = 'Segeln') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '815'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-12-18 04:00:01+01','2020-12-18 05:00:01+01', '475', (SELECT id FROM boat WHERE name = 'GummiPaddel'), (SELECT id FROM discipline WHERE name = 'Rudern') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '475'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-12-9 04:00:01+01','2020-12-9 05:00:01+01', '815', (SELECT id FROM boat WHERE name = 'Der einsame Windfang'), (SELECT id FROM discipline WHERE name = 'Kanu') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '815'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-10-13 04:00:01+01','2020-10-13 05:00:01+01', '230', (SELECT id FROM boat WHERE name = 'RadialSystem'), (SELECT id FROM discipline WHERE name = 'Kanupolo') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '230'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-4-2 04:00:01+01','2020-4-2 05:00:01+01', '262', (SELECT id FROM boat WHERE name = 'Rudi2'), (SELECT id FROM discipline WHERE name = 'Windsurfen') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '262'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-8-22 04:00:01+01','2020-8-22 05:00:01+01', '101', (SELECT id FROM boat WHERE name = 'FiddelKatzo'), (SELECT id FROM discipline WHERE name = 'Stand-Up-Paddling') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '101'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-6-7 04:00:01+01','2020-6-7 05:00:01+01', '806', (SELECT id FROM boat WHERE name = 'Maxi2000'), (SELECT id FROM discipline WHERE name = 'Segeln') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '806'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-11-17 04:00:01+01','2020-11-17 05:00:01+01', '719', (SELECT id FROM boat WHERE name = 'Maxi1'), (SELECT id FROM discipline WHERE name = 'Rudern') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '719'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-3-11 04:00:01+01','2020-3-11 05:00:01+01', '237', (SELECT id FROM boat WHERE name = 'Der Erste'), (SELECT id FROM discipline WHERE name = 'Kanu') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '237'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-8-26 04:00:01+01','2020-8-26 05:00:01+01', '317', (SELECT id FROM boat WHERE name = 'GummiPaddel'), (SELECT id FROM discipline WHERE name = 'Kanupolo') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '317'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-1-24 04:00:01+01','2020-1-24 05:00:01+01', '514', (SELECT id FROM boat WHERE name = 'Der einsame Windfang'), (SELECT id FROM discipline WHERE name = 'Windsurfen') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '514'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-6-11 04:00:01+01','2020-6-11 05:00:01+01', '508', (SELECT id FROM boat WHERE name = 'RadialSystem'), (SELECT id FROM discipline WHERE name = 'Stand-Up-Paddling') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '508'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-11-13 04:00:01+01','2020-11-13 05:00:01+01', '396', (SELECT id FROM boat WHERE name = 'Rudi2'), (SELECT id FROM discipline WHERE name = 'Segeln') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '396'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-12-12 04:00:01+01','2020-12-12 05:00:01+01', '837', (SELECT id FROM boat WHERE name = 'FiddelKatzo'), (SELECT id FROM discipline WHERE name = 'Rudern') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '837'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-8-7 04:00:01+01','2020-8-7 05:00:01+01', '304', (SELECT id FROM boat WHERE name = 'Maxi2000'), (SELECT id FROM discipline WHERE name = 'Kanu') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '304'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-6-10 04:00:01+01','2020-6-10 05:00:01+01', '401', (SELECT id FROM boat WHERE name = 'Maxi1'), (SELECT id FROM discipline WHERE name = 'Kanupolo') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '401'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-4-11 04:00:01+01','2020-4-11 05:00:01+01', '632', (SELECT id FROM boat WHERE name = 'Der Erste'), (SELECT id FROM discipline WHERE name = 'Windsurfen') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '632'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-5-23 04:00:01+01','2020-5-23 05:00:01+01', '483', (SELECT id FROM boat WHERE name = 'GummiPaddel'), (SELECT id FROM discipline WHERE name = 'Stand-Up-Paddling') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '483'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-4-2 04:00:01+01','2020-4-2 05:00:01+01', '231', (SELECT id FROM boat WHERE name = 'Der einsame Windfang'), (SELECT id FROM discipline WHERE name = 'Segeln') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '231'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-4-1 04:00:01+01','2020-4-1 05:00:01+01', '704', (SELECT id FROM boat WHERE name = 'RadialSystem'), (SELECT id FROM discipline WHERE name = 'Rudern') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '704'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-6-11 04:00:01+01','2020-6-11 05:00:01+01', '779', (SELECT id FROM boat WHERE name = 'Rudi2'), (SELECT id FROM discipline WHERE name = 'Kanu') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '779'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-3-10 04:00:01+01','2020-3-10 05:00:01+01', '593', (SELECT id FROM boat WHERE name = 'FiddelKatzo'), (SELECT id FROM discipline WHERE name = 'Kanupolo') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '593'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-1-16 04:00:01+01','2020-1-16 05:00:01+01', '112', (SELECT id FROM boat WHERE name = 'Maxi2000'), (SELECT id FROM discipline WHERE name = 'Windsurfen') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '112'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-8-13 04:00:01+01','2020-8-13 05:00:01+01', '449', (SELECT id FROM boat WHERE name = 'Maxi1'), (SELECT id FROM discipline WHERE name = 'Stand-Up-Paddling') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '449'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-12-4 04:00:01+01','2020-12-4 05:00:01+01', '361', (SELECT id FROM boat WHERE name = 'Der Erste'), (SELECT id FROM discipline WHERE name = 'Segeln') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '361'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-7-27 04:00:01+01','2020-7-27 05:00:01+01', '543', (SELECT id FROM boat WHERE name = 'GummiPaddel'), (SELECT id FROM discipline WHERE name = 'Rudern') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '543'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-11-9 04:00:01+01','2020-11-9 05:00:01+01', '470', (SELECT id FROM boat WHERE name = 'Der einsame Windfang'), (SELECT id FROM discipline WHERE name = 'Kanu') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '470'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-11-24 04:00:01+01','2020-11-24 05:00:01+01', '162', (SELECT id FROM boat WHERE name = 'RadialSystem'), (SELECT id FROM discipline WHERE name = 'Kanupolo') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '162'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-9-22 04:00:01+01','2020-9-22 05:00:01+01', '210', (SELECT id FROM boat WHERE name = 'Rudi2'), (SELECT id FROM discipline WHERE name = 'Windsurfen') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '210'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-10-16 04:00:01+01','2020-10-16 05:00:01+01', '426', (SELECT id FROM boat WHERE name = 'FiddelKatzo'), (SELECT id FROM discipline WHERE name = 'Stand-Up-Paddling') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '426'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-6-10 04:00:01+01','2020-6-10 05:00:01+01', '145', (SELECT id FROM boat WHERE name = 'Maxi2000'), (SELECT id FROM discipline WHERE name = 'Segeln') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '145'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-6-14 04:00:01+01','2020-6-14 05:00:01+01', '865', (SELECT id FROM boat WHERE name = 'Maxi1'), (SELECT id FROM discipline WHERE name = 'Rudern') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '865'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-1-26 04:00:01+01','2020-1-26 05:00:01+01', '782', (SELECT id FROM boat WHERE name = 'Der Erste'), (SELECT id FROM discipline WHERE name = 'Kanu') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '782'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-6-25 04:00:01+01','2020-6-25 05:00:01+01', '123', (SELECT id FROM boat WHERE name = 'GummiPaddel'), (SELECT id FROM discipline WHERE name = 'Kanupolo') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '123'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-9-26 04:00:01+01','2020-9-26 05:00:01+01', '425', (SELECT id FROM boat WHERE name = 'Der einsame Windfang'), (SELECT id FROM discipline WHERE name = 'Windsurfen') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '425'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-10-6 04:00:01+01','2020-10-6 05:00:01+01', '107', (SELECT id FROM boat WHERE name = 'RadialSystem'), (SELECT id FROM discipline WHERE name = 'Stand-Up-Paddling') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '107'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-1-11 04:00:01+01','2020-1-11 05:00:01+01', '819', (SELECT id FROM boat WHERE name = 'Rudi2'), (SELECT id FROM discipline WHERE name = 'Segeln') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '819'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-10-27 04:00:01+01','2020-10-27 05:00:01+01', '600', (SELECT id FROM boat WHERE name = 'FiddelKatzo'), (SELECT id FROM discipline WHERE name = 'Rudern') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '600'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-12-5 04:00:01+01','2020-12-5 05:00:01+01', '179', (SELECT id FROM boat WHERE name = 'Maxi2000'), (SELECT id FROM discipline WHERE name = 'Kanu') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '179'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-10-6 04:00:01+01','2020-10-6 05:00:01+01', '955', (SELECT id FROM boat WHERE name = 'Maxi1'), (SELECT id FROM discipline WHERE name = 'Kanupolo') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '955'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-8-20 04:00:01+01','2020-8-20 05:00:01+01', '625', (SELECT id FROM boat WHERE name = 'Der Erste'), (SELECT id FROM discipline WHERE name = 'Windsurfen') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '625'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-11-17 04:00:01+01','2020-11-17 05:00:01+01', '172', (SELECT id FROM boat WHERE name = 'GummiPaddel'), (SELECT id FROM discipline WHERE name = 'Stand-Up-Paddling') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '172'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-6-8 04:00:01+01','2020-6-8 05:00:01+01', '175', (SELECT id FROM boat WHERE name = 'Der einsame Windfang'), (SELECT id FROM discipline WHERE name = 'Segeln') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '175'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-4-22 04:00:01+01','2020-4-22 05:00:01+01', '733', (SELECT id FROM boat WHERE name = 'RadialSystem'), (SELECT id FROM discipline WHERE name = 'Rudern') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '733'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-9-4 04:00:01+01','2020-9-4 05:00:01+01', '340', (SELECT id FROM boat WHERE name = 'Rudi2'), (SELECT id FROM discipline WHERE name = 'Kanu') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '340'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-7-1 04:00:01+01','2020-7-1 05:00:01+01', '571', (SELECT id FROM boat WHERE name = 'FiddelKatzo'), (SELECT id FROM discipline WHERE name = 'Kanupolo') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '571'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-6-19 04:00:01+01','2020-6-19 05:00:01+01', '737', (SELECT id FROM boat WHERE name = 'Maxi2000'), (SELECT id FROM discipline WHERE name = 'Windsurfen') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '737'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-10-27 04:00:01+01','2020-10-27 05:00:01+01', '494', (SELECT id FROM boat WHERE name = 'Maxi1'), (SELECT id FROM discipline WHERE name = 'Stand-Up-Paddling') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '494'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-2-7 04:00:01+01','2020-2-7 05:00:01+01', '948', (SELECT id FROM boat WHERE name = 'Der Erste'), (SELECT id FROM discipline WHERE name = 'Segeln') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '948'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-7-10 04:00:01+01','2020-7-10 05:00:01+01', '616', (SELECT id FROM boat WHERE name = 'GummiPaddel'), (SELECT id FROM discipline WHERE name = 'Rudern') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '616'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-12-22 04:00:01+01','2020-12-22 05:00:01+01', '909', (SELECT id FROM boat WHERE name = 'Der einsame Windfang'), (SELECT id FROM discipline WHERE name = 'Kanu') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '909'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-12-8 04:00:01+01','2020-12-8 05:00:01+01', '329', (SELECT id FROM boat WHERE name = 'RadialSystem'), (SELECT id FROM discipline WHERE name = 'Kanupolo') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '329'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-5-23 04:00:01+01','2020-5-23 05:00:01+01', '485', (SELECT id FROM boat WHERE name = 'Rudi2'), (SELECT id FROM discipline WHERE name = 'Windsurfen') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '485'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-2-20 04:00:01+01','2020-2-20 05:00:01+01', '638', (SELECT id FROM boat WHERE name = 'FiddelKatzo'), (SELECT id FROM discipline WHERE name = 'Stand-Up-Paddling') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '638'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-12-8 04:00:01+01','2020-12-8 05:00:01+01', '621', (SELECT id FROM boat WHERE name = 'Maxi2000'), (SELECT id FROM discipline WHERE name = 'Segeln') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '621'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-9-9 04:00:01+01','2020-9-9 05:00:01+01', '341', (SELECT id FROM boat WHERE name = 'Maxi1'), (SELECT id FROM discipline WHERE name = 'Rudern') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '341'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-5-2 04:00:01+01','2020-5-2 05:00:01+01', '534', (SELECT id FROM boat WHERE name = 'Der Erste'), (SELECT id FROM discipline WHERE name = 'Kanu') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '534'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-12-12 04:00:01+01','2020-12-12 05:00:01+01', '432', (SELECT id FROM boat WHERE name = 'GummiPaddel'), (SELECT id FROM discipline WHERE name = 'Kanupolo') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '432'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-7-4 04:00:01+01','2020-7-4 05:00:01+01', '393', (SELECT id FROM boat WHERE name = 'Der einsame Windfang'), (SELECT id FROM discipline WHERE name = 'Windsurfen') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '393'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-2-17 04:00:01+01','2020-2-17 05:00:01+01', '432', (SELECT id FROM boat WHERE name = 'RadialSystem'), (SELECT id FROM discipline WHERE name = 'Stand-Up-Paddling') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '432'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-12-14 04:00:01+01','2020-12-14 05:00:01+01', '373', (SELECT id FROM boat WHERE name = 'Rudi2'), (SELECT id FROM discipline WHERE name = 'Segeln') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '373'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-3-12 04:00:01+01','2020-3-12 05:00:01+01', '104', (SELECT id FROM boat WHERE name = 'FiddelKatzo'), (SELECT id FROM discipline WHERE name = 'Rudern') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '104'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-1-21 04:00:01+01','2020-1-21 05:00:01+01', '842', (SELECT id FROM boat WHERE name = 'Maxi2000'), (SELECT id FROM discipline WHERE name = 'Kanu') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '842'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-5-14 04:00:01+01','2020-5-14 05:00:01+01', '442', (SELECT id FROM boat WHERE name = 'Maxi1'), (SELECT id FROM discipline WHERE name = 'Kanupolo') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '442'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-2-25 04:00:01+01','2020-2-25 05:00:01+01', '701', (SELECT id FROM boat WHERE name = 'Der Erste'), (SELECT id FROM discipline WHERE name = 'Windsurfen') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '701'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-3-10 04:00:01+01','2020-3-10 05:00:01+01', '205', (SELECT id FROM boat WHERE name = 'GummiPaddel'), (SELECT id FROM discipline WHERE name = 'Stand-Up-Paddling') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '205'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-12-4 04:00:01+01','2020-12-4 05:00:01+01', '998', (SELECT id FROM boat WHERE name = 'Der einsame Windfang'), (SELECT id FROM discipline WHERE name = 'Segeln') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '998'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-4-2 04:00:01+01','2020-4-2 05:00:01+01', '854', (SELECT id FROM boat WHERE name = 'RadialSystem'), (SELECT id FROM discipline WHERE name = 'Rudern') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '854'));

INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT '2020-10-6 04:00:01+01','2020-10-6 05:00:01+01', '586', (SELECT id FROM boat WHERE name = 'Rudi2'), (SELECT id FROM discipline WHERE name = 'Kanu') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = '586'));



