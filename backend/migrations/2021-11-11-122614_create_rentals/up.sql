CREATE TABLE rentals (
  id SERIAL PRIMARY KEY,
  vehicle_id TEXT NOT NULL, 
  rentee_id TEXT NOT NULL,
  comments TEXT NOT NULl,
  returned BOOLEAN NOT NULL DEFAULT FALSE
)
