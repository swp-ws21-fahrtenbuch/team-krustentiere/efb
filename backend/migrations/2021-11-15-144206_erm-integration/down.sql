-- This file should undo anything in `up.sql`
DROP TABLE IF EXISTS person;
DROP TABLE IF EXISTS boat_user;
DROP TABLE IF EXISTS boat_type_to_discipline;
DROP TABLE IF EXISTS borrow_entry;
DROP TABLE IF EXISTS discipline;
DROP TABLE IF EXISTS boat;
DROP TABLE IF EXISTS boat_type;



