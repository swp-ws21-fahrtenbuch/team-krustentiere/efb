-- Your SQL goes here

CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE IF NOT EXISTS boat_type (
	id SERIAL PRIMARY KEY,
	min_persons INT NOT NULL,
	max_persons INT NOT NULL,
	name VARCHAR NOT NULL,
	deleted BOOLEAN NOT NULL DEFAULT 'FALSE'
);

CREATE TABLE IF NOT EXISTS boat(
	id SERIAL PRIMARY KEY,
	broken BOOLEAN NOT NULL DEFAULT 'FALSE',
	deleted BOOLEAN NOT NULL DEFAULT 'FALSE',
	name VARCHAR NOT NULL ,
	damage_reports VARCHAR,
	broken_counter INTEGER NOT NULL DEFAULT 0, 
	damage_counter INTEGER NOT NULL DEFAULT 0,
	boat_type_id INTEGER NOT NULL REFERENCES boat_type (id)
);

CREATE TABLE IF NOT EXISTS discipline (
	id SERIAL PRIMARY KEY,
	name VARCHAR NOT NULL,
	deleted BOOLEAN NOT NULL DEFAULT 'FALSE'
);


CREATE TABLE IF NOT EXISTS borrow_entry (
	id UUID DEFAULT uuid_generate_v4 () PRIMARY KEY,
	begintime TIMESTAMP WITH TIME ZONE NOT NULL,
	endtime TIMESTAMP WITH TIME ZONE,
	destination VARCHAR NOT NULL,
	damage_report VARCHAR,
	boat_id INTEGER NOT NULL,
	discipline_id INTEGER NOT NULL,
	FOREIGN KEY (boat_id) REFERENCES boat(id),
	FOREIGN KEY (discipline_id) REFERENCES discipline(id)
);
	

CREATE TABLE IF NOT EXISTS boat_type_to_discipline (
	boat_type_id INTEGER REFERENCES boat_type (id),
	discipline_id INTEGER REFERENCES discipline (id),
	PRIMARY KEY (boat_type_id, discipline_id)
);




CREATE TABLE IF NOT EXISTS boat_user (
	surname VARCHAR NOT NULL,
	name VARCHAR NOT NULL,
	phone_number VARCHAR,
	email VARCHAR, 
	street VARCHAR,
	street_number VARCHAR,
	plz INT,
	city VARCHAR, 
	borrow_entry_id UUID REFERENCES borrow_entry (id),
	PRIMARY KEY (borrow_entry_id, surname, name)
);


CREATE TABLE IF NOT EXISTS person (
	id SERIAL PRIMARY KEY,
	surname VARCHAR NOT NULL,
	name VARCHAR NOT NULL,
	username VARCHAR NOT NULL,
	email VARCHAR NOT NULL,
	password VARCHAR NOT NULL,
	login_session VARCHAR,
	role_flag SMALLINT NOT NULL
);








