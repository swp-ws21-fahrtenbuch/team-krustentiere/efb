use crate::common::mapper::map_boat_type;
use crate::database::models::{
    Boat, BoatTypeFE, BoatTypeFilter, BorrowEntry, BorrowEntryWithUsers, Discipline, NewBoat,
    NewDiscipline, NewUpdateBoatTypeFE, UpdateBoat, UpdateDiscipline,
};
use crate::database::repository::{
    create_boat, create_boat_type, create_boat_type_to_discipline, create_discipline,
    delete_boat_type_to_discipline, find_matching_boat_types, find_selected_boats, get_all_entries,
    get_all_entries_with_users, get_boat_admin_helper, get_boat_types_by_filter,
    get_disciplines_admin, update_boat, update_boat_type, update_discipline,
    validate_unique_discipline_name,
};

use crate::DbConnection;
use rocket::http::Status;
use rocket::response::status;
use rocket::serde::json::Json;
use rocket::{get, patch, post};
use rocket_okapi::openapi;

use crate::auth::session::Session;

#[openapi(tag = "admin: discipline")]
#[get("/admin/disciplines?<deleted_flag>")]
pub async fn get_admin_disciplines(
    _session: Session,
    connection: DbConnection,
    deleted_flag: Option<bool>,
) -> Result<Json<Vec<Discipline>>, status::Custom<String>> {
    //For testing until swagger send jwt (Token from swagger login endpoint!):
    // curl -X 'GET' \
    //'http://localhost:8000/api/admin/disciplines?deleted_flag=false' \
    // -H 'accept: application/json' -H "Authentication: Bearer <Token>"

    //println!("session username: {}", session.username.to_string());
    //println!("session role_flag: {}", session.role_flag.to_string());

    let requested_disciplines = connection
        .run(move |c| get_disciplines_admin(&c, None, deleted_flag))
        .await;

    match requested_disciplines {
        Err(_) => Result::Err(status::Custom(
            Status::InternalServerError,
            String::from("Query for fetching disciplines failed"),
        )),
        Ok(data) => Result::Ok(Json(data)),
    }
}

#[openapi(tag = "admin: boat-type")]
#[get("/admin/boat-type/<id>")]
pub async fn get_boat_type_id_admin(
    connection: DbConnection,
    _session: Session,  
    id: i32) -> Result<Json<Vec<BoatTypeFE>>, status::Custom<String>> {
    
    //get boat type with according filter 
    let requested_boat_types_by_filter =  connection
        .run(move |c| { get_boat_types_by_filter(
            &c,
            BoatTypeFilter {
                discipline_id: Option::None,
                boat_type_id: Option::Some(id),
                deleted: Option::None,
            },
        )}).await;
    
    // return if error, otherwise pack boat-types and return them
    match requested_boat_types_by_filter {
        Err(_) => Result::Err(status::Custom(
            Status::InternalServerError,
            String::from("Query for fetching boat_types_by_filter failed"),
        )),
        Ok(boat_types_by_filter) => Result::Ok(Json(map_boat_type(boat_types_by_filter))),
    }
}

#[openapi(tag = "admin: boat-type")]
#[get("/admin/boat-types?<discipline_id>&<deleted_flag>")]
pub async fn get_boat_types_admin(
    _session: Session,
    connection: DbConnection,
    discipline_id: Option<i32>,
    deleted_flag: Option<bool>,
) -> Result<Json<Vec<BoatTypeFE>>, status::Custom<String>> {
    //get boat type with according filter
    let requested_boat_types_by_filter = connection
        .run(move |c| {
            get_boat_types_by_filter(
                &c,
                BoatTypeFilter {
                    discipline_id: discipline_id,
                    boat_type_id: Option::None,
                    deleted: deleted_flag,
                },
            )
        })
        .await;

    // return if error, otherwise pack boat-types and return them
    match requested_boat_types_by_filter {
        Err(_) => Result::Err(status::Custom(
            Status::InternalServerError,
            String::from("Query for fetching boat_types_by_filter failed"),
        )),
        Ok(boat_types_by_filter) => Result::Ok(Json(map_boat_type(boat_types_by_filter))),
    }
}

#[openapi(tag = "admin: boat-type")]
#[post("/admin/boat-types", data = "<new_boat_type_data>")]
pub async fn add_boat_types_admin(
    _session: Session,
    connection: DbConnection,
    new_boat_type_data: Json<NewUpdateBoatTypeFE>,
) -> Result<Json<BoatTypeFE>, status::Custom<String>> {
    let new_boat_type_data_copy = new_boat_type_data.clone();
    // insert new BoatType
    let maybe_inserted_boat_type = connection
        .run(move |c| {
            create_boat_type(
                &c,
                new_boat_type_data.min_persons,
                new_boat_type_data.max_persons,
                &new_boat_type_data.name,
                new_boat_type_data.deleted,
            )
        })
        .await;

    let inserted_boat_type = match maybe_inserted_boat_type {
        Err(error) => return Result::Err(error),
        Ok(data) => data,
    };

    let new_boat_type_data = new_boat_type_data_copy.clone();

    // insert new BoatTypeToDiscipline
    let requested_inserted_boat_type_to_discipline = connection
        .run(move |c| {
            create_boat_type_to_discipline(
                &c,
                inserted_boat_type.id,
                new_boat_type_data.discipline_id,
            )
        })
        .await;

    match requested_inserted_boat_type_to_discipline {
        Err(_) => Result::Err(status::Custom(
            Status::InternalServerError,
            String::from("Query for inserting boat_type failed"),
        )),
        Ok(inserted_boat_type_to_discipline) => Result::Ok(Json(BoatTypeFE {
            id: inserted_boat_type.id,
            min_persons: inserted_boat_type.min_persons,
            max_persons: inserted_boat_type.max_persons,
            name: inserted_boat_type.name,
            deleted: inserted_boat_type.deleted,
            discipline_id: inserted_boat_type_to_discipline.discipline_id,
        })),
    }
}

#[openapi(tag = "admin: boat-type")]
#[patch("/admin/boat-type/<id>", data = "<patch_boat_type_data>")]
pub async fn patch_boat_type_admin(
    _session: Session,
    connection: DbConnection,
    id: i32,
    patch_boat_type_data: Json<NewUpdateBoatTypeFE>,
) -> Result<Json<BoatTypeFE>, status::Custom<String>> {
    //preparation
    let patch_boat_type_data_copy = patch_boat_type_data.clone();

    //get boat type with according filter
    let requested_boat_types_by_filter = connection
        .run(move |c| {
            get_boat_types_by_filter(
                &c,
                BoatTypeFilter {
                    discipline_id: Option::None,
                    boat_type_id: Option::Some(id),
                    deleted: Option::None,
                },
            )
        })
        .await;

    //call map_boat_type even if Error occured
    let current_boat_types = match requested_boat_types_by_filter {
        Err(_) => map_boat_type(Vec::new()),
        Ok(boat_types_by_filter) => map_boat_type(boat_types_by_filter),
    };

    // check if fitting boat_type was returned and put in optional if it was
    let mut current_boat_type = Option::None;
    if current_boat_types.len() as i32 == 1 {
        current_boat_type = Some(current_boat_types[0].clone());
    }

    match current_boat_type {
        Some(boat_type) => {
            // update BoatType
            let requested_updated_boat_type = connection
                .run(move |c| {
                    update_boat_type(
                        &c,
                        id,
                        patch_boat_type_data.min_persons,
                        patch_boat_type_data.max_persons,
                        &patch_boat_type_data.name,
                        patch_boat_type_data.deleted,
                    )
                })
                .await;

            let patch_boat_type_data = patch_boat_type_data_copy.clone();

            // if discipline_id is different boat_type_to_disciplne table must be updated
            let maybe_error;
            if boat_type.discipline_id != patch_boat_type_data.discipline_id {
                // update BoatTypeToDiscipline
                connection
                    .run(move |c| delete_boat_type_to_discipline(&c, id, boat_type.discipline_id))
                    .await;
                maybe_error = connection
                    .run(move |c| {
                        create_boat_type_to_discipline(&c, id, patch_boat_type_data.discipline_id)
                    })
                    .await;
                if let Err(_) = maybe_error {
                    return Result::Err(status::Custom(
                        Status::InternalServerError,
                        String::from("Query for updating boat failed"),
                    ));
                };
            }

            match requested_updated_boat_type {
                Err(_) => Result::Err(status::Custom(
                    Status::InternalServerError,
                    String::from("Query for updating boat failed"),
                )),
                Ok(updated_boat_type) => Result::Ok(Json(BoatTypeFE {
                    id: updated_boat_type.id,
                    min_persons: updated_boat_type.min_persons,
                    max_persons: updated_boat_type.max_persons,
                    name: updated_boat_type.name,
                    deleted: updated_boat_type.deleted,
                    discipline_id: patch_boat_type_data.discipline_id,
                })),
            }
        }
        None => Result::Err(status::Custom(
            Status::InternalServerError,
            String::from("Query failed for getting boat_types by filter, maybe wrong ID?"),
        )),
    }
}

#[openapi(tag = "admin: discipline")]
#[post("/admin/disciplines", data = "<new_discipline>")]
pub async fn add_discipline_admin(
    _session: Session,
    connection: DbConnection,
    new_discipline: Json<NewDiscipline>,
) -> Result<Json<Discipline>, status::Custom<String>> {
    /* - checks if disciplines is new*/

    connection
        .run(move |c| {
            if validate_unique_discipline_name(&c, &new_discipline.name) {
                // insert new discipline
                let discipline = create_discipline(&c, &new_discipline.name);

                match discipline {
                    Err(error) => Result::Err(error),
                    Ok(data) => Result::Ok(Json(data),)
                }
            } else {
                return Result::Err(status::Custom(Status::BadRequest, String::from("A discipline with that name exists already")));
            }
        })
        .await
}

#[openapi(tag = "admin: discipline")]
#[get("/admin/discipline/<discipline_id>")]
pub async fn get_discipline_admin(
    _session: Session,
    connection: DbConnection,
    discipline_id: i32,
) -> Result<Json<Discipline>, status::Custom<String>> {
    let selected_disciplines = connection
        .run(move |c| get_disciplines_admin(&c, Some(discipline_id), None))
        .await;

    match selected_disciplines {
        Err(_) => Result::Err(status::Custom(
            Status::InternalServerError,
            String::from("Query for fetching disciplines failed"),
        )),
        Ok(data) => {
            if data.len() as i32 == 1 {
                Result::Ok(Json(data[0].clone()))
            } else {
                Result::Err(status::Custom(
                    Status::InternalServerError,
                    String::from("Query for fetching disciplines failed"),
                ))
            }
        }
    }
}

#[openapi(tag = "admin: discipline")]
#[patch("/admin/discipline/<discipline_id>", data = "<patch_discipline_data>")]
pub async fn patch_discipline_admin(
    _session: Session,
    connection: DbConnection,
    discipline_id: i32,
    patch_discipline_data: Json<UpdateDiscipline>,
) -> Result<Json<Discipline>, status::Custom<String>> {
    let updated_discipline = connection
        .run(move |c| {
            update_discipline(
                discipline_id,
                &patch_discipline_data.name,
                patch_discipline_data.deleted,
                &c,
            )
        })
        .await;

    match updated_discipline {
        Err(error) => Result::Err(error),
        Ok(data) => Result::Ok(Json(data)),
    }
}

#[openapi(tag = "admin: boat")]
#[get("/admin/boats?<discipline_id>&<deleted_flag>&<broken_flag>&<orphan_flag>")]
pub async fn get_boats_admin(
    _session: Session,
    connection: DbConnection,
    discipline_id: Option<Vec<i32>>,
    deleted_flag: Option<bool>,
    broken_flag: Option<bool>,
    orphan_flag: Option<bool>,
) -> Result<Json<Vec<Boat>>, status::Custom<String>> {
    //find all boat_types that can be used based on the given RequestFilter option
    let requested_possible_boat_types = connection
        .run(move |c| find_matching_boat_types(&c, discipline_id))
        .await;

    match requested_possible_boat_types {
        Err(_) => Result::Err(status::Custom(
            Status::InternalServerError,
            String::from("Query error in finding matching boat_types"),
        )),
        Ok(possible_boat_types) => match connection
            .run(move |c| {
                find_selected_boats(
                    &c,
                    deleted_flag,
                    broken_flag,
                    orphan_flag,
                    possible_boat_types,
                )
            })
            .await
        {
            Err(_) => Result::Err(status::Custom(
                Status::InternalServerError,
                String::from("Query error in fetching boats with flags"),
            )),
            Ok(results) => Result::Ok(Json(results)),
        },
    }
}

#[openapi(tag = "admin: boat")]
#[post("/admin/boats", data = "<new_boat_data>")]
pub async fn add_boat_admin(
    _session: Session,
    connection: DbConnection,
    new_boat_data: Json<NewBoat>,
) -> Result<Json<Boat>, status::Custom<String>> {
    let result = connection
        .run(move |c| create_boat(&c, &new_boat_data.name, new_boat_data.boat_type_id))
        .await;

    match result {
        Err(error) => Result::Err(error),
        Ok(data) => Result::Ok(Json(data)),
    }
}

#[openapi(tag = "admin: borrow-entry")]
#[get("/admin/entry-users?<show_active>")]
pub async fn get_entries_with_users_admin(
    _session: Session,
    show_active: Option<bool>,
    connection: DbConnection,
) -> Result<Json<Vec<BorrowEntryWithUsers>>, status::Custom<String>> {
    match connection
        .run(move |c| get_all_entries_with_users(&c, show_active))
        .await
    {
        Err(error) => Result::Err(error),
        Ok(data) => Result::Ok(Json(data)),
    }
}

#[openapi(tag = "admin: borrow-entry")]
#[get("/admin/entries")]
pub async fn get_entries_admin(
    _session: Session,
    connection: DbConnection,
) -> Result<Json<Vec<BorrowEntry>>, status::Custom<String>> {
    match connection.run(move |c| get_all_entries(&c)).await {
        Err(error) => Result::Err(error),
        Ok(data) => Result::Ok(Json(data)),
    }
}

#[openapi(tag = "admin: boat")]
#[patch("/admin/boat/<boat_id>", data = "<patch_boat_data>")]
pub async fn patch_boat_admin(
    _session: Session,
    connection: DbConnection,
    boat_id: i32,
    patch_boat_data: Json<UpdateBoat>,
) -> Result<Json<Boat>, status::Custom<String>> {
    let updated_boat = connection
        .run(move |c| {
            update_boat(
                boat_id,
                &c,
                patch_boat_data.broken,
                patch_boat_data.deleted,
                patch_boat_data.name.clone(),
                patch_boat_data.damage_reports.clone(),
            )
        })
        .await;

    match updated_boat {
        Err(error) => Result::Err(error),
        Ok(data) => Result::Ok(Json(data)),
    }
}

#[openapi(tag = "admin: boat")]
#[get("/admin/boat/<boat_id>")]
pub async fn get_boat_admin(
    _session: Session, 
    connection: DbConnection, 
    boat_id: i32) -> Result<Json<Boat>, status::Custom<String>> {
    
    let boat = connection
            .run(move |c| get_boat_admin_helper(&c, boat_id))
            .await;
    
    match boat {
        Err(error) => Result::Err(error),
        Ok(data) => Result::Ok(Json(data)),
    }
    
}
