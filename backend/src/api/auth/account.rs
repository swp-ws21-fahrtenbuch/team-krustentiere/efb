use crate::database::models::{ChangePerson, NewPerson, PersonDTO};
use crate::database::repository;
use crate::DbConnection;
use bcrypt;
use rocket::response::status;
use rocket::{delete, get, patch, post, serde::json::Json};
use rocket_okapi::openapi;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};

use super::jwt::Token;
use super::session::Session;
use crate::auth::admin_session::AdminSession;
use rocket::http::Status;

#[derive(Debug, Serialize, Deserialize, JsonSchema, Clone)]
pub struct Credentials {
    pub username: String,
    pub password: String,
}

#[openapi(tag = "admin: account")]
#[delete("/admin/account/<account_id>")]
pub async fn delete_account(
    _admin_session: AdminSession,
    connection: DbConnection,
    account_id: i32,
) -> status::Custom<String> {
    let valid_delete = connection
        .run(move |c| repository::validate_person_deletion(&c, account_id))
        .await;
    if valid_delete {
        let return_value = connection
            .run(move |c| repository::delete_person(&c, account_id))
            .await;

        match return_value {
            Err(error) => error,
            Ok(data) => {
                if data < 1 {
                    return status::Custom(
                        Status::BadRequest,
                        String::from("Could not find person with this identifier"),
                    );
                } else if data > 1 {
                    return status::Custom(Status::InternalServerError, String::from("Deleted multiple person; Please contact the admin - This is a serious error"));
                } else {
                    return status::Custom(Status::Ok, String::from("Done!"));
                }
            }
        }
    } else {
        return status::Custom(
            Status::BadRequest,
            String::from("Cannot delete last admin."),
        );
    }
}

#[openapi(tag = "admin: account")]
#[get("/admin/account?<account_id>&<role_flag>")]
pub async fn get_accounts(
    session: Session,
    connection: DbConnection,
    account_id: Option<i32>,
    role_flag: Option<i16>,
) -> Result<Json<Vec<PersonDTO>>, status::Custom<String>> {
    let mut requested_get_person_result = Result::Err(status::Custom(
        Status::Unauthorized,
        String::from("Please login"),
    ));
    if session.role_flag == 1 {
        requested_get_person_result = connection
            .run(move |c| repository::get_person(&c, account_id, role_flag))
            .await;
    } else if account_id == None {
        requested_get_person_result = connection
            .run(move |c| repository::get_person(&c, Some(session.account_id), role_flag))
            .await;
    } else if account_id == Some(session.account_id) {
        requested_get_person_result = connection
            .run(move |c| repository::get_person(&c, account_id, role_flag))
            .await;
    }

    match requested_get_person_result {
        Err(error) => Result::Err(error),
        Ok(data) => Result::Ok(Json(data)),
    }
}

pub fn hash_password(password: Option<String>) -> Option<String> {
    // check for missing
    match password.clone() {
        None => None,
        Some(pwd) => {
            Some(bcrypt::hash(pwd.clone(), bcrypt::DEFAULT_COST).expect("Error hashing password"))
        }
    }
}

#[openapi(tag = "admin: account")]
#[patch("/admin/account/<account_id>", data = "<person_data>")]
pub async fn change_account_details(
    session: Session,
    connection: DbConnection,
    account_id: i32,
    person_data: Json<ChangePerson>,
) -> Result<Json<PersonDTO>, status::Custom<String>> {
    if session.role_flag == 1 || account_id == session.account_id {
        // Only 1 or 2 are allowed as role_flags:
        if person_data.role_flag > 2 || person_data.role_flag < 1 {
            return Result::Err(status::Custom(
                Status::BadRequest,
                String::from("role_flag wrong"),
            ));
        } else {
            let password = hash_password(person_data.password.clone());

            let role_flag = if account_id != session.account_id {
                person_data.role_flag
            } else {
                2
            };

            let return_value = connection
                .run(move |c| {
                    repository::change_person(
                        &c,
                        account_id,
                        person_data.surname.clone(),
                        person_data.name.clone(),
                        person_data.username.clone(),
                        person_data.email.clone(),
                        password,
                        role_flag,
                    )
                })
                .await;

            match return_value {
                Err(error) => Result::Err(error),
                Ok(data) => Result::Ok(Json(data)),
            }
        }
    } else {
        return Result::Err(status::Custom(
            Status::Unauthorized,
            String::from("Please login"),
        ));
    }
}

#[openapi(tag = "admin: account")]
#[post("/admin/account", data = "<person_data>")]
pub async fn create_account(
    _admin_session: AdminSession,
    connection: DbConnection,
    person_data: Json<NewPerson>,
) -> Result<Json<PersonDTO>, status::Custom<String>> {
    // Only 1 or 2 are allowed as role_flags:
    if person_data.role_flag > 2 || person_data.role_flag < 1 {
        return Result::Err(status::Custom(
            Status::BadRequest,
            String::from("Please login"),
        ));
    } else {
        let person_data = person_data.clone();

        // check username does not exists already
        let usernamecopy = person_data.username.clone();
        if !connection
            .run(move |c| repository::validate_unique_username(&c, &(usernamecopy)))
            .await
        {
            return Result::Err(status::Custom(
                Status::BadRequest,
                String::from("Username already existig, Query error"),
            ));
        }

        let pwd = bcrypt::hash(person_data.password, 12);
        match pwd {
            Err(_) => Result::Err(status::Custom(
                Status::BadRequest,
                String::from("Could not hash password"),
            )),
            Ok(hashed_password) => {
                let created_person = connection
                    .run(move |c| {
                        repository::create_person(
                            &c,
                            person_data.surname,
                            person_data.name,
                            person_data.username,
                            person_data.email,
                            hashed_password,
                            person_data.role_flag,
                        )
                    })
                    .await;

                match created_person {
                    Err(error) => Result::Err(error),
                    Ok(data) => Result::Ok(Json(data)),
                }
            }
        }
    }
}

#[openapi(tag = "account")]
#[post("/admin/login", data = "<login_credentials>")]
pub async fn login(
    connection: DbConnection,
    login_credentials: Json<Credentials>,
) -> Result<Json<Token>, status::Custom<&'static str>> {
    let credentials = login_credentials.into_inner();
    let credentials_copy = credentials.clone();

    // check if username is in database
    let person_exists = connection
        .run(move |c| repository::check_person_username_exists(&c, credentials.username))
        .await;

    match person_exists {
        Ok(person_exists_flag) => {
            if person_exists_flag {
                let credentials_copy_copy = credentials_copy.clone();

                // finde person
                let person = connection
                    .run(move |c| {
                        repository::find_person_by_username(&c, credentials_copy.username)
                    })
                    .await;

                match person {
                    Ok(selected_person) => {
                        // vergleiche passwort hash mit person hash
                        match bcrypt::verify(
                            credentials_copy_copy.password,
                            &selected_person.password,
                        ) {
                            Ok(verified) => {
                                if !verified {
                                    Err(status::Custom(
                                        Status::Unauthorized,
                                        "User is not authorized.",
                                    ))
                                } else {
                                    Ok(Json(Token::new(
                                        selected_person.id,
                                        credentials_copy_copy.username,
                                        selected_person.role_flag,
                                    )))
                                }
                            }
                            Err(_) => Err(status::Custom(
                                Status::InternalServerError,
                                "Error while evaluating user login.",
                            )),
                        }
                    }

                    Err(_) => Err(status::Custom(
                        Status::InternalServerError,
                        "Error while evaluating user login.",
                    )),
                }
            } else {
                Err(status::Custom(
                    Status::Unauthorized,
                    "User is not authorized.",
                ))
            }
        }

        Err(_) => Err(status::Custom(
            Status::InternalServerError,
            "Error while evaluating user login.",
        )),
    }
}
