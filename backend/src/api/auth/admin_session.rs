use super::oauth2;
use rocket::{
    http::Status,
    request::{FromRequest, Outcome, Request},
};
use rocket_okapi::request::OpenApiFromRequest;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};

use rocket_okapi::{gen::OpenApiGenerator, request::RequestHeaderInput};

pub const AUTH_HEADER_NAME: &str = "Authentication";
const SPACING: &str = " ";
const KEYWORD: &str = "Bearer";

#[derive(Debug, Serialize, Deserialize, JsonSchema)]
pub struct AdminSession {
    pub account_id: i32,
    pub username: String,
    pub role_flag: i16,
}

#[derive(Debug)]
pub enum AdminSessionError {
    Unauthorized,
    // Bad,
}

#[rocket::async_trait]
impl<'r> FromRequest<'r> for AdminSession {
    type Error = AdminSessionError;

    async fn from_request(request: &'r Request<'_>) -> Outcome<Self, Self::Error> {
        let encrypted_token = match request.headers().get_one(AUTH_HEADER_NAME) {
            Some(header) => {
                let mut parts = header.split(SPACING);
                if parts.next() != Some(KEYWORD) {
                    // return Outcome::Failure((Status::BadRequest, AdminSessionError::Bad));
                    return Outcome::Failure((
                        Status::Unauthorized,
                        AdminSessionError::Unauthorized,
                    ));
                }
                parts.next().unwrap()
            }
            None => {
                // // ONLY FOR TESTING!!!!!!
                // return
                // Outcome::Success(AdminSession {
                //     account_id: 1,
                //     username: "Heinz".to_string(),
                //     role_flag: 1,
                // });

                // return Outcome::Forward(());
                return Outcome::Failure((Status::Unauthorized, AdminSessionError::Unauthorized));
            }
        };

        let subject = match super::jwt::get_username(encrypted_token.to_string()) {
            Some(sub) => sub,
            None => {
                return Outcome::Failure((Status::Unauthorized, AdminSessionError::Unauthorized))
            }
        };

        let role_flag = match super::jwt::get_role_flag(encrypted_token.to_string()) {
            Some(role_flag) => {
                if role_flag != 1 {
                    return Outcome::Failure((
                        Status::Unauthorized,
                        AdminSessionError::Unauthorized,
                    ));
                } else {
                    role_flag
                }
            }
            None => {
                return Outcome::Failure((Status::Unauthorized, AdminSessionError::Unauthorized))
            }
        };

        let account_id = match super::jwt::get_account_id(encrypted_token.to_string()) {
            Some(account_id) => account_id,
            None => {
                return Outcome::Failure((Status::Unauthorized, AdminSessionError::Unauthorized))
            }
        };

        Outcome::Success(AdminSession {
            account_id: account_id,
            username: subject,
            role_flag,
        })
    }
}

impl<'a> OpenApiFromRequest<'a> for AdminSession {
    fn from_request_input(
        _gen: &mut OpenApiGenerator,
        _name: String,
        _required: bool,
    ) -> rocket_okapi::Result<RequestHeaderInput> {
        rocket_okapi::Result::Ok(oauth2::request_header_input())
    }
}
