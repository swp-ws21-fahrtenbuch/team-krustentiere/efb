use chrono::{Duration, Utc};
use jsonwebtoken::{decode, encode, DecodingKey, EncodingKey, Header, Validation};
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};
use std::ops::Add;

const SECRET: &str = "this is a secret";

#[derive(Debug, Serialize, Deserialize)]
struct Claims {
    pub account_id: i32,
    pub sub: String,
    pub roles: Vec<i16>,
    pub exp: u64,
}

#[derive(Debug, Serialize, Deserialize, JsonSchema)]
pub struct Token {
    access_token: String,
}

impl Token {
    pub fn new(account_id: i32, username: String, role_flag: i16) -> Self {
        let encoding_key = EncodingKey::from_secret(SECRET.as_ref());
        let header = Header::default();
        let claims = Claims {
            account_id: account_id,
            sub: username,
            exp: Utc::now().add(Duration::hours(1)).timestamp() as u64,
            roles: vec![role_flag],
        };

        Token {
            access_token: encode(&header, &claims, &encoding_key).unwrap(),
        }
    }
}

pub fn get_username(token: String) -> Option<String> {
    let decoding_key = DecodingKey::from_secret(SECRET.as_ref());
    let validation = Validation::default();
    let jwt = match decode::<Claims>(&token, &decoding_key, &validation) {
        Ok(token_data) => token_data,
        Err(_) => {
            return None;
        }
    };
    Some(jwt.claims.sub)
}


pub fn get_account_id(token: String) -> Option<i32> {
    let decoding_key = DecodingKey::from_secret(SECRET.as_ref());
    let validation = Validation::default();
    let jwt = match decode::<Claims>(&token, &decoding_key, &validation) {
        Ok(token_data) => token_data,
        Err(_) => {
            return None;
        }
    };
    Some(jwt.claims.account_id)
}


pub fn get_role_flag(token: String) -> Option<i16> {
    let decoding_key = DecodingKey::from_secret(SECRET.as_ref());
    let validation = Validation::default();
    let jwt = match decode::<Claims>(&token, &decoding_key, &validation) {
        Ok(token_data) => token_data,
        Err(_) => {
            return None;
        }
    };
    Some(jwt.claims.roles[0])
}

#[cfg(test)]
pub mod tests {
    #[test]
    pub fn create_and_resolve_token() {
        let original_username = "test_username".to_string();
        let original_role_flag: i16 = 2;
        let original_account_id: i32 = 11;
        let token = super::Token::new(original_account_id, original_username.clone(), original_role_flag);
        let decoded_username = super::get_username(token.access_token.clone());
        let decoded_role_flag = super::get_role_flag(token.access_token.clone());
        let decoded_account_id = super::get_account_id(token.access_token);

        assert_eq!(
            original_username,
            decoded_username.expect("username could not be decrypted")
        );

        assert_eq!(
            original_role_flag,
            decoded_role_flag.expect("role flag could not be decrypted")
        );

        assert_eq!(
            original_account_id,
            decoded_account_id.expect("account id could not be decrypted")
        );
    }
}
