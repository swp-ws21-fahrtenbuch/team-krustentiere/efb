use okapi::openapi3::{SecurityRequirement, SecurityScheme, SecuritySchemeData};
use rocket_okapi::request::RequestHeaderInput;

use super::admin_session::AUTH_HEADER_NAME;

const SECURITY_SCHEME_NAME: &str = "JWT Header Authentication";
const SECURITY_SCHEME_DESC: &str =
    "Insert Bearer plus the token received from login endpoint as authentication (Bearer {jwt_token})";
const API_KEY_LOCATION: &str = "header";

pub fn request_header_input() -> RequestHeaderInput {
    let security_scheme = SecurityScheme {
        description: Some(SECURITY_SCHEME_DESC.to_string()),
        data: SecuritySchemeData::ApiKey {
            name: AUTH_HEADER_NAME.to_string(),
            location: API_KEY_LOCATION.to_string(),
        },
        extensions: okapi::map! {},
    };
    let mut security_requirement = SecurityRequirement::new();
    security_requirement.insert(SECURITY_SCHEME_NAME.to_string(), vec![]);
    return RequestHeaderInput::Security(
        SECURITY_SCHEME_NAME.to_string(),
        security_scheme,
        security_requirement,
    );
}
