use crate::common::mapper::map_public_boat_type;
use crate::database::models::{
    BoatTypeFilter, BorrowEntry, BorrowEntryDTO, NewBorrowEntryUsers, PublicBoat, PublicBoatType,
    PublicDiscipline, UpdateBorrowEntryDTO,
};
use crate::database::repository::{
    copy_damage_report_to_boat, create_boat_user, create_borrow_entry, fetch_open_entries,
    find_matching_boat_types, get_boat_types_by_filter, get_borrow_entry_boatid, get_person,
    get_public_boats, get_public_disciplines, get_rented_boats, increase_boat_damage_counter,
    update_borrow_entry, validate_borrow_entry_dependencies, get_boat_admin_helper,check_if_entry_is_open,
};

use crate::mails::config_mail::MailConfig;

use crate::mails::mail_handler::{send_mail_to_boot_attendant, send_mail_to_user};

use crate::rocket_config::RocketConfig;
use crate::DbConnection;
use chrono::Utc;
use rocket::futures::future::join_all;
use rocket::http::Status;
use rocket::response::status;
use rocket::serde::json::Json;
use rocket::tokio::spawn;
use rocket::{get, patch, post};
use rocket_okapi::openapi;
use uuid::Uuid;

#[openapi(tag = "boat")]
#[get("/boats?<discipline_id>")]
pub async fn get_boats(
    connection: DbConnection,
    discipline_id: Option<Vec<i32>>,
) -> Result<Json<Vec<PublicBoat>>, status::Custom<String>> {
    //find all boat_types that can be used based on the given RequestFilter option
    let requested_possible_boat_types = connection
        .run(move |c| find_matching_boat_types(&c, discipline_id))
        .await;

    match requested_possible_boat_types {
        Err(_) => Result::Err(status::Custom(
            Status::InternalServerError,
            String::from("Query error in finding matching boat_types"),
        )),
        Ok(possible_boat_types) => match connection
            .run(move |c| get_public_boats(&c, possible_boat_types))
            .await
        {
            Ok(data) => Result::Ok(Json(data)),
            Err(_) => Result::Err(status::Custom(
                Status::InternalServerError,
                String::from("Query error in fetching matching boats"),
            )),
        },
    }
    //Return all found boats as PublicBoat in Json
}

#[openapi(tag = "boat-type")]
#[get("/boat-type/<id>")]
pub async fn get_boat_type_by_id(
    connection: DbConnection,
    id: i32,
) -> Result<Json<Vec<PublicBoatType>>, status::Custom<String>> {
    let requested_boat_types_by_filter = connection
        .run(move |c| {
            get_boat_types_by_filter(
                &c,
                BoatTypeFilter {
                    discipline_id: Option::None,
                    boat_type_id: Option::Some(id),
                    deleted: Option::Some(false),
                },
            )
        })
        .await;

    match requested_boat_types_by_filter {
        Err(_) => Result::Err(status::Custom(
            Status::InternalServerError,
            String::from("Query for fetching boat_types_by_filter failed"),
        )),
        Ok(boat_types_by_filter) => Result::Ok(Json(map_public_boat_type(boat_types_by_filter))),
    }
}

#[openapi(tag = "boat-type")]
#[get("/boat-types?<discipline_id>")]
pub async fn get_boat_types(
    connection: DbConnection,
    discipline_id: Option<i32>,
) -> Result<Json<Vec<PublicBoatType>>, status::Custom<String>> {
    let requested_boat_types_by_filter = connection
        .run(move |c| {
            get_boat_types_by_filter(
                &c,
                BoatTypeFilter {
                    discipline_id: discipline_id,
                    boat_type_id: Option::None,
                    deleted: Option::Some(false),
                },
            )
        })
        .await;

    match requested_boat_types_by_filter {
        Err(_) => Result::Err(status::Custom(
            Status::InternalServerError,
            String::from("Query for fetching boat_types_by_filter failed"),
        )),
        Ok(boat_types_by_filter) => Result::Ok(Json(map_public_boat_type(boat_types_by_filter))),
    }
}

#[openapi(tag = "discipline")]
#[get("/disciplines")]
pub async fn get_disciplines(
    connection: DbConnection,
) -> Result<Json<Vec<PublicDiscipline>>, status::Custom<String>> {
    match connection.run(move |c| get_public_disciplines(&c)).await {
        Err(_) => Result::Err(status::Custom(
            Status::InternalServerError,
            String::from("Disciplines query failed"),
        )),
        Ok(data) => Result::Ok(Json(data)),
    }
}

#[openapi(tag = "borrow-entry")]
#[get("/entries")]
pub async fn get_entries(
    connection: DbConnection,
) -> Result<Json<Vec<BorrowEntryDTO>>, status::Custom<String>> {
    match connection.run(move |c| fetch_open_entries(&c)).await {
        Err(_) => Result::Err(status::Custom(
            Status::InternalServerError,
            String::from("Entries query failed"),
        )),
        Ok(data) => Result::Ok(Json(data)),
    }
}

#[openapi(tag = "borrow-entry")]
#[get("/check-entry/<borrow_id>")]
pub async fn check_entry_for_return(
    connection: DbConnection,
    borrow_id: Uuid,
) -> Result<Json<bool>, status::Custom<String>> {
    match connection
        .run(move |c| check_if_entry_is_open(&c, borrow_id))
        .await {
            Err(error) => Result::Err(error),
            Ok(data) => Result::Ok(Json(data)),
        }
}



#[openapi(tag = "borrow-entry")]
#[patch("/entries/<uuid>", data = "<borrow_entry_endtime>")]
pub async fn patch_borrow_entry(
    connection: DbConnection,
    mail_info: MailConfig,
    rocket_config: RocketConfig,
    uuid: Uuid,
    borrow_entry_endtime: Json<UpdateBorrowEntryDTO>,
) -> Result<Json<BorrowEntry>, status::Custom<String>> {
    let requested_boat_id = connection
        .run(move |c| get_borrow_entry_boatid(&c, uuid))
        .await;

    let borrow_entry_endtime_copy = borrow_entry_endtime.clone();
    let boat_id;
    if let Ok(id) = requested_boat_id {
        boat_id = id;
    } else {
        return Result::Err(status::Custom(
            Status::InternalServerError,
            String::from("Query Error, fetching boat_id from borrow_entry failed"),
        ));
    }

    let requested_updated_borrow_entry = connection
        .run(move |c| {
            update_borrow_entry(
                uuid,
                &c,
                Option::Some(Utc::now()),
                borrow_entry_endtime.damage_report.clone(),
            )
        })
        .await;

    let updated_borrow_entry;
    if let Ok(entry) = requested_updated_borrow_entry {
        updated_borrow_entry = entry;
    } else {
        return Result::Err(status::Custom(
            Status::InternalServerError,
            String::from("Query Error, fetching borrow entry failed"),
        ));
    }

    let valid_borrow_entry = updated_borrow_entry.clone();

    if borrow_entry_endtime_copy.damage_report != "" {
        let mut unneeded_variable = connection
            .run(move |c| increase_boat_damage_counter(uuid, &c))
            .await;

        if let Err(error) = unneeded_variable {
            return Result::Err(error);
        }

        unneeded_variable = connection
            .run(move |c| {
                copy_damage_report_to_boat(
                    &c,
                    borrow_entry_endtime_copy.damage_report.to_string(),
                    boat_id,
                )
            })
            .await;
        if let Err(error) = unneeded_variable {
            return Result::Err(error);
        }

        let requested_boat_attendant_persons =
            connection.run(move |c| get_person(&c, None, Some(2))).await;
        let boat_attendant_persons;
        if let Ok(data) = requested_boat_attendant_persons {
            boat_attendant_persons = data;
        } else {
            return Result::Err(status::Custom(
                Status::InternalServerError,
                String::from("Query Error, get_person query failed"),
            ));
        }

        let req_rented_boat = connection
            .run(move |c| get_boat_admin_helper(&c, boat_id))
            .await;

        let rented_boat;
        if let Ok(data) = req_rented_boat {
            rented_boat = data;
        } else {
            return Result::Err(status::Custom(
                Status::InternalServerError,
                String::from("Query failed in get_boat_admin_helper"),
            ));
        }

        let mut futures = Vec::new();
        for i in 0..boat_attendant_persons.len() {
            let future = send_mail_to_boot_attendant(
                mail_info.clone(),
                rocket_config.clone(),
                boat_attendant_persons[i].email.clone(),
                updated_borrow_entry.clone(),
                rented_boat.name.clone(),
            );
            futures.push(future);
        }
        spawn(async move {
            join_all(futures).await;
        });
    }

    Result::Ok(Json(valid_borrow_entry))
}

#[openapi(tag = "borrow-entry")]
#[post("/entry", data = "<borrow_entry_users>")]
pub async fn add_borrow_entry(
    connection: DbConnection,
    mail_info: MailConfig,
    rocket_config: RocketConfig,
    borrow_entry_users: Json<NewBorrowEntryUsers>,
) -> Result<Json<BorrowEntry>, status::Custom<String>> {
    /*
    - destination must be set
    - first user must contain name and surname information
    - boat_id must match to discipline_id
    - boat can't be broken or deleted, boat_type can't be deleted, discipline can't be deleted
    - user count must match boat_type specifications (min_persons and max_persons)
    - boat can't be currently borrowed*/

    let boat_id_copy = borrow_entry_users.boat_id.clone();
    let borrow_entry_users_copy = borrow_entry_users.clone();

    let catch_error = connection
        .run(move |c| get_rented_boats(&c, Some(boat_id_copy)))
        .await;

    let boat_not_rented: bool;
    if let 0 = catch_error.len() {
        boat_not_rented = true;
    } else {
        return Result::Err(status::Custom(
            Status::InternalServerError,
            String::from("Query error for fetching borrowed boats"),
        ));
    }

    let valid_return = connection
        .run(move |c| {
            validate_borrow_entry_dependencies(
                &c,
                borrow_entry_users.boat_id,
                borrow_entry_users.discipline_id,
                borrow_entry_users.boat_users.len() as i32,
            )
        })
        .await;

    if borrow_entry_users_copy.destination != ""
        && borrow_entry_users_copy.boat_users[0].surname != ""
        && borrow_entry_users_copy.boat_users[0].name != ""
        && boat_not_rented
        && valid_return
    {
        let borrow_entry_users = borrow_entry_users_copy.clone();
        // insert new borrow_entry
        let requested_borrow_entry = connection
            .run(move |c| {
                create_borrow_entry(
                    &c,
                    Utc::now(),
                    None,
                    &borrow_entry_users.destination,
                    "",
                    borrow_entry_users.boat_id,
                    borrow_entry_users.discipline_id,
                )
            })
            .await;

        let borrow_entry;
        if let Ok(data) = requested_borrow_entry {
            borrow_entry = data;
        } else {
            return Result::Err(status::Custom(
                Status::InternalServerError,
                String::from("Query error for creating borrow_entry"),
            ));
        }

        // insert boat_users to current borrow_entry
        for i in 0..borrow_entry_users.boat_users.len() {
            let borrow_entry_users = borrow_entry_users_copy.clone();
            if borrow_entry_users.boat_users[i].surname.len() > 0 && borrow_entry_users.boat_users[i].name.len() > 0 {
                let catch_error = connection
                    .run(move |c| {
                        create_boat_user(
                            &c,
                            borrow_entry_users.boat_users[i].surname.clone(),
                            borrow_entry_users.boat_users[i].name.clone(),
                            borrow_entry_users.boat_users[i].email.clone(),
                            borrow_entry.id,
                        )
                    })
                .   await;
                if let Err(data) = catch_error {
                    return Result::Err(data);
                }
            }
        }

        if let Some(mail_address) = &borrow_entry_users.boat_users[0].email {
            send_mail_to_user(
                mail_info,
                rocket_config,
                mail_address,
                &borrow_entry,
                &borrow_entry_users.boat_users[0].name,
            )
            .await;
        };

        Result::Ok(Json(borrow_entry))
    } else {
        Result::Err(status::Custom(
            Status::BadRequest,
            String::from(
                "Potential mistakes: missing surname, missing name, boat already borrowed",
            ),
        ))
    }
}
