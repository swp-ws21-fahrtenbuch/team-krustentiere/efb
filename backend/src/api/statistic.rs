use crate::database::models::Statistic;
use crate::database::repository::select_statistic_data;
use crate::auth::session::Session;

use rocket::response::status;
use crate::DbConnection;
use chrono::prelude::*;
use rocket::get;
use rocket::serde::json::Json;
use rocket_okapi::openapi;

#[openapi(tag = "admin: statistic")]
#[get("/statistic?<filter_year>")]
pub async fn get_statistics(
    _session: Session,
    connection: DbConnection,
    filter_year: Option<i32>,
) -> Result<Json<Vec<Statistic>>, status::Custom<String>> {
    let filter_begin_year = match filter_year {
        Some(year) => Some(Utc.ymd(year, 1, 1).and_hms(1, 1, 1)),
        None => None,
    };
    let filter_end_year = match filter_year {
        Some(year) => Some(Utc.ymd(year, 12, 31).and_hms(23, 59, 59)),
        None => None,
    };

    
    let result = connection
        .run(move |c| select_statistic_data(&c, filter_begin_year, filter_end_year))
        .await;
    
    match result {
        Err(error) => Result::Err(error),
        Ok(data) => Result::Ok(Json(data)),
    }
    
}
