use crate::database::models::{BoatTypeFE, BoatUser, BoatUserFE, PublicBoatType, Person, PersonDTO};

pub fn person_to_persondto(data: Person) -> PersonDTO {
    PersonDTO {
        surname: data.surname,
        name: data.name,
        username: data.username,
        email: data.email,
        role_flag: data.role_flag,
        id: data.id,
    }
}

pub fn boat_user_to_boat_user_fe(data: BoatUser) -> BoatUserFE {
    BoatUserFE {
        surname: data.surname,
        name: data.name,
        email: data.email,
        borrow_entry_id: data.borrow_entry_id,
    }
}

pub fn map_public_boat_type(
    boat_types: Vec<(i32, i32, i32, String, bool, i32)>,
) -> Vec<PublicBoatType> {
    let mut result = Vec::new();

    //create Vec<PublicBoatType> out of unstructured fields
    for boat_type_element in boat_types.iter() {
        result.push(PublicBoatType {
            id: boat_type_element.0,
            min_persons: boat_type_element.1,
            max_persons: boat_type_element.2,
            name: boat_type_element.3.clone(),
            discipline_id: boat_type_element.5,
        });
    }

    return result;
}

pub fn map_boat_type(boat_types: Vec<(i32, i32, i32, String, bool, i32)>) -> Vec<BoatTypeFE> {
    let mut result = Vec::new();

    //create Vec<BoatType> out of unstructured fields
    for boat_type_element in boat_types.iter() {
        result.push(BoatTypeFE {
            id: boat_type_element.0,
            min_persons: boat_type_element.1,
            max_persons: boat_type_element.2,
            name: boat_type_element.3.clone(),
            deleted: boat_type_element.4,
            discipline_id: boat_type_element.5,
        });
    }

    return result;
}
