use crate::schema::boat;
use crate::schema::boat_type;
use crate::schema::boat_type_to_discipline;
use crate::schema::boat_user;
use crate::schema::borrow_entry;
use crate::schema::discipline;
use crate::schema::person;

use chrono;
use chrono::{DateTime, Utc};
use uuid::Uuid;

use diesel::*;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};

#[derive(JsonSchema, Queryable, Serialize)]
pub struct Person {
    pub id: i32,
    pub surname: String,
    pub name: String,
    pub username: String,
    pub email: String,
    pub password: String,
    pub login_session: Option<String>,
    pub role_flag: i16,
}

#[derive(AsChangeset, JsonSchema, Insertable, Deserialize, Serialize, Clone, Queryable)]
#[table_name = "person"]
pub struct NewPerson {
    pub surname: String,
    pub name: String,
    pub username: String,
    pub email: String,
    pub password: String,
    pub role_flag: i16,
}

#[derive(JsonSchema, Insertable, Deserialize, Serialize, Clone, Queryable)]
#[table_name = "person"]
pub struct ChangePerson {
    pub surname: String,
    pub name: String,
    pub username: String,
    pub email: String,
    pub password: Option<String>,
    pub role_flag: i16,
}

#[derive(AsChangeset, JsonSchema, Insertable, Deserialize, Serialize, Clone, Queryable)]
#[table_name = "person"]
pub struct UpdatePersonWithoutPW {
    pub surname: String,
    pub name: String,
    pub username: String,
    pub email: String,
    pub role_flag: i16,
}

#[derive(JsonSchema, Insertable, Deserialize, Serialize, Queryable)]
#[table_name = "person"]
pub struct PersonDTO {
    pub id: i32,
    pub surname: String,
    pub name: String,
    pub username: String,
    pub email: String,
    pub role_flag: i16,
}

#[derive(JsonSchema, Queryable, Serialize)]
pub struct BoatType {
    pub id: i32,
    pub min_persons: i32,
    pub max_persons: i32,
    pub name: String,
    pub deleted: bool,
}

#[derive(JsonSchema, Serialize, Deserialize, Clone)]
pub struct BoatTypeFE {
    pub id: i32,
    pub min_persons: i32,
    pub max_persons: i32,
    pub name: String,
    pub deleted: bool,
    pub discipline_id: i32,
}

#[derive(JsonSchema, Serialize, Deserialize, Clone)]
pub struct NewUpdateBoatTypeFE {
    pub min_persons: i32,
    pub max_persons: i32,
    pub name: String,
    pub deleted: bool,
    pub discipline_id: i32,
}

#[derive(JsonSchema, Queryable, Serialize, Clone)]
pub struct PublicBoatType {
    pub id: i32,
    pub min_persons: i32,
    pub max_persons: i32,
    pub name: String,
    pub discipline_id: i32,
}

#[derive(AsChangeset, JsonSchema, Insertable, Deserialize)]
#[table_name = "boat_type"]
pub struct NewUpdateBoatType<'a> {
    pub min_persons: i32,
    pub max_persons: i32,
    pub name: &'a str,
    pub deleted: bool,
}

#[derive(JsonSchema, Queryable, Serialize, Clone)]
pub struct Discipline {
    pub id: i32,
    pub name: String,
    pub deleted: bool,
}

#[derive(JsonSchema, Queryable, Serialize, Clone)]
pub struct PublicDiscipline {
    pub id: i32,
    pub name: String,
}

#[derive(JsonSchema, Insertable, Deserialize)]
#[table_name = "discipline"]
pub struct NewDiscipline {
    pub name: String,
}

#[derive(JsonSchema, AsChangeset, Deserialize, Insertable)]
#[table_name = "discipline"]
pub struct UpdateDiscipline {
    pub name: String,
    pub deleted: bool,
}

#[derive(JsonSchema, Queryable, Serialize)]
pub struct Boat {
    pub id: i32,
    pub broken: bool,
    pub deleted: bool,
    pub name: String,
    pub damage_reports: Option<String>,
    pub broken_counter: i32,
    pub damage_counter: i32,
    pub boat_type_id: i32,
}

#[derive(JsonSchema, Serialize, Deserialize)]
pub struct BoatTypeFilter {
    pub discipline_id: Option<i32>,

    #[serde(skip_deserializing)]
    pub boat_type_id: Option<i32>,
    pub deleted: Option<bool>,
}

#[derive(JsonSchema, Insertable, Deserialize, Serialize, Queryable, QueryableByName)]
#[table_name = "boat"]
pub struct PublicBoat {
    pub id: i32,
    pub name: String,
    pub damage_reports: Option<String>,
    pub boat_type_id: i32,
}

#[derive(JsonSchema, Queryable, Deserialize, Serialize)]
pub struct BorrowEntryDTO {
    pub id: Uuid,
    pub destination: String,
    pub damage_report: Option<String>,
    pub boat_id: i32,
    pub boat_name: String,
    pub discipline_id: i32,
}

#[derive(JsonSchema, Insertable, Deserialize)]
#[table_name = "boat"]
pub struct NewBoat {
    pub broken: bool,
    pub deleted: bool,
    pub name: String,
    pub boat_type_id: i32,
}

#[derive(JsonSchema, Insertable, Deserialize)]
#[table_name = "boat"]
pub struct NewBoatWithReport<'a> {
    pub broken: bool,
    pub deleted: bool,
    pub name: String,
    pub boat_type_id: i32,
    pub damage_reports: &'a str,
}

#[derive(
    AsChangeset, JsonSchema, Insertable, Deserialize, Serialize, Queryable, QueryableByName,
)]
#[table_name = "boat"]
pub struct DamagedBoat {
    //pub damage_reports: Option<&'a str>,
    pub damage_counter: i32,
}

#[derive(JsonSchema, Queryable, Serialize, Clone)]
pub struct BorrowEntry {
    pub id: Uuid,
    pub begintime: DateTime<Utc>,
    pub endtime: Option<DateTime<Utc>>,
    pub destination: String,
    pub damage_report: Option<String>,
    pub boat_id: i32,
    pub discipline_id: i32,
}

#[derive(JsonSchema, Queryable, Serialize, Clone)]
pub struct BorrowEntryWithBoatName {
    pub id: Uuid,
    pub begintime: DateTime<Utc>,
    pub endtime: Option<DateTime<Utc>>,
    pub destination: String,
    pub damage_report: Option<String>,
    pub boat_id: i32,
    pub boat_name: String,
    pub discipline_id: i32,
}

#[derive(JsonSchema, Insertable, Deserialize)]
#[table_name = "borrow_entry"]
pub struct NewBorrowEntry<'a> {
    pub begintime: DateTime<Utc>,
    pub endtime: Option<DateTime<Utc>>,
    pub destination: &'a str,
    pub damage_report: &'a str,
    pub boat_id: i32,
    pub discipline_id: i32,
}

#[derive(AsChangeset, JsonSchema, Insertable, Deserialize, Clone)]
#[table_name = "borrow_entry"]
pub struct UpdateBorrowEntryDTO {
    pub damage_report: String,
}

#[derive(AsChangeset, JsonSchema, Insertable, Deserialize)]
#[table_name = "borrow_entry"]
pub struct UpdateBorrowEntry<'a> {
    pub endtime: Option<DateTime<Utc>>,
    pub damage_report: &'a str,
}

#[derive(AsChangeset, JsonSchema, Insertable, Deserialize)]
#[table_name = "boat"]
pub struct UpdateBoat {
    pub broken: bool,
    #[serde(skip_deserializing)]
    pub broken_counter: i32,
    pub deleted: bool,
    pub name: String,
    pub damage_reports: Option<String>,
}

#[derive(AsChangeset, JsonSchema, Insertable, Deserialize)]
#[table_name = "boat"]
pub struct UpdateBoatDamageReports<'a> {
    pub damage_reports: Option<&'a str>,
}

#[derive(JsonSchema, Insertable, Deserialize, Queryable, Serialize)]
#[table_name = "boat_type_to_discipline"]
pub struct BoatTypeToDiscipline {
    pub boat_type_id: i32,
    pub discipline_id: i32,
}

#[derive(JsonSchema, Insertable, Deserialize, Queryable, Serialize, Clone)]
#[table_name = "boat_user"]
pub struct NewBoatUser {
    pub surname: String,
    pub name: String,
    pub email: Option<String>,
}

#[derive(JsonSchema, Insertable, Deserialize, Queryable, Serialize, Clone)]
#[table_name = "boat_user"]
pub struct NewBoatUserWithEmail {
    pub surname: String,
    pub name: String,
    pub email: String,
    pub borrow_entry_id: Uuid,
}

#[derive(JsonSchema, Insertable, Deserialize, Queryable, Serialize, Clone)]
#[table_name = "boat_user"]
pub struct NewBoatUserWithoutEmail {
    pub surname: String,
    pub name: String,
    pub borrow_entry_id: Uuid,
}

#[derive(JsonSchema, Queryable, Serialize)]
pub struct BoatUserFE {
    pub surname: String,
    pub name: String,
    pub email: Option<String>,
    pub borrow_entry_id: Uuid,
}

#[derive(JsonSchema, Queryable, Serialize)]
pub struct BoatUser {
    pub surname: String,
    pub name: String,
    pub phone_number: Option<String>,
    pub email: Option<String>,
    pub street: Option<String>,
    pub street_number: Option<String>,
    pub plz: Option<i32>,
    pub city: Option<String>,
    pub borrow_entry_id: Uuid,
}

#[derive(JsonSchema, Deserialize, Clone)]
pub struct NewBorrowEntryUsers {
    pub destination: String,
    pub boat_id: i32,
    pub discipline_id: i32,
    pub boat_users: Vec<NewBoatUser>,
}

#[derive(JsonSchema, Queryable, Serialize, Clone)]
pub struct BorrowEntryWithUsers {
    pub id: Uuid,
    pub begintime: DateTime<Utc>,
    pub endtime: Option<DateTime<Utc>>,
    pub destination: String,
    pub damage_report: Option<String>,
    pub boat_id: i32,
    pub boat_name: String,
    pub discipline_id: i32,
    pub boat_user: Vec<NewBoatUser>,
}

#[derive(JsonSchema, Queryable, Deserialize, Serialize, Clone)]
pub struct Statistic {
    pub begintime: DateTime<Utc>,
    pub endtime: Option<DateTime<Utc>>,
    pub boat_name: String,
    pub boat_type_name: String,
    pub discipline_name: String,
}
