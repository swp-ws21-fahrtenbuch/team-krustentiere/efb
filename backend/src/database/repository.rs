use chrono;
use chrono::{DateTime, Utc};
use diesel::dsl::count;
use diesel::pg::PgConnection;
use diesel::prelude::*;
use rocket::http::Status;
use uuid::Uuid;

use crate::common::mapper::{boat_user_to_boat_user_fe, person_to_persondto};
use rocket::response::status;

use crate::database::models::{
    Boat, BoatType, BoatTypeFilter, BoatTypeToDiscipline, BoatUserFE, BorrowEntry, BorrowEntryDTO,
    BorrowEntryWithBoatName, BorrowEntryWithUsers, DamagedBoat, Discipline, NewBoatUser,
    NewBoatUserWithEmail, NewBoatUserWithoutEmail, NewBoatWithReport, NewBorrowEntry,
    NewDiscipline, NewPerson, NewUpdateBoatType, Person, PersonDTO, PublicBoat, PublicDiscipline,
    Statistic, UpdateBoat, UpdateBoatDamageReports, UpdateBorrowEntry, UpdateDiscipline,
    UpdatePersonWithoutPW,
};

pub fn get_public_disciplines(
    connection: &PgConnection,
) -> Result<Vec<PublicDiscipline>, diesel::result::Error> {
    use crate::schema::discipline::dsl::*;

    discipline
        .filter(deleted.eq(false))
        .select((id, name))
        .order_by(id)
        .load::<PublicDiscipline>(connection)
}

pub fn delete_person(connection: &PgConnection, account_id: i32) -> Result<usize, status::Custom<String>> {
    use crate::schema::person::dsl::*;

    let result = diesel::delete(person.filter(id.eq(account_id)))
        .execute(connection);
    
    match result {
        Err(_) => Result::Err(status::Custom(Status::InternalServerError, String::from("Could not delete Person, Query failed"))),
        Ok(data) => Result::Ok(data),
    }
}

pub fn validate_person_deletion(connection: &PgConnection, account_id: i32) -> bool {
    use crate::schema::person::dsl::*;

    let roleflag_for_deletion = person
        .select(role_flag)
        .filter(id.eq(account_id))
        .get_result::<i16>(connection);

    match roleflag_for_deletion {
        Err(_) => false,
        Ok(roleflag) => {
            if roleflag == 2 {
                true
            } else if roleflag == 1 {
                let number_of_admins = count_admins(connection);
                if number_of_admins > 1 {
                    true
                } else {
                    false
                }
            } else {
                false
            }
        }
    }
}

fn count_admins(connection: &PgConnection) -> i64 {
    use crate::schema::person::dsl::*;

    let count_admin = person
        .select(count(id))
        .filter(role_flag.eq(1))
        .get_result::<i64>(connection);

    match count_admin {
        Err(_) => 0,
        Ok(number_admin) => number_admin,
    }
}

pub fn get_person(
    connection: &PgConnection,
    account_id: Option<i32>,
    filter_role_flag: Option<i16>,
) -> Result<Vec<PersonDTO>, status::Custom<String>> {
    use crate::schema::person::dsl::*;

    let mut query = person.into_boxed();

    if let Some(requested_account_id) = account_id {
        query = query.filter(id.eq(requested_account_id));
    }

    if let Some(requested_role_flag) = filter_role_flag {
        query = query.filter(role_flag.eq(requested_role_flag));
    }

    let catch_error = query
        .select((id, surname, name, username, email, role_flag))
        .order_by(id)
        .get_results::<PersonDTO>(connection);

    match catch_error {
        Err(_) => Result::Err(status::Custom(
            Status::InternalServerError,
            String::from("Query Error, get_person query failed"),
        )),
        Ok(data) => Result::Ok(data),
    }
}

pub fn check_person_username_exists(
    connection: &PgConnection,
    filter_username: String,
) -> Result<bool, Status> {
    use crate::schema::person::dsl::*;

    match person
        .select(count(id))
        .filter(username.eq(filter_username))
        .get_result::<i64>(connection)
    {
        Ok(person_username_count) => Ok(person_username_count == 1),
        Err(_) => Err(Status::InternalServerError),
    }
}

pub fn find_person_by_username(
    connection: &PgConnection,
    filter_username: String,
) -> Result<Person, Status> {
    use crate::schema::person::dsl::*;

    match person
        .filter(username.eq(filter_username))
        .get_result::<Person>(connection)
    {
        Ok(selected_person) => Ok(selected_person),
        Err(_) => Err(Status::InternalServerError),
    }
}

pub fn get_disciplines_admin(
    connection: &PgConnection,
    discipline_id: Option<i32>,
    deleted_flag: Option<bool>,
) -> Result<Vec<Discipline>, diesel::result::Error> {
    use crate::schema::discipline::dsl::*;

    let mut query = discipline.order_by(id).into_boxed();

    if let Some(requested_discipline_id) = discipline_id {
        query = query.filter(id.eq(requested_discipline_id));
    }

    if let Some(requested_deleted_flag) = deleted_flag {
        query = query.filter(deleted.eq(requested_deleted_flag));
    }

    query.load::<Discipline>(connection)
}
pub fn create_discipline(connection: &PgConnection, name: &str) -> Result<Discipline, status::Custom<String>> {
    use crate::schema::discipline;

    let new_discipline = NewDiscipline {
        name: name.to_string(),
    };

    let created_discipline = diesel::insert_into(discipline::table)
        .values(&new_discipline)
        .get_result(connection);
    
    match created_discipline {
        Err(_) => Result::Err(status::Custom(Status::InternalServerError, String::from("Query failed in create discipline"))),
        Ok(data) => Result::Ok(data),
    }
}

pub fn update_discipline(
    id: i32,
    name: &str,
    deleted: bool,
    connection: &PgConnection,
) -> Result<Discipline, status::Custom<String>> {
    use crate::schema::discipline;

    let updated_discipline = UpdateDiscipline {
        name: name.to_string(),
        deleted,
    };

    let updated_discipline = diesel::update(discipline::table.find(id))
        .set(&updated_discipline)
        .get_result(connection);

    match updated_discipline {
        Err(_) => Result::Err(status::Custom(Status::InternalServerError, String::from("Query failed in updating discipline"))),
        Ok(data) => Result::Ok(data),
    }
}

pub fn fetch_open_entries(
    connection: &PgConnection,
) -> Result<Vec<BorrowEntryDTO>, diesel::result::Error> {
    use crate::schema::boat::dsl::*;
    use crate::schema::borrow_entry::dsl::*;

    borrow_entry
        .inner_join(boat)
        .filter(endtime.is_null())
        .select((
            crate::schema::borrow_entry::id,
            destination,
            damage_report,
            crate::schema::boat::id,
            name,
            discipline_id,
        ))
        .order_by(begintime)
        .get_results::<BorrowEntryDTO>(connection)
}

pub fn get_all_entries(
    connection: &PgConnection,
) -> Result<Vec<BorrowEntry>, status::Custom<String>> {
    use crate::schema::borrow_entry::dsl::*;

    let requested_entry = borrow_entry
        .order_by(begintime)
        .get_results::<BorrowEntry>(connection);

    match requested_entry {
        Err(_) => Result::Err(status::Custom(
            Status::InternalServerError,
            String::from("Fetching entries failed in get_all_entries"),
        )),
        Ok(data) => Result::Ok(data),
    }
}
pub fn get_all_entries_with_users(
    connection: &PgConnection,
    requested_show_active: Option<bool>,
) -> Result<Vec<BorrowEntryWithUsers>, status::Custom<String>> {
    use crate::schema::boat::dsl::*;
    use crate::schema::boat_user::dsl::*;
    use crate::schema::borrow_entry::dsl::*;

    let mut query = borrow_entry
        .inner_join(boat)
        .select((
            crate::schema::borrow_entry::id,
            crate::schema::borrow_entry::begintime,
            crate::schema::borrow_entry::endtime,
            crate::schema::borrow_entry::destination,
            crate::schema::borrow_entry::damage_report,
            crate::schema::borrow_entry::boat_id,
            crate::schema::boat::name,
            crate::schema::borrow_entry::discipline_id,
        ))
        .order_by(begintime)
        .into_boxed();

    if let Some(show_active) = requested_show_active {
        match show_active {
            true => query = query.filter(endtime.is_null()),
            false => query = query.filter(endtime.is_not_null()),
        }
    }

    let requested_entry = query.get_results::<BorrowEntryWithBoatName>(connection);

    let entries;
    if let Ok(data) = requested_entry {
        entries = data;
    } else {
        return Result::Err(status::Custom(
            Status::InternalServerError,
            String::from("Fetching entries failed in get_all_entries_with_users"),
        ));
    }

    let mut return_vec = Vec::new();
    for entry in entries {
        let requested_users = boat_user
            .select((
                crate::schema::boat_user::name,
                crate::schema::boat_user::surname,
                crate::schema::boat_user::email,
            ))
            .filter(borrow_entry_id.eq(entry.id))
            .get_results::<NewBoatUser>(connection);

        let users;
        if let Ok(data) = requested_users {
            users = data;
        } else {
            return Result::Err(status::Custom(
                Status::InternalServerError,
                String::from("Fetching users failed in get_all_entries_with_users"),
            ));
        }
        return_vec.push(BorrowEntryWithUsers {
            boat_user: users,
            id: entry.id,
            begintime: entry.begintime,
            endtime: entry.endtime,
            destination: entry.destination,
            damage_report: entry.damage_report,
            boat_id: entry.boat_id,
            boat_name: entry.boat_name,
            discipline_id: entry.discipline_id,
        });
    }
    Result::Ok(return_vec)
}

pub fn get_boat_admin_helper(connection: &PgConnection, boat_id: i32) -> Result<Boat, status::Custom<String>> {
    use crate::schema::boat::dsl::*;

    let result = boat.filter(id.eq(boat_id))
        .get_result::<Boat>(connection);

    match result {
        Err(_) => Result::Err(status::Custom(Status::InternalServerError, String::from("Query failed in get_boat_admin_helper"))),
        Ok(data) => Result::Ok(data),
    }
}

pub fn get_rented_boats(connection: &PgConnection, filter_boat_id: Option<i32>) -> Vec<i32> {
    use crate::schema::borrow_entry::dsl::*;

    let mut query = borrow_entry
        .filter(endtime.is_null())
        .select(boat_id)
        .order_by(begintime)
        .into_boxed();

    if let Some(requested_boat_id) = filter_boat_id {
        query = query.filter(boat_id.eq(requested_boat_id));
    }

    query
        .get_results::<i32>(connection)
        .expect("Error fetching boats from open_borrow entries")
}

pub fn get_public_boats(
    connection: &PgConnection,
    possible_boat_types: Vec<i32>,
) -> Result<Vec<PublicBoat>, diesel::result::Error> {
    use crate::schema::boat::dsl::*;

    let rented_boats = get_rented_boats(connection, None);

    //Find all boats based on boat_type findings above and only if not broken and not deleted, because they should be not publicly viewable
    boat.select((
        crate::schema::boat::id,
        name,
        damage_reports,
        crate::schema::boat::boat_type_id,
    ))
    .filter(crate::schema::boat::id.ne_all(rented_boats))
    .filter(broken.eq(false))
    .filter(deleted.eq(false))
    .filter(crate::schema::boat::boat_type_id.eq_any(possible_boat_types))
    .order_by(id)
    .load::<PublicBoat>(connection)
}

pub fn create_boat(
    connection: &PgConnection,
    boat_name: &str,
    type_of_boat_id: i32,
) -> Result<Boat, status::Custom<String>> {
    use crate::schema::boat;
    use crate::schema::boat::dsl::*;

    let new_boat = NewBoatWithReport {
        name: boat_name.to_string(),
        boat_type_id: type_of_boat_id,
        broken: false,
        deleted: false,
        damage_reports: "",
    };

    let boat_names_count = boat
        .filter(name.eq(boat_name))
        .select(count(id))
        .get_result::<i64>(connection);

    if let Ok(count) = boat_names_count {
        if count > 0 {
            return Result::Err(status::Custom(
                Status::InternalServerError,
                String::from("Query failed for fetching boat names in POST boat"),
            ));
        } else {
            let value = diesel::insert_into(boat::table)
                .values(&new_boat)
                .get_result(connection);

            match value {
                Err(_) => Result::Err(status::Custom(
                    Status::InternalServerError,
                    String::from("Query failed for adding boat in POST boat"),
                )),
                Ok(data) => Result::Ok(data),
            }
        }
    } else {
        return Result::Err(status::Custom(
            Status::BadRequest,
            String::from("Boat with that name already existing"),
        ));
    }
}

pub fn update_boat(
    boat_id: i32,
    connection: &PgConnection,
    boat_broken: bool,
    boat_deleted: bool,
    boat_name: String,
    boat_damage_reports: Option<String>,
) -> Result<Boat, status::Custom<String>> {
    use crate::schema::boat;
    use crate::schema::boat::dsl::*;

    // load counter from boat to update them if needed
    let requested_result = boat
        .filter(id.eq(boat_id))
        .select((broken, broken_counter))
        .load::<(bool, i32)>(connection);

    let result;
    if let Ok(data) = requested_result {
        result = data;
    } else {
        return Result::Err(status::Custom(Status::InternalServerError, String::from("Query failed in update_boat")));
    }

    let (old_broken, mut new_broken_counter) = result[0];

    if old_broken == false && boat_broken == true {
        new_broken_counter += 1;
    }

    let updated_boat = UpdateBoat {
        broken: boat_broken,
        broken_counter: new_broken_counter,
        deleted: boat_deleted,
        name: boat_name,
        damage_reports: boat_damage_reports,
    };

    let some_value = diesel::update(boat::table.find(boat_id))
        .set(&updated_boat)
        .get_result(connection);

    match some_value {
        Err(_) => Result::Err(status::Custom(Status::InternalServerError, String::from("Query failed for updating boat"))),
        Ok(data) => Result::Ok(data),
    }
}

pub fn create_boat_type(
    connection: &PgConnection,
    boat_type_min_persons: i32,
    boat_type_max_persons: i32,
    boat_type_name: &str,
    boat_type_deleted: bool,
) -> Result<BoatType, status::Custom<String>> {
    use crate::schema::boat_type;
    use crate::schema::boat_type::dsl::*;

    if boat_type_max_persons < 1 || boat_type_min_persons < 1  {
        return Result::Err(status::Custom(
            Status::BadRequest,
            String::from("min-persons and max-persons have to be at least 1"),
        ));
    }

    if boat_type_max_persons < boat_type_min_persons  {
        return Result::Err(status::Custom(
            Status::BadRequest,
            String::from("max-person cant be smaller than min-persons"),
        ));
    }

    let new_boat_type = NewUpdateBoatType {
        min_persons: boat_type_min_persons,
        max_persons: boat_type_max_persons,
        name: boat_type_name,
        deleted: boat_type_deleted,
    };

    let requested_boat_type_names_count = boat_type
        .filter(name.eq(boat_type_name))
        .select(count(id))
        .get_result::<i64>(connection);

    // checks if both queries were successfull and returns error if one of them was not
    // return of data only if all queries without errors
    match requested_boat_type_names_count {
        Err(_) => Result::Err(status::Custom(
            Status::InternalServerError,
            String::from("Query error: failed to load boat_types"),
        )),
        Ok(boat_type_names_count) => {
            if boat_type_names_count != 0 {
                return Result::Err(status::Custom(
                    Status::InternalServerError,
                    String::from(
                        "Query error: adding boat_type failed, maybe the name already existed",
                    ),
                ));
            }
            match diesel::insert_into(boat_type::table)
                .values(&new_boat_type)
                .get_result(connection)
            {
                Err(_) => Result::Err(status::Custom(
                    Status::InternalServerError,
                    String::from("Query error: adding boat_type failed"),
                )),
                Ok(requested_data) => Result::Ok(requested_data),
            }
        }
    }
}

pub fn get_boat_types_by_filter(
    connection: &PgConnection,
    get_filter: BoatTypeFilter,
) -> Result<Vec<(i32, i32, i32, String, bool, i32)>, diesel::result::Error> {
    use crate::schema::boat_type::dsl::*;
    use crate::schema::boat_type_to_discipline::dsl::*;

    //find all boat_types that can be used based on the given RequestFilter option
    let mut query = boat_type_to_discipline
        .inner_join(boat_type)
        .select((id, min_persons, max_persons, name, deleted, discipline_id))
        .order_by(id)
        .into_boxed();

    if let Some(requested_discipline_id) = get_filter.discipline_id {
        query = query.filter(discipline_id.eq(requested_discipline_id));
    }

    if let Some(requested_boat_type_id) = get_filter.boat_type_id {
        query = query.filter(crate::schema::boat_type::id.eq(requested_boat_type_id));
    }

    if let Some(requested_deleted_flag) = get_filter.deleted {
        query = query.filter(crate::schema::boat_type::deleted.eq(requested_deleted_flag));
    }

    query.get_results::<(i32, i32, i32, String, bool, i32)>(connection)
}

pub fn validate_unique_username(connection: &PgConnection, filter_username: &str) -> bool {
    use crate::schema::person::dsl::*;

    let return_value = person
        .filter(username.eq(filter_username))
        .select(count(id))
        .get_result::<i64>(connection);
        
    match return_value {
        Err(_) => false,
        Ok(data) => {
            if data == 0 {
                return true;
            } else {
                return false;
            }
        }
    }
}

pub fn validate_unique_discipline_name(connection: &PgConnection, dis_name: &str) -> bool {
    use crate::schema::discipline::dsl::*;

    let is_valid;

    let result_same_name = discipline
        .select(crate::schema::discipline::id)
        .filter(crate::schema::discipline::name.eq(dis_name))
        .get_result::<i32>(connection);

    match result_same_name {
        Ok(_) => is_valid = false,
        Err(_) => is_valid = true,
    }
    is_valid
}

pub fn find_selected_boats(
    connection: &PgConnection,
    deleted_flag: Option<bool>,
    broken_flag: Option<bool>,
    orphan_flag: Option<bool>,
    possible_boat_types: Vec<i32>,
) -> Result<Vec<Boat>, diesel::result::Error> {
    use crate::schema::boat::dsl::*;
    use crate::schema::boat_type::dsl::*;
    use crate::schema::boat_type_to_discipline::dsl::*;
    use crate::schema::discipline::dsl::*;

    let mut query = boat
        .inner_join(boat_type)
        .inner_join(boat_type_to_discipline.on(
            crate::schema::boat_type_to_discipline::boat_type_id.eq(crate::schema::boat_type::id),
        ))
        .inner_join(discipline.on(crate::schema::discipline::id.eq(discipline_id)))
        .filter(crate::schema::boat::boat_type_id.eq_any(possible_boat_types))
        .order_by(crate::schema::boat::id)
        .into_boxed();

    if let Some(requested_orphan_flag) = orphan_flag {
        if requested_orphan_flag {
            query = query
                .filter(crate::schema::discipline::deleted.eq(requested_orphan_flag))
                .or_filter(crate::schema::boat_type::deleted.eq(requested_orphan_flag))
        } else {
            query = query
                .filter(crate::schema::discipline::deleted.eq(requested_orphan_flag))
                .filter(crate::schema::boat_type::deleted.eq(requested_orphan_flag))
        }
    }

    if let Some(requested_broken_flag) = broken_flag {
        query = query.filter(broken.eq(requested_broken_flag));
    }

    if let Some(requested_deleted_flag) = deleted_flag {
        query = query.filter(crate::schema::boat::deleted.eq(requested_deleted_flag));
    }

    query
        .select((
            crate::schema::boat::id,
            crate::schema::boat::broken,
            crate::schema::boat::deleted,
            crate::schema::boat::name,
            crate::schema::boat::damage_reports,
            crate::schema::boat::broken_counter,
            crate::schema::boat::damage_counter,
            crate::schema::boat::boat_type_id,
        ))
        .load::<Boat>(connection)
}

pub fn find_matching_boat_types(
    connection: &PgConnection,
    boat_discipline_id: Option<Vec<i32>>,
) -> Result<Vec<i32>, diesel::result::Error> {
    use crate::schema::boat_type_to_discipline::dsl::*;

    let mut query = boat_type_to_discipline
        .select(crate::schema::boat_type_to_discipline::boat_type_id)
        .order_by(discipline_id)
        .into_boxed();

    if let Some(requested_discipline_id) = boat_discipline_id {
        if requested_discipline_id.len() > 0 {
            query = query.filter(discipline_id.eq_any(requested_discipline_id));
        }
    }

    query.get_results::<i32>(connection)
}

pub fn get_borrow_entry_boatid(
    connection: &PgConnection,
    uuid: Uuid,
) -> Result<i32, diesel::result::Error> {
    use crate::schema::borrow_entry::dsl::*;

    borrow_entry
        .filter(id.eq(uuid))
        .select(boat_id)
        .get_result::<i32>(connection)
}

pub fn validate_borrow_entry_dependencies(
    connection: &PgConnection,
    boat_id: i32,
    check_discipline_id: i32,
    borrow_entry_users_count: i32,
) -> bool {
    use crate::schema::boat::dsl::*;
    use crate::schema::boat_type::dsl::*;
    use crate::schema::boat_type_to_discipline::dsl::*;
    use crate::schema::discipline::dsl::*;

    let mut is_valid = true;

    let mut valid_boat_type_id = 0;

    let result_boat_type_id = boat
        .select(crate::schema::boat::boat_type_id)
        .filter(crate::schema::boat::id.eq(boat_id))
        .filter(crate::schema::boat::broken.eq(false))
        .filter(crate::schema::boat::deleted.eq(false))
        .get_result::<i32>(connection);

    match result_boat_type_id {
        Ok(check_valid_boat_type_id) => {
            // Check, if the boat exists and is not broken or deleted
            if check_valid_boat_type_id <= 0 {
                is_valid = false;
            } else {
                valid_boat_type_id = check_valid_boat_type_id;
            }
        }
        Err(_) => is_valid = false,
    }

    if is_valid {
        println!("valid_boat_type_id: {}", valid_boat_type_id);

        /*SELECT min_persons, max_persons
        FROM public.boat_type_to_discipline
        INNER JOIN public.boat_type ON public.boat_type_to_discipline.boat_type_id = public.boat_type.id
        INNER JOIN public.discipline ON public.boat_type_to_discipline.discipline_id = public.discipline.id
        WHERE public.boat_type.deleted = false
        AND public.discipline.deleted = false
        AND public.boat_type_to_discipline.boat_type_id = _
        AND public.boat_type_to_discipline.discipline_id = _; */
        let result_valid_boat_types_disciplines = boat_type_to_discipline
            .inner_join(boat_type)
            .inner_join(discipline)
            .select((min_persons, max_persons))
            .filter(crate::schema::boat_type_to_discipline::boat_type_id.eq(valid_boat_type_id))
            .filter(crate::schema::boat_type_to_discipline::discipline_id.eq(check_discipline_id))
            .filter(crate::schema::boat_type::deleted.eq(false))
            .filter(crate::schema::discipline::deleted.eq(false))
            .get_result::<(i32, i32)>(connection);

        match result_valid_boat_types_disciplines {
            Ok(min_max_persons) => {
                // Check, if it is a valid number of boat users
                if borrow_entry_users_count < min_max_persons.0
                    || borrow_entry_users_count > min_max_persons.1
                {
                    is_valid = false;
                }
            }
            Err(_) => is_valid = false,
        }
    }

    is_valid
}

pub fn update_boat_type(
    connection: &PgConnection,
    boat_type_id: i32,
    boat_type_min_persons: i32,
    boat_type_max_persons: i32,
    boat_type_name: &str,
    boat_type_deleted: bool,
) -> Result<BoatType, diesel::result::Error> {
    use crate::schema::boat_type;

    let updated_boat_type = NewUpdateBoatType {
        min_persons: boat_type_min_persons,
        max_persons: boat_type_max_persons,
        name: boat_type_name,
        deleted: boat_type_deleted,
    };

    diesel::update(boat_type::table.find(boat_type_id))
        .set(&updated_boat_type)
        .get_result(connection)
}

pub fn create_boat_type_to_discipline(
    connection: &PgConnection,
    boat_type_id: i32,
    discipline_id: i32,
) -> Result<BoatTypeToDiscipline, diesel::result::Error> {
    use crate::schema::boat_type_to_discipline;

    let new_boat_type_to_discipline = BoatTypeToDiscipline {
        boat_type_id: boat_type_id,
        discipline_id: discipline_id,
    };

    diesel::insert_into(boat_type_to_discipline::table)
        .values(&new_boat_type_to_discipline)
        .get_result(connection)
}

pub fn delete_boat_type_to_discipline(
    connection: &PgConnection,
    del_boat_type_id: i32,
    del_discipline_id: i32,
) -> bool {
    use crate::schema::boat_type_to_discipline::dsl::*;

    match diesel::delete(
        boat_type_to_discipline
            .filter(boat_type_id.eq(del_boat_type_id))
            .filter(discipline_id.eq(del_discipline_id)),
    )
    .execute(connection)
    {
        Ok(size) => size > 0,
        Err(_) => false,
    }
}

pub fn update_borrow_entry(
    id: Uuid,
    connection: &PgConnection,
    endtime: Option<DateTime<Utc>>,
    damage_report: String,
) -> Result<BorrowEntry, diesel::result::Error> {
    use crate::schema::borrow_entry;

    let updated_borrow_entry = UpdateBorrowEntry {
        endtime,
        damage_report: &damage_report,
    };

    diesel::update(borrow_entry::table.find(id))
        .set(&updated_borrow_entry)
        .get_result(connection)
}

pub fn check_if_entry_is_open(
    connection: &PgConnection,
    borrow_id: Uuid,
) -> Result<bool, status::Custom<String>> {
    use crate::schema::borrow_entry::dsl::*;

    let still_open = borrow_entry
        .filter(id.eq(borrow_id))
        .filter(endtime.is_null())
        .get_results::<BorrowEntry>(connection);

    match still_open {
        Err(_) => Result::Err(status::Custom(Status::InternalServerError, String::from("Query failed while loading borrow_entries for check_if_entry_is_open"))),
        Ok(data) => Result::Ok({
            if data.len() > 0  {
                true
            } else {
                false
            }
        }),
    }
}

pub fn create_borrow_entry(
    connection: &PgConnection,
    begintime: DateTime<Utc>,
    endtime: Option<DateTime<Utc>>,
    destination: &str,
    damage_report: &str,
    boat_id: i32,
    discipline_id: i32,
) -> Result<BorrowEntry, diesel::result::Error> {
    use crate::schema::borrow_entry;

    let new_borrow_entry = NewBorrowEntry {
        begintime,
        endtime,
        destination,
        damage_report,
        boat_id,
        discipline_id,
    };

    diesel::insert_into(borrow_entry::table)
        .values(&new_borrow_entry)
        .get_result(connection)
}

pub fn change_person(
    connection: &PgConnection,
    account_id: i32,
    surname: String,
    name: String,
    username: String,
    email: String,
    password: Option<String>,
    role_flag: i16,
) -> Result<PersonDTO, status::Custom<String>> {
    use crate::schema::person;

    let req_required_struct = match password {
        None => {
            let return_value = diesel::update(person::table.find(account_id))
            .set(&UpdatePersonWithoutPW {
                name: name,
                surname: surname,
                username: username,
                email: email,
                role_flag: role_flag,
            })
            .get_result(connection);

            match return_value {
                Err(_) => Result::Err(status::Custom(Status::InternalServerError, String::from("Query failed in updated_person"))),
                Ok(data) => Result::Ok(data),
            }
        },
        Some(pw) => {
            let return_value = diesel::update(person::table.find(account_id))
            .set(&NewPerson {
                name: name,
                surname: surname,
                username: username,
                email: email,
                password: pw,
                role_flag: role_flag,
            })
            .get_result(connection);

            match return_value {
                Err(_) => Result::Err(status::Custom(Status::InternalServerError, String::from("Query failed in updated_person with password"))),
                Ok(data) => Result::Ok(data),
            }            
        },
    };
    match req_required_struct {
        Err(error) => Result::Err(error),
        Ok(required_struct) => Result::Ok(person_to_persondto(required_struct)),
    }
}

pub fn create_person(
    connection: &PgConnection,
    surname: String,
    name: String,
    username: String,
    email: String,
    password: String,
    role_flag: i16,
) -> Result<PersonDTO, status::Custom<String>> {
    use crate::schema::person;

    let new_person = NewPerson {
        surname: surname,
        name: name,
        username: username,
        email: email,
        password: password,
        role_flag: role_flag,
    };

    let req_created_person = diesel::insert_into(person::table)
        .values(&new_person)
        //.select(crate::schema::person::surname, crate::schema::person::name, crate::schema::person::username, crate::schema::person::email, crate::schema::person::role_flag)
        .get_result(connection);
    
    match req_created_person {
    Err(_) => Result::Err(status::Custom(Status::InternalServerError, String::from("Query Error, creating person failed"))),
    Ok(created_person) => Result::Ok(person_to_persondto(created_person)),
    }
}

pub fn create_boat_user(
    connection: &PgConnection,
    surname: String,
    name: String,
    email: Option<String>,
    borrow_entry_id: Uuid,
) -> Result<BoatUserFE, status::Custom<String>> {
    use crate::schema::boat_user;

    let requested_inserted_boat_user = match email {
        None => diesel::insert_into(boat_user::table)
            .values(NewBoatUserWithoutEmail {
                surname: surname,
                name: name,
                borrow_entry_id,
            })
            .get_result(connection),
        Some(user_email) => diesel::insert_into(boat_user::table)
            .values(NewBoatUserWithEmail {
                surname: surname,
                name: name,
                email: user_email,
                borrow_entry_id,
            })
            .get_result(connection),
    };
    if let Err(_) = requested_inserted_boat_user {
        return Result::Err(status::Custom(
            Status::InternalServerError,
            String::from("Query Error, creating boat_user failed"),
        ));
    }

    Result::Ok(boat_user_to_boat_user_fe(
        requested_inserted_boat_user.unwrap(),
    ))
}

pub fn increase_boat_damage_counter(
    borrow_entry_id: Uuid,
    connection: &PgConnection,
) -> Result<Boat, status::Custom<String>> {
    use crate::schema::boat::dsl::*;
    use crate::schema::borrow_entry::dsl::*;
    let requested_possible_damaged_boat_id = borrow_entry
        .filter(crate::schema::borrow_entry::id.eq(borrow_entry_id))
        .select(crate::schema::borrow_entry::boat_id)
        .get_result::<i32>(connection);

    let possible_damaged_boat_id;
    if let Ok(b_id) = requested_possible_damaged_boat_id {
        possible_damaged_boat_id = b_id;
    } else {
        return Result::Err(status::Custom(Status::InternalServerError, String::from("Query Error, fetching boat_id from borrow_entry in increase_boat_damage_counter failed")));
    }

    let requested_damaged_boat = boat
        //.select((crate::schema::boat::damage_reports, crate::schema::boat::damage_counter))
        .filter(crate::schema::boat::id.eq(possible_damaged_boat_id))
        .get_result::<Boat>(connection);

    let damaged_boat;
    if let Ok(aboat) = requested_damaged_boat {
        damaged_boat = aboat;
    } else {
        return Result::Err(status::Custom(
            Status::InternalServerError,
            String::from("Query Error, fetching boat for adding damage failed"),
        ));
    }

    let updated_boat = DamagedBoat {
        damage_counter: damaged_boat.damage_counter + 1,
    };
    use crate::schema::boat;
    let data = diesel::update(boat::table.find(possible_damaged_boat_id))
        .set(&updated_boat)
        .get_result::<Boat>(connection);

    match data {
        Err(_) => Result::Err(status::Custom(
            Status::InternalServerError,
            String::from("Query Error, updating damage counter for boat failed"),
        )),
        Ok(aboat) => Result::Ok(aboat),
    }
}

pub fn copy_damage_report_to_boat(
    connection: &PgConnection,
    damage_report: String,
    boat_id: i32,
) -> Result<Boat, status::Custom<String>> {
    use crate::schema::boat::dsl::*;
    let requested_updated_boat = boat.filter(id.eq(boat_id)).get_result::<Boat>(connection);

    let updated_boat;
    if let Ok(a_boat) = requested_updated_boat {
        updated_boat = a_boat;
    } else {
        return Result::Err(status::Custom(
            Status::InternalServerError,
            String::from("Query Error, fetching boat for adding damage failed"),
        ));
    }

    let separator = "; ";

    let new_damage_reports = match updated_boat.damage_reports {
        None => damage_report,
        Some(mut some_damage_reports) => {
            some_damage_reports.push_str(separator);
            some_damage_reports.push_str(&damage_report);
            some_damage_reports
        }
    };

    let s_slice: &str = &new_damage_reports[..];

    let updated_boat = UpdateBoatDamageReports {
        damage_reports: Some(s_slice),
    };
    use crate::schema::boat;
    let return_value = diesel::update(boat::table.find(boat_id))
        .set(&updated_boat)
        .get_result::<Boat>(connection);

    match return_value {
        Err(_) => Result::Err(status::Custom(
            Status::InternalServerError,
            String::from("Query Error, updating boat for adding damage_report"),
        )),
        Ok(data) => Result::Ok(data),
    }
}

pub fn select_statistic_data(
    connection: &PgConnection,
    filter_begin_year: Option<DateTime<Utc>>,
    filter_end_year: Option<DateTime<Utc>>,
) -> Result<Vec<Statistic>, status::Custom<String>> {
    use crate::schema::boat::dsl::*;
    use crate::schema::boat_type::dsl::*;
    use crate::schema::borrow_entry::dsl::*;
    use crate::schema::discipline::dsl::*;

    let mut query = borrow_entry
        .inner_join(boat)
        .inner_join(boat_type.on(crate::schema::boat::id.eq(crate::schema::boat_type::id)))
        .inner_join(discipline)
        .select((
            begintime,
            endtime,
            crate::schema::boat::name,
            crate::schema::boat_type::name,
            crate::schema::discipline::name,
        ))
        .filter(endtime.is_not_null())
        .order_by(begintime)
        .into_boxed();

    if let Some(requested_begin_year) = filter_begin_year {
        query = query.filter(begintime.ge(requested_begin_year));
    }

    if let Some(requested_end_year) = filter_end_year {
        query = query.filter(begintime.le(requested_end_year));
    }

    let return_value = query
        .get_results::<Statistic>(connection);

    match return_value {
        Err(_) => Result::Err(status::Custom(Status::InternalServerError, String::from("Query Error, fetching Statistics failed"))),
        Ok(data) => Result::Ok(data),
    }
}
