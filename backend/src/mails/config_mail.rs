use rocket::request::{FromRequest, Outcome, Request};
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};

use rocket_okapi::request::OpenApiFromRequest;
use rocket_okapi::{gen::OpenApiGenerator, request::RequestHeaderInput};

#[derive(Debug, Serialize, Deserialize, JsonSchema, Clone)]
pub struct MailConfig {
    pub mail_server: String,
    pub mail_user: String,
    pub mail_password: String,
}


#[rocket::async_trait]
impl<'r> FromRequest<'r> for MailConfig {
    type Error = ();

    async fn from_request(request: &'r Request<'_>) -> Outcome<Self, ()> {
        //Retrieve the config states for the email configuration
        let mail_config = request.rocket().state::<MailConfig>().unwrap();
        Outcome::Success(mail_config.clone())
    }
}

impl<'a> OpenApiFromRequest<'a> for MailConfig {
    fn from_request_input(
        _gen: &mut OpenApiGenerator,
        _name: String,
        _required: bool,
    ) -> rocket_okapi::Result<RequestHeaderInput> {
        Ok(rocket_okapi::request::RequestHeaderInput::None)
    }
}
