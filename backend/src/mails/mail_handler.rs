use crate::database::models::BorrowEntry;
use crate::rocket_config::RocketConfig;

use lettre::smtp::authentication::Credentials;
use lettre::{SmtpClient, Transport};
use lettre_email::{Email, EmailBuilder};

use crate::mails::config_mail::MailConfig;
use rocket::tokio::fs;

use check_if_email_exists::{check_email, CheckEmailInput, Reachable};

async fn test_email_deliverability(email: &str) -> bool {
    // test deliverability of email
    let input = CheckEmailInput::new(vec![email.into()]);
    let result = check_email(&input).await;

    println!("{:?}", result);

    // is_reachable is unkown at fu-berlin.de, so probably don't wanna block that
    result.len() > 0
        && result[0].syntax.is_valid_syntax
        && (result[0].is_reachable == Reachable::Safe
            || result[0].is_reachable == Reachable::Unknown)
        && match &result[0].smtp {
            Ok(data) => data.is_deliverable,
            Err(_) => true,
        }
}

pub async fn send_mail_to_user(
    mail_info: MailConfig,
    rocket_config: RocketConfig,
    mail_to: &str,
    borrow_entry: &BorrowEntry,
    name: &str,
) {
    if test_email_deliverability(mail_to).await {
        let contents = fs::read_to_string("/efb-backend/src/mails/borrow_email.html")
            .await
            .unwrap();

        let email = EmailBuilder::new()
            .from(mail_info.mail_user.clone())
            .to(mail_to.to_string())
            .subject("Deine Ausleihe")
            .html(
                contents
                    .replace("{0}", name)
                    .replace(
                        "{1}",
                        &format!("{}/report-damage/", &rocket_config.rocket_url),
                    )
                    .replace("{2}", &(borrow_entry.id.to_string())),
            )
            .build()
            .unwrap();

        send_mail(email, mail_info);
    }
}

pub async fn send_mail_to_boot_attendant(
    mail_info: MailConfig,
    rocket_config: RocketConfig,
    mail_to: String,
    borrow_entry: BorrowEntry,
    boat_name: String,
) {
    if test_email_deliverability(&mail_to).await {
        let contents = fs::read_to_string("/efb-backend/src/mails/boat_attendant_email.html")
            .await
            .unwrap();

        let email = EmailBuilder::new()
            .from(mail_info.mail_user.clone())
            .to(mail_to)
            .subject("Schadensmeldung aus dem elektronischen Fahrtenbuch")
            .html(
                contents
                    .replace("{0}", &boat_name)
                    .replace(
                        "{1}",
                        &(borrow_entry.damage_report.clone().unwrap_or_default()),
                    )
                    .replace(
                        "{2}",
                        &format!("{}/admin/boat-list/", &rocket_config.rocket_url),
                    ),
            )
            .build()
            .unwrap();

        send_mail(email, mail_info);
    }
}

fn send_mail(email: Email, mail_info: MailConfig) {
    let creds = Credentials::new(mail_info.mail_user.clone(), mail_info.mail_password.clone());

    let mut mailer = SmtpClient::new_simple(&mail_info.mail_server.clone())
        .unwrap()
        .credentials(creds)
        .transport();

    match mailer.send(email.into()) {
        Ok(_) => println!("Email sent successfully!"),
        Err(e) => println!("Could not send email: {:?}", e),
    }
}
