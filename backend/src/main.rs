#[macro_use]
extern crate rocket;
#[macro_use]
extern crate diesel;
extern crate dotenv;

mod api;
mod common;
mod database;
mod mails;
mod rocket_config;
mod schema;

use crate::api::admin::*;
use crate::api::auth::account::*;
use crate::api::borrow::*;
use crate::api::statistic::*;
use crate::common::fairings::CORS;
use crate::database::connection_pool::DbConnection;
use crate::mails::config_mail::MailConfig;
use api::auth;
use rocket::fairing::AdHoc;
use rocket::{Build, Rocket};
use rocket_config::RocketConfig;
use rocket_okapi::{openapi_get_routes, swagger_ui::*};

#[launch]
fn rocket() -> Rocket<Build> {
    rocket::build()
        .attach(CORS)
        .mount(
            "/api/",
            openapi_get_routes![
                delete_account,
                get_accounts,
                create_account,
                change_account_details,
                get_disciplines,
                get_boat_types,
                get_boat_type_by_id,
                get_boat_type_id_admin,
                get_boat_types_admin,
                add_boat_types_admin,
                patch_boat_type_admin,
                check_entry_for_return,
                get_boats,
                get_entries,
                get_entries_admin,
                get_entries_with_users_admin,
                add_borrow_entry,
                add_boat_admin,
                patch_borrow_entry,
                get_boat_admin,
                patch_boat_admin,
                get_boats_admin,
                get_admin_disciplines,
                add_discipline_admin,
                get_discipline_admin,
                patch_discipline_admin,
                get_statistics,
                auth::account::login,
            ],
        )
        .attach(AdHoc::config::<RocketConfig>())
        .attach(AdHoc::config::<MailConfig>())
        // .attach(AdHoc::config::<MailUser>())
        // .attach(AdHoc::config::<MailPassword>())
        .attach(DbConnection::fairing())
        .mount(
            "/api/swagger-ui/",
            make_swagger_ui(&SwaggerUIConfig {
                url: "../openapi.json".to_owned(),
                ..Default::default()
            }),
        )
}
