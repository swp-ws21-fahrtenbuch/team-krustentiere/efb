use std::net::IpAddr;

use rocket::request::{FromRequest, Outcome, Request};
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};

use rocket_okapi::request::OpenApiFromRequest;
use rocket_okapi::{gen::OpenApiGenerator, request::RequestHeaderInput};

#[derive(Debug, Serialize, Deserialize, JsonSchema, Clone)]
pub struct RocketConfig {
    pub address: IpAddr,
    pub port: u16,
    pub rocket_url: String,
}

#[rocket::async_trait]
impl<'r> FromRequest<'r> for RocketConfig {
    type Error = ();

    async fn from_request(request: &'r Request<'_>) -> Outcome<Self, ()> {
        //Retrieve the config states for the rocket configuration
        let rocket_config = request.rocket().state::<RocketConfig>().unwrap();

        Outcome::Success(rocket_config.clone())
    }
}

impl<'a> OpenApiFromRequest<'a> for RocketConfig {
    fn from_request_input(
        _gen: &mut OpenApiGenerator,
        _name: String,
        _required: bool,
    ) -> rocket_okapi::Result<RequestHeaderInput> {
        Ok(rocket_okapi::request::RequestHeaderInput::None)
    }
}
