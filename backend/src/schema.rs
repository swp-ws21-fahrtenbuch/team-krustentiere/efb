table! {
    boat (id) {
        id -> Int4,
        broken -> Bool,
        deleted -> Bool,
        name -> Varchar,
        damage_reports -> Nullable<Varchar>,
        broken_counter -> Int4,
        damage_counter -> Int4,
        boat_type_id -> Int4,
    }
}

table! {
    boat_type (id) {
        id -> Int4,
        min_persons -> Int4,
        max_persons -> Int4,
        name -> Varchar,
        deleted -> Bool,
    }
}

table! {
    boat_type_to_discipline (boat_type_id, discipline_id) {
        boat_type_id -> Int4,
        discipline_id -> Int4,
    }
}

table! {
    boat_user (borrow_entry_id, surname, name) {
        surname -> Varchar,
        name -> Varchar,
        phone_number -> Nullable<Varchar>,
        email -> Nullable<Varchar>,
        street -> Nullable<Varchar>,
        street_number -> Nullable<Varchar>,
        plz -> Nullable<Int4>,
        city -> Nullable<Varchar>,
        borrow_entry_id -> Uuid,
    }
}

table! {
    borrow_entry (id) {
        id -> Uuid,
        begintime -> Timestamptz,
        endtime -> Nullable<Timestamptz>,
        destination -> Varchar,
        damage_report -> Nullable<Varchar>,
        boat_id -> Int4,
        discipline_id -> Int4,
    }
}

table! {
    discipline (id) {
        id -> Int4,
        name -> Varchar,
        deleted -> Bool,
    }
}

table! {
    person (id) {
        id -> Int4,
        surname -> Varchar,
        name -> Varchar,
        username -> Varchar,
        email -> Varchar,
        password -> Varchar,
        login_session -> Nullable<Varchar>,
        role_flag -> Int2,
    }
}

table! {
    rentals (id) {
        id -> Int4,
        vehicle_id -> Text,
        rentee_id -> Text,
        comments -> Text,
        returned -> Bool,
    }
}

joinable!(boat -> boat_type (boat_type_id));
joinable!(boat_type_to_discipline -> boat_type (boat_type_id));
joinable!(boat_type_to_discipline -> discipline (discipline_id));
joinable!(boat_user -> borrow_entry (borrow_entry_id));
joinable!(borrow_entry -> boat (boat_id));
joinable!(borrow_entry -> discipline (discipline_id));

allow_tables_to_appear_in_same_query!(
    boat,
    boat_type,
    boat_type_to_discipline,
    boat_user,
    borrow_entry,
    discipline,
    person,
    rentals,
);
