English version below
# Endpunkte Dokumentation DE

## Admin Endpunkte für Ausleihe

### /admin/entries GET
* Gibt alle entry Entitäten mit all ihren Attributen zurück

Beispiel: 
```
    {"id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",  
    "begintime": "2022-02-11T12:23:27.563Z",  
    "endtime": "2022-02-11T12:23:27.563Z",  
    "destination": "string",  
    "damage_report": "string",  
    "boat_id": 0,  
    "discipline_id": 0}   
```

### /admin/entry-users GET 
* Genau wie /admin/entries GET nur mit den Leuten, die das Boot gemietet haben
* ACHTUNG: mehr Rechenaufwand, nur benutzen, wenn benötigt

```
{
    "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    "begintime": "2022-02-11T12:25:22.543Z",
    "endtime": "2022-02-11T12:25:22.543Z",
    "destination": "string",
    "damage_report": "string",
    "boat_id": 0,
    "discipline_id": 0,
    "boat_user": [
      {
        "surname": "string",
        "name": "string",
        "email": "string"
      }
    ]
  }
```

### /admin/boats GET / POST
* POST = Zum erstellen neuer Boote.  
Beispiel:
```
  {
  "broken": true,  
  "deleted": true,
  "name": "string",  
  "damage_reports": "string",  
  "boat_type_id" 1,
  } 
```
* GET = Optionale Query-Strings: ?<discipline>&<deleted_flag>&<broken_flag>&<orphan_flag>
* Mit der Orphan flag wird nach Booten gesucht, welche eine gelöschte Disziplin/ gelöschten Boat_type haben  
* Je nachdem welche Flags gesetzt sind, ergeben sich folgende Ausgaben ausgenommen von der orphan Flag:  
b = broken Boote werden angezeigt  
d = deleted Boote werden angezeigt  
!b = nicht broken Boote werden angezeigt  

| broken \ deleted | None                     | false                                   | true                             |
|------------------|--------------------------|-----------------------------------------|----------------------------------|
| None             | b + d + !b + !d alles    | b + !b + !d nicht kaputt                | b + !b + d gelöscht              |
| false            | d + !d + !b nicht kaputt | !b + !d nicht kaputt und nicht gelöscht | d + !b gelöscht und nicht kaputt |
| true             | d + !d + b kaputt        | !d + b nicht gelöscht und kaputt        | b + d kaputt und gelöscht        |

Beispiele:
```
  foo.bar/admin/boats?discipline_id=2&deleted_flag=false&broken_flag=true 

  foo.bar/admin/boats?broken_flag=true 

  foo.bar/admin/boats 
```
### /admin/boat/<id> GET/PATCH
* GET = Get data of a boat by id  
Beispiel: 
``` 
  foo.bar/admin/boat/12  

  {
    "id": 0,
    "broken": true,
    "deleted": true,
    "name": "string",
    "damage_reports": "string",
    "broken_counter": 0,
    "damage_counter": 0,
    "boat_type_id": 0
  }
```
* PATCH = Change data of a boat by id  
Beispiel:  
```
foo.bar/admin/boat/1 mit
  "broken": true,  
  "deleted": true,  
  "name": "string",  
  "damage_reports": "string"  
```
Änderungen von broken von false auf true erhöht den broken counter um 1. 

### /admin/boat_types GET / POST
* GET = Daten vom boat_type erhalten
Query-String: ?<discipline_id>&<deleted_flag>

```
{
    "id": 0,
    "min_persons": 0,
    "max_persons": 0,
    "name": "string",
    "deleted": true,
    "discipline_id": 0
  }
```

* POST = Einen neuen Boattype hinzufügen

```
{
  "min_persons": 0,
  "max_persons": 0,
  "name": "string",
  "deleted": true,
  "discipline_id": 0
}
```

### /admin/boat_type/<id> GET/PATCH
* GET = Daten von einem spezifischen Boattype erhalten

```
{
  "min_persons": 0,
  "max_persons": 0,
  "name": "string",
  "deleted": true,
  "discipline_id": 0
}
```

* PATCH = Boattype überarbeiten

```
{
  "min_persons": 0,
  "max_persons": 0,
  "name": "string",
  "deleted": true,
  "discipline_id": 0
}
```

### /admin/disciplines GET/POST
* GET = Disziplinen laden und eventuell filtern mit Query-String  
Query-String: ?<deleted_flag>  

```
 {
    "id": 0,
    "name": "string",
    "deleted": true
  }
```

* POST = Creating new disciplines 
``` 
  {
  "id": 0,  
  "name": "string",  
  "deleted": true  
  }
```
### /admin/discipline/<id> GET/PATCH
* GET = Von einer einzelnen Disziplin die Daten anfordern 

```
{
  "id": 0,
  "name": "string",
  "deleted": true
}
```

* PATCH = Eine Disziplin bearbeiten mit:  
```
  {
  "name": "string",  
  "deleted": true  
  }
```
## Admin Endpunkte für Accountverwaltung

### /admin/account/<account_id> DELETE/PATCH/

* mit DELETE wird der Account gelöscht, KEIN SOFT DELETE
* mit PATCH kann der Account (von einem selber) modifziert werden  

```
{
  "surname": "string",
  "name": "string",
  "username": "string",
  "email": "string",
  "password": "string",
  "role_flag": 0
}
```

### /admin/account GET

* Optionale Angabe eines Accounts um Nutzerdaten davon einzusehen
* Bei spezifischer Angabe werden alle Angaben der Bootswarte angezeigt außer das Password

```
 {
    "id": 0,
    "surname": "string",
    "name": "string",
    "username": "string",
    "email": "string",
    "role_flag": 0
  }
```

### /admin/account POST

* Erstellen eines neuen Accounts (immer nur Bootswart)
* role_flag=1 => Bootswart
* role_flag=0 => Admin

```
{
  "surname": "string",
  "name": "string",
  "username": "string",
  "email": "string",
  "password": "string",
  "role_flag": 0
}
```


## Admin Endpunkte Statistiken 

### /statistic?<filter_year> GET

* Filterung der Statistik mit: filter_year von 01.01.[filter_year] - 31.12.[filter_year]
* Liefert alle Ausleihen für das angegebene Jahr in Json der Form:  
```
{  
begintime,  
endtime,  
boat_name,  
boat_type_name,  
discipline_name,  
}  
```
## Ausleihe Endpunkte

### /boats GET
* GET = Rückgabe aller Boote, welche nicht gelöscht (=deleted) und nicht kaputt (=broken) sind

### /boat_types GET
* GET = Gibt alle boat_types zurück

### /boat_types/<id> GET
* GET = Rückgabe des boat_types mit der angegebenen ID

### /disciplines GET
* GET = Rückgabe aller Disziplinen

### /entries GET/POST
* GET = Gibt offene entries zurück (wird an endtime festgestellt)  
Beispiel:  
```
  {
    "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",  
    "destination": "string",  
    "damage_report": "string",  
    "boat_id": 0,  
    "boat_name": "string",  
    "discipline_id": 0  
  }
```
* POST = erstellen einer neuer Ausleihe

```
{
  "destination": "string",
  "boat_id": 0,
  "discipline_id": 0,
  "boat_users": [
    {
      "surname": "string",
      "name": "string",
      "email": "string"
    }
  ]
}
```

### /entries/<id> PATCH
* Für die Rückgabe des Bootes kann hier endtime und optional damage_report angegeben werden  
* endtime wird überwacht - darf also nur Werte annehmen, die einen gewissen zeitlichen Sinn ergeben

```
{
  "damage_report": "string"
}
```


## Authentifikation

### /admin/login POST 
* benötigt LoginCredentials (username, password)
* prüft natürlich das gehashte password in der Datenbank mit dem angegebenen ab
* gibt ein JWT Token zurück welcher Account_id, Rolle und Username beinhaltet falls korrekte Credentials

```
{
  "username": "string",
  "password": "string"
}
```

# Endpoint documentation EN

to shorten the file the examples may only be in the german part

## Admin endpoints for lending

### /admin/entries GET
* Returns all entry entities with all their attributes.

Example: 
```
  {
    "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",  
    "begintime": {  
      "secs_since_epoch": 0,  
      "nanos_since_epoch": 0  
    },  
    "endtime": {  
      "secs_since_epoch": 0,  
      "nanos_since_epoch": 0  
    },  
    "destination": "string",  
    "damage_report": "string",  
    "boat_id": 0,  
    "discipline_id": 0  
  }
```
### /admin/entry-users GET 
* Just like /admin/entries GET only with the people who rented the boat.
* ATTENTION: more computation, use only if needed

### /admin/boats GET / POST
* POST = To create new boats.  
Example:
```
{
"broken": true,  
"deleted": true,
"name": "string",  
"damage_reports": "string",  
"boat_type_id" 1  
}
```
* GET = Optional query strings: ?<discipline>&<deleted_flag>&<broken_flag>&<orphan_flag>
* The orphan flag is used to search for boats that have a deleted discipline/boat_type.  
* Depending on which flags are set, the following outputs will result except for the orphan flag:  
b = broken boats are displayed  
d = deleted boats will be displayed  
!b = not broken boats are displayed  

| broken \ deleted | None | false | true |
|------------------|--------------------------|-----------------------------------------|----------------------------------|
| None | b + d + !b + !d all | b + !d not broken | b + !b + d deleted |
| false | d + !d + !b not broken | !b + !d not broken and not deleted | d + !b deleted and not broken |
| true | d + !d + b broken | !d + b not deleted and broken | b + d broken and deleted |

Examples:
```
foo.bar/admin/boats?discipline_id=2&deleted_flag=false&broken_flag=true 

foo.bar/admin/boats?broken_flag=true 

foo.bar/admin/boats 
```
### /admin/boat/<id> GET/PATCH
* GET = Get data of a boat by id  
Example:  
foo.bar/admin/boat/12  

* PATCH = Change data of a boat by id  
Example:  
```
foo.bar/admin/boat/1 with.
  "broken": true,  
  "deleted": true,  
  "name": "string",  
  "damage_reports": "string".  
```
Changing broken from false to true increases the broken counter by 1. 

### /admin/boat_types GET / POST
* GET = get data from boat_type
Query string: ?<discipline_id>&<deleted_flag>

* POST = Add a new boat type

### /admin/boat_type/<id> GET/PATCH
* GET = get data from a specific Boattype
* PATCH = Revise a boat type

### /admin/disciplines GET/POST
* GET = load disciplines and possibly filter with query string  
Query string: ?<deleted_flag>  
* POST = Creating new disciplines  
```
  {
  "id": 0,  
  "name": "string",  
  "deleted": true  
  }
```
### /admin/discipline/<id> GET/PATCH
* GET = Request data from a single discipline 
* PATCH = Edit a discipline with:  
```
  {
  "name": "string",  
  "deleted": true  
  }
```
## Admin endpoints for account management

### /admin/account/<account_id> DELETE/PATCH/

* DELETE deletes the account, NOT a SOFT DELETE.
* PATCH can be used to modify the account (by yourself)

### /admin/account?<account_id> GET

* Optional specification of an account to view user data from it
* If specified, all details of the boat control room are displayed except the password.

### /admin/account POST

* Create a new account (always boat keeper only)


## Admin endpoints statistics 

### /statistic?<filter_year> GET

* Filter statistics with: filter_year from 01.01.[filter_year] - 31.12.[filter_year].
* Returns all loans for the specified year in Json of the form:  
```
{  
begintime,  
endtime,  
boat_name,  
boat_type_name,  
discipline_name,  
}  
```
## Loan endpoints

### /boats GET/PATCH
* GET = Return all boats that are not deleted and not broken.
* PATCH = specification of a discipline via DisciplineFilter to return boats of this discipline

### /boat_types GET/PATCH
* GET = Returns all boat_types
* PATCH = Returns all boat_types with the specification of a discipline via DisciplineFilter

### /boat_types/<id> GET
* GET = Returns the boat_types with the specified ID

### /disciplines GET
* GET = return all disciplines

### /entries GET/POST
* GET = Returns open entries (determined at endtime)  
Example:
```
  {  
    "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",  
    "destination": "string",  
    "damage_report": "string",  
    "boat_id": "0",  
    "boat_name": "string",  
    "discipline_id": 0  
  }
```
* POST = create a new loan

### /entries/<id> PATCH
* For the return of the boat, endtime and optionally damage_report can be specified here.  
* endtime is monitored - may therefore only accept values that make a certain temporal sense

## Authentication

### /admin/login POST 
* requires LoginCredentials (username, password)
* of course checks the hashed password in the database against the given one
* returns a JWT token containing account_id, role and username if credentials are correct



