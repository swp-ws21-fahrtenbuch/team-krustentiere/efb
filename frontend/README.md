# **EFB Frontend**

## Einrichtung

Für die Einrichtung des Gesamten Projekts siehe [hier](../README.md)


### Einrichtung ohne Docker

Um alle Bibliotheken zu installieren führen Sie folgendes Kommando im Ordner /frontend aus:

    npm install 

Um den Server für die Seiten im Entwicklungsmodus zu starten und dadurch Features wie hot-reloading und schnellere Bauzeiten zu genießen wird folgender Befehl verwendet:

    npm run dev

Für linting verwendet man den Befehl

    npm run lint

und für das bauen der Seite für die Produktion:

    npm run build

### Einrichtung mit Docker und docker-compose

Das Projekt sollte bereits laufen, wenn nicht lesen sie [hier](../README.md) nach. Wenn Sie lokal arbeiten, kann es nützlich sein die Bibliotheken ebenfalls mit

    npm install

lokal zu installieren. Wenn Sie mit einem [Devcontainer](https://code.visualstudio.com/docs/remote/containers) in VSCode arbeiten ist das nicht nötig. Generell lohnt sich letzterer, da er eine sehr einfache Entwicklung ermöglicht und von uns bereits initialisiert wurde.

## Bibliotheken und Werkzeuge

### npm

Wir verwenden npm als Package Manager.

### [Next.js][nextjs]

Für Routing und Ladeoptimierung verwenden wir Next.js. Für dieses Projekt gibt es dabei folgende Veränderungen im Gegensatz zu plain React:

- Ein pages Ordner, der alle Aufrufbaren Seiten enthält. Die Struktur des Ordners Bestimmt dabei die URL. Mehr dazu [hier](https://nextjs.org/docs/basic-features/pages).

- Wrapper die in React normalerweise in die `<App/>` Komponente geschrieben werden kommen in die Datei pages/_app.tsx. mehr dazu hier [hier](https://nextjs.org/docs/advanced-features/custom-app)

- Redirection ist in den allermeisten Fällen in der Datei [next.onfig.js](next.config.js) eingebaut. Mehr dazu [hier](https://nextjs.org/docs/api-reference/next.config.js/redirects)

### [MUI][mui]

Wir verwenden MUI um uns möglichst wenig um Styling und Verhalten vom User Interface kümmern zu müssen. Da es sich um eine ziemlich bekannte Library mit viel Dokumentation handelt, gibt es für unser Projekt wenige Besonderheiten. Zwei Hinweise seien trotzdem genannt:

- Da wir die MIT Lizenz verwenden, mussten gerade bei [Datagrids](https://mui.com/components/data-grid/) ein paar Workarounds verwendet werden

- Themes definieren wir in der Datei [`_app.tsx`](pages/_app.tsx).

### [react-openapi-client][openapi-client]

Für eine möglichst unkomplizierte und dynamische Verbindung zum Backend verwenden wir die Bibliothek react-openapi-client. Sie ermöglicht uns, die OpenAPI Spezifiktation aus dem Backend zu nehmen und daraus dynamisch unser API-Calls zu generieren. Aufgrund eines Problems der Bibliothek verwenden wir einen eigenen modifizierten Fork. Außerdem verträgt sich die Typisierung nicht besonders gut mit dem dynamischen Ansatz, was wir hier aber ersteinmal ignoriert haben.

### [Vega lite][vega-lite]

Für die Statistiken verwenden wir vega-lite. Das ermöglicht uns, die vorgefertigten Daten aus der API einfach in Grafiken umzusetzen, die bereits schon von sich aus interaktive Möglichkeiten bieten.

### [next-i18next][next-i18next]

Eine i18next Erweiterung, welche die Static Site Generation und das Server Side Rendering von Next.js unterstützt.

## Struktur

### Ordner

- .devcontainer  
*Einrichtung für die direkte Entwicklung im Docker-Container. Mehr dazu bei [Einrichtung](#einrichtung)*

- components  
*Alle benötigten Komponenten*
  - admin  
  *Komponenten für die Nutzerverwaltung*
  
  - admin_wrapper  
  *Komponenten die über mehrere Seiten im Adminbereich persistieren*
  
  - lists  
  *Komponenten für die Listen wie Boote, Disziplinen und Bootstypen*
  
  - stats  
  *Komponenten für die Statistiken*
  
  - user  
  *Komponenten für das User Frontend*

- hooks  
*Selbstgeschriebene Hooks*

- pages  
*Alle Next.js page componenten. User pages sind nicht in eimen gesonderten Ordner, da das Auswirkungen auf die URL hat.*
  - admin  
  *Alle Admin Seiten liegen in diesem Ordner*
  
  - report-damage  
  *Für [dynamisches Routing](https://nextjs.org/docs/routing/dynamic-routes) beim Eintragen der Schäden durch User*

- public  
*Enthält Dateien zur Übersetzung wichtiger Begriffe*


## Styling

- [MUI][mui]
- [FU Farben][fu-farben]

## Data Fetching

- [React-openapi-client][openapi-client]

## Authentifizierung und Autorisierung im Frontend

Alle Admin Endpunkte sind gesichert und können nur mit einem gültigen Token erreicht werden. Um leere Seiten zu verhindern, muss die [AuthPage](./components/AuthPage.tsx) Komponente um jede Admin Seite als Wrapper geschrieben werden und leitet bei nicht vorhandenem oder ungüligem Token auf die login Seite weiter. Der Token wird in der [_app.tsx](./pages/_app.tsx) in den Context der API Verbindung geschrieben.

[next-i18next]: https://www.npmjs.com/package/next-i18next
[fu-farben]: https://www.fu-berlin.de/sites/corporate-design/grundlagen/farben/index.html
[vega-lite]: https://vega.github.io/vega-lite/
[openapi-client]: https://github.com/anttiviljami/react-openapi-client
[mui]: https://mui.com/getting-started/installation/
[nextjs]: https://nextjs.org/
[install-docker]: https://docs.docker.com/engine/install/
[install-docker-compose]: https://docs.docker.com/compose/install/
[install-extension]: https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers
