import { useRouter } from "next/router"
import { useContext, useEffect } from "react"
import useLocalStorage from "@rehooks/local-storage";
import { OpenAPIContext } from "react-openapi-client"
import { Typography } from "@mui/material";
import { useTranslation } from "next-i18next";

type payload = {sub: string, roles: [number], exp: number}

// Header, payload, and signature are separated by dot ('.') characters and encoded in base64
export const decodePayload = (token: string) => JSON.parse(Buffer.from(token.split('.')[1],'base64').toString()) as payload

const AuthPage = (props: any) => {
    // load api
    const { api } = useContext(OpenAPIContext)
    const router  = useRouter()
    const [authHeaderValue, setAuthHeaderValue] = useLocalStorage("AuthHeaderValue", "")

    const { t } = useTranslation("admin");

    useEffect(() => {
        // test if auth header is set as local variable
        if (authHeaderValue == "") {
            router.push("/login")
        } else {
            // test if auth header is expired (maybe add 10 seconds leeway)
            const payload = decodePayload(authHeaderValue)
            const currentEpoch = (new Date().getTime()) / 1000
            const isExpired = payload.exp < currentEpoch
            if (isExpired) {
                setAuthHeaderValue("")
                //router.push("/login")
            }
        }
    },[authHeaderValue])

    
    if ( authHeaderValue === "") {
        return (<>
        <Typography align="center" variant="h3">{t("redirect")}</Typography>
        </>)
    }
    return (
        <>
        {props.children}
        </>)
}

export default AuthPage