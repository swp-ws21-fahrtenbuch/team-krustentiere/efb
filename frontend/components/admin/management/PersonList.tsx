import {
    Fab,
    Container,
    List, 
    ListItem, 
    IconButton, 
    ListItemButton, 
    ListItemAvatar, 
    Avatar, 
    ListItemText,
    Typography
  } from "@mui/material"
import DeleteForeverIcon from "@mui/icons-material/DeleteForever"
import AccountCircleIcon from "@mui/icons-material/AccountCircle"
import AdminPanelSettingsIcon from '@mui/icons-material/AdminPanelSettings'
import { accountEntry } from "../../../pages/admin/management"
import AddIcon from "@mui/icons-material/Add";
import * as React from "react";
import useLocalStorage from "@rehooks/local-storage"
import { decodePayload } from "../../AuthPage"
import { useTranslation } from "next-i18next";

export type personListProps = {
    data: [accountEntry],
    deleteAction: (id: number) => void,
    editAction: (entry: accountEntry) => void,
    createAction: () => void,
    confirmAction: () => void
}

export default function PersonList({data, deleteAction, editAction, createAction, confirmAction}: personListProps) {
    const { t } = useTranslation("admin");
    const [authHeaderValue, setAuthHeaderValue] = useLocalStorage("AuthHeaderValue", "")
    const [admin, setAdmin] = React.useState(false)
    React.useEffect(()=>{
        const payload = decodePayload(authHeaderValue);
        if(payload.roles.includes(1)){
            setAdmin(true);
        }
        console.log(payload)
    },[authHeaderValue,setAdmin])
    return (<Container>
                <Typography variant="h3">{t("Administration")}</Typography>
                <List>
                    {
                        (data ?? []).map( (entry: accountEntry) =>
                            <ListItem 
                                key={entry.id}
                                secondaryAction={admin &&
                                    <IconButton
                                        edge="end"
                                        aria-label="delete"
                                        onClick={() => {
                                            deleteAction(entry.id)
                                        }}
                                    >
                                        <DeleteForeverIcon />
                                    </IconButton>
                                }
                            >
                            <ListItemButton
                                onClick={() => editAction(entry)}
                            >
                                <ListItemAvatar>
                                    <Avatar>
                                        {entry.role_flag == 1 ? <AdminPanelSettingsIcon/ > : <AccountCircleIcon />}
                                    </Avatar>
                                </ListItemAvatar>
                                <ListItemText primary={entry.username}/>
                            </ListItemButton>
                            </ListItem>
                        )
                    }
                </List>
                {admin && <Fab color="secondary" onClick={createAction} aria-label="add account" sx={{position: 'fixed', bottom: "5%", right: "2%"} }>
                    <AddIcon />
                </Fab>}
            </Container>
    )
}