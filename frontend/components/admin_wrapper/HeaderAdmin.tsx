import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import { useTranslation } from 'next-i18next';
import router from 'next/router';
import LocaleSelect from '../user/LocaleSelect';
import RentedBoatsComponent from './RentedBoatsComponent';
import useLocalStorage from "@rehooks/local-storage";

export default function HeaderAdmin() {
    const { t } = useTranslation('admin');
    const [,setAuthHeaderValue] = useLocalStorage("AuthHeaderValue")

  return (
    <Box sx={{ flexGrow: 2 }}>
      <AppBar position="fixed" sx={{ marginBottom:"50px" }}>
        <Toolbar >

        <Typography variant="h6" component="div" sx={{ flexGrow: 1, cursor: 'pointer' }}
                    onClick={() => router.push("/admin/boat-list")}>

            WSZ Fahrtenbuch Admin
            
          </Typography>


        <Typography variant="h6" component="div" sx={{ flexGrow: 1, cursor: 'pointer' }}
            onClick={() => router.push("/admin/boat-list")}>
             
              {t("boats")}
              
        </Typography>
          

        <Typography variant="h6" component="div" sx={{ flexGrow: 1, cursor: 'pointer' }}
            onClick={() => router.push("/admin/disciplines-list")}>
              
              {t("disciplines")}

        </Typography>

        <Typography variant="h6" component="div" sx={{ flexGrow: 1, cursor: 'pointer' }}
                    onClick={() => router.push("/admin/boat-types-list")}>

            {t("boat-types")}

        </Typography>


        <Typography variant="h6" component="div" sx={{ flexGrow: 1, cursor: 'pointer' }}
            onClick={() => router.push("/admin/management")}>

              {t("administration")}

        </Typography>


        <Typography variant="h6" component="div" sx={{ flexGrow: 1, cursor: 'pointer' }}
            onClick={() => router.push("/admin/stats")}>
              
              {t("statistics")}

        </Typography>
          
          
        <Typography variant="h6" component="div" sx={{ flexGrow: 1, cursor: 'pointer' }}
            onClick={() => {
                setAuthHeaderValue("")
                router.push("/login")
            }}>
              {t("logout")}

        </Typography>

        <RentedBoatsComponent/>
        <LocaleSelect />
        </Toolbar>
      </AppBar>
      <Toolbar sx={{height: "100px"}} />
    </Box>
    
  );
}