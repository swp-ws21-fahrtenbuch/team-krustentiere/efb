import HeaderAdmin from "./HeaderAdmin";
import {Container} from "@mui/material";

export default function Layout({children}: any){
  return(
    <>
        <HeaderAdmin/>
        <main><Container maxWidth="xl">{children}</Container></main>
    </>
  )
}
