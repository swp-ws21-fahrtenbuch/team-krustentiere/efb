import * as React from "react";
import { useTranslation } from "next-i18next";
import { useOperationMethod } from "react-openapi-client";
import { ListItem, List, ListItemText,  IconButton, Drawer, Collapse, Badge, Typography, Divider } from "@mui/material";
import { Boat } from "../lists/BoatList";
import EditIcon from "@mui/icons-material/Edit"
import SailingIcon from '@mui/icons-material/Sailing';
import CheckIcon from "@mui/icons-material/Check"
import { TransitionGroup } from "react-transition-group";
import ReturnBoat from "../../pages/return-boat";

declare type RentedBoatCardProps = {
 entryId: string,
 boatName: string,
 destination: string,
 email: string,
 name: string,
 returnBoat: (entryId: string) => void
}

export default function RentedBoatsComponent() {
  
  const { t } = useTranslation("admin");
  // needs to be swapped with get only boats which aren't free
  
  const [getEntries, {loading: entriesLoading, data: entriesData, error: entriesError}] = useOperationMethod("get_entries_with_users_admin")
  const [returnBoat, {data: returnData, loading: returnLoading , error: returnError}]  = useOperationMethod("patch_borrow_entry");

  const [opened, setOpened] = React.useState(false)
  const [waiting, setWaiting] = React.useState(false)
  
  const toggleDrawer =
    (open: boolean) =>
    (event: React.KeyboardEvent | React.MouseEvent) => {
      if (
        event.type === 'keydown' &&
        ((event as React.KeyboardEvent).key === 'Tab' ||
          (event as React.KeyboardEvent).key === 'Shift')
      ) {
        return;
      }
      setOpened(open)
  }


  
  React.useEffect(()=>{getEntries("true")},[])


  if (waiting && !returnLoading){
    setWaiting(false)
    getEntries("true")
  }

  return (
  <div>
    
    <IconButton onClick={()=>{setOpened(true)}}>
    <Badge color="secondary" badgeContent={ entriesData ? entriesData.length : 0 }>
      <SailingIcon
      htmlColor="white" 
      />
    </Badge>
    </IconButton>


    <Drawer
      open={opened}
      anchor="right"
      onClose={toggleDrawer(false)}
    >
      <Typography variant="h4" component={"div"} margin={2}  >Ausgeliehene Boote</Typography>
      <TransitionGroup>
        {
          entriesData && 
          //ts-ignore
          entriesData.map(({id, boat_name, destination, boat_user})=>{
            return (
              <Collapse key={id}>
                <RentedBoatCard 
                boatName={boat_name} 
                entryId={id} 
                destination={destination} 
                email={boat_user[0]?.email} 
                name={ boat_user[0]?.surname + " " + boat_user[0]?.name }
                returnBoat={
                  (entryId)=>{
                    returnBoat(entryId, {damage_report: ""})
                    setWaiting(true)
                  }
                }
                /> 
              </Collapse>)
          })
        }
      </TransitionGroup>
    </Drawer>
  </div>
  );
}



function RentedBoatCard({boatName, name, email, entryId, returnBoat}: RentedBoatCardProps){
  return (
      <ListItem alignItems="flex-start" >
        {
          email ?
            <ListItemText primary={boatName} secondary={<a href={"mailto:"+ email}>{name}</a>}/>
          : <ListItemText primary={boatName} secondary={name}/>
        }
        
        <IconButton edge="end" aria-label='return' onClick={()=>{returnBoat(entryId)}}>
          <CheckIcon/>
        </IconButton>
      </ListItem>
      
    
  )
}