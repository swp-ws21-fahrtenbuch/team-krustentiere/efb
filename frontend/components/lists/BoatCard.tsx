import * as React from 'react';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions';
import Collapse from '@mui/material/Collapse';
import IconButton, { IconButtonProps } from '@mui/material/IconButton';
import QueryStatsIcon from '@mui/icons-material/QueryStats';
import DeleteIcon from '@mui/icons-material/Delete';
import RestoreFromTrashIcon from '@mui/icons-material/RestoreFromTrash';
import EditIcon from '@mui/icons-material/Edit';
import DamagesList from './DamagesList';
import { Boat, BoatTypeMap } from "./BoatList"
import { setTimeout } from 'timers';
import { VegaLite } from "react-vega";
import {  Chip, Stack, TextField } from '@mui/material';
import HandymanIcon from '@mui/icons-material/Handyman';
import CloseIcon from '@mui/icons-material/Close';
import CheckIcon from '@mui/icons-material/Check';
import { useTranslation } from "next-i18next";
import AssignmentIcon from '@mui/icons-material/Assignment';

declare type BoatCardProps ={
  boat: Boat,
  handleBoatChange: (boat: Boat)=>void,
  stats: any,
  boatType: BoatTypeMap | undefined
} 


export default function BoatCard({boat, handleBoatChange, boatType, stats}: BoatCardProps) {
  const { t } = useTranslation("admin");
  const [expanded, setExpanded] = React.useState(false);
  const [statExpanded, setStatExpanded] = React.useState(false);

  const [damageList, setDamageList] = React.useState(boat.damage_reports.split(";").splice(1).map((damage, index)=>{return {id:index, text: damage}}));
  const [newName, setNewName] = React.useState(boat.name)

  const [edit, setEdit] = React.useState(false)
  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  

  function handleDamageDelete(itemId: number) {
    var newDamagesList = damageList.filter(({id})=>{return id != itemId })
    setDamageList(newDamagesList)
    setTimeout(()=>{
      handleBoatChange(
      {
        ...boat,
        damage_reports: newDamagesList.length == 0 ? "" : ";" + newDamagesList.map(({text})=>{ return text}).join(";"),
      }
    )}, 1000)
    
  }


  const spec = {
    data: { name: 'table' },
    "transform": [ {"filter": "datum.boat_name==='"+boat.name+"'" } ],
    "vconcat": [{
      "width": 370,
      "mark": "bar",
      "encoding": {
        "x": {
          field: "begintime", type: "temporal", "timeUnit": {"unit": "yearmonthdate", "step": 1},
          "scale": {"domain": {"selection": "brush"}},
          "axis": {"title": ""}
        },
        "y": { "type": "quantitative", field: "boat_type_name", aggregate: "count","axis": {"title": t("Borrows per Day"), "tickMinStep": 1} }
      }
    }, {
      "width": 370,
      "height": 60,
      "mark": "bar",
      "selection": {
        "brush": {"type": "interval", "encodings": ["x"]}
      },
      "encoding": {
        "x": {
          field: "begintime", type: "temporal", "timeUnit": {"unit": "yearmonthdate", "step": 7},
          "axis": {"title": t("day_of_borrow")}
        },
        "y": {
          field: "boat_type_name", aggregate: "count",
          "axis": {"title": t("Borrows per Week")}
        }
      }
    }]
  }
  function handleDelete() {
    handleBoatChange(
      {
        ...boat,
        deleted: !boat.deleted
      }
    );

  }

  function handleBroken() {
    handleBoatChange(
      {
        ...boat,
        broken: !boat.broken
      }
    )
    
  }

  function handleNameChange(name: string){
    setEdit(false)
    boat.name = name
    handleBoatChange(
      {
        ...boat,
      }
    )
  }

  return (
    <Card key={boat.id} >
      <CardHeader
        action={
         
          <IconButton aria-label='edit' onClick={()=>{setEdit(!edit)}} >
          <EditIcon/>
        </IconButton>
        }
        title={
          edit?
          <>
            <TextField variant="standard" autoFocus value={newName} onChange={(event)=>{setNewName(event.target.value)} } sx={{width:"70%"}} /> 

            <IconButton aria-label='ok' onClick={()=>{handleNameChange(newName)}}>
              <CheckIcon/>
            </IconButton>
            <IconButton aria-label='exit' onClick={()=>{setEdit(false)}}>
              <CloseIcon/>
            </IconButton>
            
            
          </>:
          boat.name 
        }
        subheader= {boatType?.name}
      />
      
      <CardContent >
        <Stack direction={"row"} spacing={1}>
          {boat.deleted && <Chip label={t("deleted")} variant='outlined' color='error' />}
          {boat.broken && <Chip label={t("broken")} variant='outlined' color='info' />}         
        </Stack>
      </CardContent>
      <CardActions disableSpacing>
        
        <IconButton aria-label="delete" onClick={handleDelete}>
        {!boat.deleted && <DeleteIcon /> }
        {boat.deleted && <RestoreFromTrashIcon />}
        </IconButton>

        <IconButton aria-label='broken' onClick={handleBroken}>
          <HandymanIcon/>
        </IconButton>

        <IconButton 
        aria-label='statistics' 
        onClick={()=>{setStatExpanded(!statExpanded)}}
        aria-expanded={statExpanded}
        >
          <QueryStatsIcon/>
        </IconButton>

        <IconButton
          onClick={handleExpandClick}
          aria-expanded={expanded}
          aria-label="show more"
        >
          <AssignmentIcon />
        </IconButton>
      </CardActions>
      <Collapse in={expanded || statExpanded} timeout="auto" unmountOnExit>
        <CardContent>
          {
          //@ts-ignore
          statExpanded && !stats.loading && <VegaLite spec={spec} data={{table: stats.data}} />}
          {expanded && <DamagesList damages={damageList} handleDamageDelete={handleDamageDelete}/>}
        </CardContent>
      </Collapse>
    </Card>
  );
}
