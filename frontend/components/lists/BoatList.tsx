import * as React from "react";
import BoatCard from "./BoatCard";
import {Masonry} from "@mui/lab"
import {Filter} from "./FilterComponent"
import { Fab } from "@mui/material";
import AddIcon from "@mui/icons-material/Add"
import NewBoatCard, { NewBoat }  from "./NewBoatCard";


export type Boat={
  id: number,
  broken: boolean,
  deleted: boolean,
  name: string,
  damage_reports:string,
  broken_counter: number,
  damage_counter: number,
  boat_type_id: number,
}

export type BoatTypeMap={
  id:number,
  name: string,
}

declare type BoatListProps = {
  boatTypeFilter: number[],
  searchQuery: string,
  boats: Boat[],
  handleBoatChange: (boat: Boat)=>void,
  showDeleted: boolean,
  filter: Filter,
  boatTypes: BoatTypeMap[],
  stats: any,
  postBoat: (boat: NewBoat) => void
}

export default function BoatList({postBoat, boatTypeFilter, searchQuery, boats, handleBoatChange, showDeleted, filter, boatTypes, stats}: BoatListProps) {
  
  //const { t } = useTranslation("admin");
  const [newBoats, setNewBoats ] = React.useState<NewBoat[]>([])

  

  function addNewBoatToList(boatType?: BoatTypeMap ){
    setNewBoats(newBoats.concat([{
      tempId: (newBoats.at(-1)?.tempId || 0) + 1,
      name: "",
      broken: false,
      deleted: false,
      boatType: boatType ? boatType : boatTypes[0]
    }
    ]))
  }

  function removeNewBoat (id: number){
    setNewBoats(newBoats.filter(({tempId})=> tempId != id))
  }

  function editNewBoat(newBoat: NewBoat){
    setNewBoats(newBoats.map((boat)=>{ return (boat.tempId == newBoat.tempId ?  newBoat : boat)}))
  }


  return (
    <div>
      <Masonry columns={3} spacing={2} sx={{alignContent:"flex-start"}}  >
        
        {boatTypes && newBoats.map(( newBoat )=>{
          return <NewBoatCard key={newBoat.tempId} postNewBoat={postBoat} newBoat={newBoat} addNewBoat={addNewBoatToList} editNewBoat={editNewBoat} boatTypes={boatTypes} removeNewBoat={removeNewBoat}/>
          })}
        
        {boats && 
        boats.filter(({boat_type_id})=>{
          return (boatTypeFilter.length==0 ? true: boatTypeFilter.includes(boat_type_id) )
        
        }).filter(({name})=>{
          return name.toLowerCase().includes(searchQuery.toLowerCase())
        
        }).filter(({deleted})=>{
          return !deleted || showDeleted 

        }).filter(({damage_reports})=>{
          return  !filter.damaged || (damage_reports.length != 0)
        
        }).filter(({broken})=>{
          return !filter.broken || broken
          
        }).sort((a, b)=>{
          return (a.name <= b.name)? -1 : 1 
        }).map((boat)=>{
          return(
            <BoatCard key={boat.id} boatType={boatTypes.find(({id})=>{return boat.boat_type_id == id})} boat={boat} handleBoatChange={handleBoatChange} stats={stats}/>
          )
        })} 

      </Masonry>
      <Fab color="secondary" onClick={()=>{addNewBoatToList()}} aria-label="add" sx={{position: 'fixed', bottom: "5%", right: "2%"} }>
        <AddIcon />
      </Fab>
  </div>
  );
}
