import {useEffect, useState} from "react";
import {useTranslation} from "next-i18next";
import BoatList, {Boat} from "./BoatList";
import SelectionComponent from "./SelectionComponent";
import {useOperation, useOperationMethod} from "react-openapi-client";
import {NewBoat } from "./NewBoatCard";
import {Filter} from "./FilterComponent";
import { Typography } from "@mui/material";

export type Category = {
    type: string,
    id: number,
    label: string,
  }


export default function BoatListComponent({query = ""}:{query: string}) {

  const { t } = useTranslation("admin");

  const [getBoatsAdmin, {loading:boatsLoading, data: boatsData, error: boatsError}]= useOperationMethod("get_boats_admin")
  const [patchBoatsAdmin, {loading:confirmLoading, data:confirmData, error:confirmError} ] = useOperationMethod("patch_boat_admin")
  const {loading:disciplinesLoading, data:disciplinesData, error:disciplinesError} = useOperation("get_admin_disciplines")
  const {loading:boatTypesLoading, data:boatTypesData, error:boatTypesError} = useOperation("get_boat_types_admin")
  const [addNewBoat, {data: addBoatData, loading: addBoatLoading, error: addBoatError}] = useOperationMethod("add_boat_admin")

  const stats = useOperation('get_statistics');

  const [currentCategorys, setCurrentCategorys] = useState<Category[]>([])
  const [searchQuery, setSearchQuery] = useState<string>(query)
  const [showDeleted, setShowDeleted] = useState(false)
  const [currentFilter, setCurrentFilter] = useState<Filter>({damaged:false, broken:false, orphan: false})

  const [waiting, setWaiting] = useState(false)


  useEffect(()=>{
    handleCategoryChange(currentCategorys)
  }, [])

  useEffect(()=>{
    handleCategoryChange(currentCategorys)
  }, [currentFilter])


  if (waiting && !confirmLoading && !addBoatLoading){
    setWaiting(false)
    // get boats but maintain discipline Categorys
    handleCategoryChange(currentCategorys);
  }

  function parseDisciplineQuery(categories: Category[]):{url: string} | undefined{
    
    const addToQuery = (array: string[], query: string, value: string) => {
      const delimiter = array.length == 0 ? "?" : "&"
      var result = array.slice()
      result.push(delimiter + query + "=" + value)
      console.log(result)
      return result
    }

    var queryArray = []


    categories.map(({id})=>{queryArray = addToQuery(queryArray, "discipline_id", id.toString())})
    
    if (currentFilter.orphan){
      queryArray = addToQuery(queryArray, "orphan_flag", "true")
    }
    
    return queryArray.length == 0 ? undefined : {url: "admin/boats" + queryArray.join("") }
  }

  
  function handleCategoryChange(values: Category[]){
    var selectedDisciplines =  values.filter(({type})=>{return type === "Discipline"})
    getBoatsAdmin(undefined, undefined, parseDisciplineQuery(selectedDisciplines))
    setCurrentCategorys(values)
  }


  function handleBoatChange(boat: Boat) {
    setWaiting(true);

    patchBoatsAdmin({boat_id: boat.id},{
      broken: boat.broken,
      deleted: boat.deleted,
      name: boat.name,
      damage_reports: boat.damage_reports
    } )
  }

  

  function postBoat(boat: NewBoat){
    setWaiting(true);
    addNewBoat(undefined, {
      broken: boat.broken,
      deleted: boat.deleted,
      name: boat.name,
      boat_type_id: boat.boatType.id
    } )
  }


  function toCategory(): Category[]{
    return disciplinesData.map(
      //@ts-ignore
      ({id, name})=>{return {type: "Discipline", id:id, label: name} }).concat(
        boatTypesData.map(
          //@ts-ignore
          ({id, name})=>{return {type: "Boat-Type", id:id, label: name}}))
  }

  return (
    <div>
      <Typography variant="h3">{t("Boats")}</Typography>
      { boatTypesData && disciplinesData &&
      <SelectionComponent
        defaultValue={searchQuery}
        handleSearchQueryChange={setSearchQuery}
        handleCategoryChange={handleCategoryChange}
        categoryOptions={toCategory()}
        showDeleted={showDeleted}
        handleDeletedChange={setShowDeleted}
        filter={currentFilter}
        handleFilterChange={setCurrentFilter}
        />}
      {boatsData && boatTypesData &&
      <BoatList
        postBoat={postBoat}
        boats={boatsData}
        boatTypeFilter={currentCategorys.filter(({type})=>{return type == "Boat-Type"}).map(({id})=>{return id})}
        searchQuery={searchQuery}
        showDeleted={showDeleted}
        handleBoatChange={handleBoatChange}
        filter={currentFilter}
        //@ts-ignore
        boatTypes={boatTypesData.map(({id, name})=>{return {id: id, name: name}})}
        stats={stats}
      />}

    </div>
  );
}
