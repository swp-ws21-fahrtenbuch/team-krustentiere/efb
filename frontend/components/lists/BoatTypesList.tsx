import * as React from 'react';
import { DataGrid, GridActionsCellItem, GridRenderCellParams, GridRowParams, GridColumns, GridCellEditCommitParams } from '@mui/x-data-grid';
import { useOperationMethod } from 'react-openapi-client';
import MenuItem from '@mui/material/MenuItem';
import Select from '@mui/material/Select';
import { Typography, Button } from '@mui/material';
import DeleteIcon from '@mui/icons-material/Delete';
import RestoreFromTrashIcon from '@mui/icons-material/RestoreFromTrash';
import SaveIcon from '@mui/icons-material/Save';
import CloseIcon from '@mui/icons-material/Close';
import AddIcon from '@mui/icons-material/Add'
import TextField from "@mui/material/TextField";
import { useTranslation } from "next-i18next";

declare type NewBoatType={
  id: 0,
  name: string,
  discipline_id: number,
  min_persons: number,
  max_persons: number,
  deleted: false,
}


export default function BoatTypesList() {
  const [getBoatTypes, { data: boatTypesData, error: boatTypesError, loading: boatTypesLoading }] = useOperationMethod("get_boat_types_admin")
  const [patchBoatTypes, { data: confirmData, error: confirmError, loading: confirmLoading }] = useOperationMethod("patch_boat_type_admin")
  const [postBoatType, {data: postData, error: postError, loading: postLoading}] = useOperationMethod("add_boat_types_admin")

  const [getDisciplines, { data: disciplinesData, error: disciplinesError, loading: disciplinesLoading }] = useOperationMethod("get_admin_disciplines")
  const [newBoatType, setnewBoatType] = React.useState<NewBoatType | undefined>()

  const [waiting, setWaiting] = React.useState(false)
  const [editing, setEditing] = React.useState(false);
  const { t } = useTranslation("admin");


  React.useEffect(() => {
    getBoatTypes();
    getDisciplines();
  }, [])


  if (waiting && !confirmLoading && !postLoading) {
    setWaiting(false)
    getBoatTypes()
  }

  function handleCellEditCommit(params: GridCellEditCommitParams){
    setEditing(false)
    if (newBoatType && params.id == newBoatType.id){
      setnewBoatType(
        {
          ...newBoatType,
          [params.field]: params.value
        }
      )
    }else{
      var boatType = boatTypesData.find((boatType) => params.id == boatType.id)

      patchBoatTypes( params.id, {
        ...boatType,
        [params.field]: params.value
      })
      setWaiting(true)
    }
  }

  const handleDeleteClick = (params: GridRowParams) =>(event) => {
    event.stopPropagation();
    var boatType = params.row
    boatType.deleted = !params.row["deleted"]
    patchBoatTypes(params.row["id"], boatType)
    setWaiting(true);
  };

  const handleAddNewBoatType = (params: GridRowParams) => (event) => {
    event.stopPropagation();
    postBoatType( undefined,params.row)
    setnewBoatType(undefined)
    setWaiting(true);
  }

  function renderDiscipline(params: GridRenderCellParams<number>) {
    return <Typography align='left' variant='inherit'>{disciplinesData.find(({id}) => {return id == params.value}).name}</Typography>
  }

  function DisciplineEditInputCell(props: GridRenderCellParams<number>) {
    const { id, value, api, field } = props;

    async function handleChange(event) {
      api.setEditCellValue({ id, field, value: Number(event.target.value) }, event);
    }

    return (
      <Select
        variant='standard'
        labelId="demo-simple-select-label"
        onChange={handleChange}
        id="demo-simple-select"
        value={value}
        sx={{ border: "hidden", width: "100%" }}
      >
        {disciplinesData.map(({ name, id }) => {
          return <MenuItem value={id} key={id}>{name}</MenuItem>
        })}
      </Select>
    );
  }

  function renderDisciplineEditInputCell(params) {
    return <DisciplineEditInputCell {...params} />;
  }

  function renderMinMaxEditInputCell(params,max = false) {
    const { id, value, api, field } = params;

    

    async function handleChange(event) {
      api.setEditCellValue({ id, field, value: Number(event.target.value) }, event);
    }

    const btMin = id == newBoatType?.id ? newBoatType.min_persons : boatTypesData.find((b)=>{return b.id == id})?.min_persons
    const min = max && btMin ? btMin : 1
    
    return <TextField 
    type="number"
    value={value}
    fullWidth
    onChange={handleChange}
    InputProps={{
        inputProps: { 
            max: 100, min: min
        }
    }}
/>
  }

  const columns: GridColumns = [
    { 
      field: 'id', 
      headerName: t('ID'), 
      width: 60,
      editable: false

    },
    {
      field: 'name',
      headerName: t('Name'),
      width: 200,
      editable: true,
    },
    {
      field: 'discipline_id',
      headerName: t('Discipline'),
      renderCell: renderDiscipline,
      renderEditCell: renderDisciplineEditInputCell,
      type: 'number',
      editable: true,
      width: 200,
      align: "left",
      headerAlign: "left"
    },
    {
      field: 'min_persons',
      headerName: t('Min_Persons'),
      renderEditCell: renderMinMaxEditInputCell,
      type: 'number',
      width: 130,
      editable: true,
      align: "left",
      headerAlign: "left"
    },
    {
      field: 'max_persons',
      headerName: t('Max_Persons'),
      renderEditCell: (p)=> {return renderMinMaxEditInputCell(p,true)},
      type: 'number',
      width: 130,
      editable: true,
      align: "left",
      headerAlign: "left"
    },
    {
      field: 'deleted',
      headerName: t('Deleted'),
      type: 'boolean',
      width: 100,
      editable: false,
    },
    {
      field: 'actions',
      type: 'actions',
      headerName: t('Actions'),
      width: 100,
      cellClassName: 'actions',
      getActions: (params) => {
        if (params.row["deleted"] == true){
          return [
            <GridActionsCellItem
              key={1}
              icon={<RestoreFromTrashIcon />}
              label="Delete"
              onClick={handleDeleteClick(params)}
              color="inherit"
            />,
          ];
        }

        if (newBoatType && params.id == newBoatType.id){
          return [
            <GridActionsCellItem
              key={2}
              disabled={editing}
              icon={<SaveIcon/>}
              label="Save"
              onClick={handleAddNewBoatType(params)}
              color="inherit"
            />,
            <GridActionsCellItem
              key={3}
              icon={<CloseIcon/>}
              onClick={()=>{setnewBoatType(undefined)}}
              label="Close"
              color="inherit"
            />,
          ];
        }
        return [
          <GridActionsCellItem
            key={4}
            icon={<DeleteIcon />}
            label="Delete"
            onClick={handleDeleteClick(params)}
            color="inherit"
          />,
        ];
      },
    },

  ];


  return (
    <>
      <Typography variant="h3">{t("Boat Types")}</Typography>
      <Button
      color='secondary'
      startIcon={<AddIcon />}
      onClick={()=>{setnewBoatType(
        {
          id: 0,
          name: "",
          discipline_id: 1,
          min_persons: 1,
          max_persons: 1,
          deleted: false,
        })}}>Add Boat Type</Button>
      <div style={{ height: '65vh', width: '100%' }}>
        {boatTypesData && disciplinesData &&
          <DataGrid
            rows={newBoatType ? [newBoatType].concat(boatTypesData) : boatTypesData}
            autoPageSize
            columns={columns}
            onCellEditStart={()=>{setEditing(true)}}
            onCellEditCommit={handleCellEditCommit}
            disableSelectionOnClick
            hideFooterSelectedRowCount
          />
        }
      </div>
      
      </>
  );
}
