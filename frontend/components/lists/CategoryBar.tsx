import * as React from "react";
import { useTranslation } from "next-i18next";
import { Autocomplete, TextField } from "@mui/material";
import { Category } from "./BoatListComponent";

declare type FilterBarProps = {
  categoryOptions: Category[],
  handleCategoryChange: (values: Category[]) => void,
}

export default function CategoryBar({categoryOptions,handleCategoryChange}:FilterBarProps) {
  
  const { t } = useTranslation("admin");

  return (
  <div>
    <Autocomplete
        sx={{"width":400}}
        multiple
        id="tags-outlined"
        options={categoryOptions}
        getOptionLabel={(option) => option.type + ": " + option.label }
        defaultValue={[]}
        onChange={(event: any, value: Category[])=>{handleCategoryChange(value)}}
        filterSelectedOptions
        renderInput={(params) => (
          <TextField
            {...params}
            label={t("Kategorien")}
            placeholder={t("Bootstypen oder Sportarten")}
          />
        )}
      />
  </div>
  );
}
