import { styled } from '@mui/material/styles';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardContent from '@mui/material/CardContent';
import IconButton, { IconButtonProps } from '@mui/material/IconButton';
import {Typography, Paper, ListItem, Divider, ListItemText} from '@mui/material';
import DeleteIcon from '@mui/icons-material/Delete';
import { Damage } from './DamagesList';

declare type DamageCardProps = {
  damage: Damage,
  handleDelete: (id: number) => void
}

export default function DamageCard({damage, handleDelete}:DamageCardProps) {

  return (
    <ListItem alignItems="flex-start" >
      <ListItemText primary={<Typography  component="div" sx={{overflowWrap:"break-word"}} >{damage.text}</Typography>} />
      <IconButton edge="end" aria-label='delete' onClick={()=>{handleDelete(damage.id)}}>
        <DeleteIcon/>
      </IconButton>
    </ListItem>
  );
}
