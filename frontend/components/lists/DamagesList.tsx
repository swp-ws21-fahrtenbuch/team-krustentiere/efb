
import { useTranslation } from "next-i18next";
import { Typography, Stack, Slide, Collapse, Divider } from "@mui/material";
import DamageCard from "./DamageCard";
import {TransitionGroup} from "react-transition-group"


export type Damage = {
  id: number,
  text: string
}

declare type DamagesListProps = {
  damages: Damage[],
  handleDamageDelete: (id: number ) => void
}


export default function DamagesList({damages, handleDamageDelete}: DamagesListProps) {
  
  const { t } = useTranslation("admin");

  return (
    <div>
      <Stack spacing={2} >
        <TransitionGroup>
          {damages.length == 0 ? <Typography variant="body1">No Damages</Typography> : 
          damages.map((damage)=>{return (
            <Collapse key={damage.id}>
              <DamageCard damage={damage} handleDelete={handleDamageDelete} />
            </Collapse>
            )})}
        </TransitionGroup>
      </Stack>  

    </div>
  );
}
