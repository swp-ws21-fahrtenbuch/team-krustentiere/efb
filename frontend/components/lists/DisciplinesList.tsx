import * as React from 'react';
import { DataGrid,  GridColumns, GridActionsCellItem, GridCellEditCommitParams, GridRowParams } from '@mui/x-data-grid';
import { useOperationMethod } from 'react-openapi-client';
import DeleteIcon from '@mui/icons-material/Delete';
import RestoreFromTrashIcon from '@mui/icons-material/RestoreFromTrash';
import SaveIcon from '@mui/icons-material/Save';
import CloseIcon from '@mui/icons-material/Close';
import { Button, Typography } from '@mui/material';
import { useTranslation } from "next-i18next";
import AddIcon from "@mui/icons-material/Add"
declare type NewDiscipline={
  id: 0,
  name: String,
  deleted: false,
}


export default function DisciplinesList() {
  const { t } = useTranslation("admin");
  const [patchDiscipline, { data: confirmData, error: confirmError, loading: confirmLoading }] = useOperationMethod("patch_discipline_admin")
  const [postDiscipline, {data: postData, error: postError, loading: postLoading}] = useOperationMethod("add_discipline_admin")
  const [getDisciplines, { data: disciplinesData, error: disciplinesError, loading: disciplinesLoading }] = useOperationMethod("get_admin_disciplines")

  const [waiting, setWaiting] = React.useState(false)
  const [newDiscipline, setnewDiscipline] = React.useState<NewDiscipline | undefined>()
  const [editing, setEditing] = React.useState(false);
  
  React.useEffect(() => {
    getDisciplines();
  }, [])



  if (waiting && !confirmLoading && !postLoading) {
    setWaiting(false)
    getDisciplines()
  }

  function handleCellEditCommit(params: GridCellEditCommitParams){
    setEditing(false)
    if (newDiscipline && params.id == newDiscipline.id){
      setnewDiscipline(
        {
          ...newDiscipline,
          [params.field]: params.value
        }
      )
    }else{
      var discipline = disciplinesData.find((discipline) => params.id == discipline.id)

      patchDiscipline( params.id, {
        ...discipline,
        [params.field]: params.value
      })
      setWaiting(true)
    }
  }

  const handleDeleteClick = (params: GridRowParams) =>(event) => {
    event.stopPropagation();
    var discipline = params.row
    discipline.deleted = !params.row["deleted"]
    patchDiscipline(params.row["id"], discipline)
    setWaiting(true);
  };

  const handleAddNewDiscipline = (params: GridRowParams) => (event) => {
    event.stopPropagation();
    postDiscipline( undefined, params.row)
    setnewDiscipline(undefined)
    setWaiting(true);
  }

  const columns: GridColumns = [
    { 
      field: 'id', 
      headerName: t('ID'), 
      width: 60,
      editable: false
    },
    {
      field: 'name',
      headerName: t('Name'),
      width: 200,
      editable: true,
    },
    {
      field: 'deleted',
      headerName: t('Deleted'),
      type: 'boolean',
      width: 100,
      editable: false,
    },
    {
      field: 'actions',
      type: 'actions',
      headerName: t('Actions'),
      width: 100,
      cellClassName: 'actions',
      getActions: (params) => {
        if (params.row["deleted"] == true){
          return [
            <GridActionsCellItem
              key={1}
              icon={<RestoreFromTrashIcon />}
              label="Delete"
              onClick={handleDeleteClick(params)}
              color="inherit"
            />,
          ];
        }

        if (newDiscipline && params.id == newDiscipline.id){
          return [
            <GridActionsCellItem
              key={2}
              icon={<SaveIcon/>}
              label="Save"
              onClick={handleAddNewDiscipline(params)}
              color="inherit"
              disabled={editing}
            />,
            <GridActionsCellItem
            key={3}
              icon={<CloseIcon/>}
              onClick={()=>{setnewDiscipline(undefined)}}
              label="Close"
              color="inherit"
            />,
          ];
        }
        return [
          <GridActionsCellItem
            key={4}
            icon={<DeleteIcon />}
            label="Delete"
            onClick={handleDeleteClick(params)}
            color="inherit"
          />,
        ];
      },
    },

  ];

  

  return (
    <>
      <Typography variant="h3">{t("Disciplines")}</Typography>
      <Button
      startIcon={<AddIcon />}
      color='secondary'
      onClick={ ()=>{
        setnewDiscipline(
        {
          id: disciplinesData.map((discipline)=> discipline.id ).reduce(
            (a,b)=> Math.max(a,b) 
          ) + 1,
          name: "",
          deleted: false
        })}}>Add Discipline</Button>
      <div style={{ height: '65vh', width: '100%' }}>
        { disciplinesData &&
          <DataGrid
            rows={ newDiscipline ? [newDiscipline].concat(disciplinesData) : disciplinesData}
            columns={columns}
            autoPageSize
            onCellEditStart={()=>{setEditing(true)}}
            onCellEditCommit={handleCellEditCommit}
            disableSelectionOnClick
            hideFooterSelectedRowCount
          /> 
        }
      </div>
    </>
  );
}



