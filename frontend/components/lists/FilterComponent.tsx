import * as React from "react";
import { useTranslation } from "next-i18next";
import {Checkbox, Divider, FormControlLabel, Grid, Stack, Typography} from "@mui/material";
import { Category } from "./BoatListComponent";

export type Filter = {
  broken: boolean,
  damaged: boolean,
  orphan: boolean
}

declare type FilterComponentProps = {
  filter: Filter,
  handleFilterChange: (filter: Filter) => void,
}

export default function FilterComponent({filter,handleFilterChange}:FilterComponentProps) {
  
  const { t } = useTranslation("admin");

  return (
  <div>
    <Stack>
      <Typography variant="h6">{t("Filter")}</Typography>
      <Divider />
      <Grid container>
        <Grid item>
          <FormControlLabel
            //@ts-ignore
            label={t("Broken")}
            control={<Checkbox checked={filter.broken} onChange={() => {
              handleFilterChange({...filter, broken: !filter.broken})
            }}/>}
        />
        </Grid>
        <Grid item><FormControlLabel
            //@ts-ignore
            label={t("Damaged")}
            control={<Checkbox checked={filter.damaged} onChange={() => {
              handleFilterChange({...filter,  damaged: !filter.damaged})
            }}/>}
        /></Grid>
        <Grid item><FormControlLabel
            //@ts-ignore
            label={t("Orphan")}
            control={<Checkbox checked={filter.orphan} onChange={() => {
              handleFilterChange({...filter,  orphan: !filter.orphan})
            }}/>}
        /></Grid>
      </Grid>


    </Stack>
  </div>
  );
}