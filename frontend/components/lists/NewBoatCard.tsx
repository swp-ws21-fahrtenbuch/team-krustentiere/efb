import * as React from 'react';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions';import Collapse from '@mui/material/Collapse';
import IconButton from '@mui/material/IconButton';
import DeleteIcon from '@mui/icons-material/Delete';
import {  BoatTypeMap } from "./BoatList"
import {  Button, Chip, MenuItem, Select, Stack, TextField } from '@mui/material';
import HandymanIcon from '@mui/icons-material/Handyman';
import AddIcon from "@mui/icons-material/Add"
import { useTranslation } from "next-i18next";

export type NewBoat = {
  tempId: number,
  broken: boolean,
  deleted: boolean,
  name: string,
  boatType: BoatTypeMap 
}

declare type NewBoatCardProps ={
  boatTypes: BoatTypeMap[],
  newBoat: NewBoat,
  addNewBoat: (boatTyp: BoatTypeMap) => void,
  removeNewBoat: (id: number) => void,
  editNewBoat: (boat: NewBoat) => void, 
  postNewBoat: (boat: NewBoat) => void,
} 


export default function NewBoatCard({newBoat , boatTypes, removeNewBoat, editNewBoat, addNewBoat, postNewBoat}:NewBoatCardProps) {
  const { t } = useTranslation("admin");
  function handleBoatTypeChange(newId: number | string){
    editNewBoat({
      ...newBoat, 
      boatType: boatTypes.find(({id})=>id == parseInt(newId as string)) || boatTypes[0]
    })
  }

  return (
    <Card >
      <CardHeader
      action={
         
      <IconButton aria-label='add' onClick={()=>{addNewBoat(newBoat.boatType)}} >
        <AddIcon/>
      </IconButton>
      }
        title={
        <TextField variant="standard" autoFocus value={newBoat.name} onChange={(event)=>{editNewBoat({...newBoat, name: event.target.value})} } sx={{width:"70%"}} /> 
        }
        subheader= {
        <Select label="Boat Type" value={newBoat.boatType.id} onChange={(event)=>{handleBoatTypeChange(event.target.value)}} variant="standard" sx={{width:"70%"}}>
          {boatTypes && boatTypes.map(({id, name})=>{return <MenuItem value={id} key={id}>{name}</MenuItem>})}
        </Select>}
      />
      <CardContent >
        <Stack direction={"row"} spacing={1}>
          {newBoat.deleted && <Chip label="deleted" variant='outlined' color='error' />}
          {newBoat.broken && <Chip label="broken" variant='outlined' color='primary' />}
        </Stack>
      </CardContent>
      <CardActions disableSpacing>
        
        <IconButton aria-label={t("delete")} onClick={()=>{editNewBoat({...newBoat, deleted: !newBoat.deleted})}}>
          <DeleteIcon />
        </IconButton>

        <IconButton aria-label={t('broken')} onClick={()=>{editNewBoat({...newBoat, broken: !newBoat.broken})}}>
          <HandymanIcon/>
        </IconButton>

        <Stack direction={"row"} spacing={1} marginLeft={"auto"}>
        <Button onClick={()=>{
          removeNewBoat(newBoat.tempId)
          postNewBoat(newBoat)
          }}>{t("Confirm")}</Button>
        <Button color='error' onClick={()=>{removeNewBoat(newBoat.tempId)}}>{t("Cancel")}</Button>
        </Stack>
      </CardActions>
    </Card>
  );
}