import * as React from "react";
import { useTranslation } from "next-i18next";
import { InputAdornment, TextField } from "@mui/material";
import SearchIcon from '@mui/icons-material/Search';

declare type SearchBarProps = {
  defaultValue: string,
  handleSearchQueryChange: (searchQuery: string) => void,
}

export default function SearchBar({defaultValue, handleSearchQueryChange}:SearchBarProps) {
  
  const { t } = useTranslation("admin");

  return (
  <div>
    <TextField
        defaultValue={defaultValue}
        InputProps={{
          startAdornment: (
            <InputAdornment position="start">
              <SearchIcon />
            </InputAdornment>
          ),
        }}
 onChange={(event)=>{handleSearchQueryChange(event.target.value)}}/>
  </div>
  );
}
