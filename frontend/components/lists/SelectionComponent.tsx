import * as React from "react";
import { useTranslation } from "next-i18next";
import CategoryBar from "./CategoryBar";
import { Category } from "./BoatListComponent";
import SearchBar from "./SearchBar";
import ToggleDeleted from "./ToggleDeleted";
import FilterComponent, {Filter} from "./FilterComponent"
import {Grid} from "@mui/material";


declare type SelectionComponentProps = {
  defaultValue: string
  categoryOptions: Category[],
  handleCategoryChange: (values: Category[]) => void,
  handleSearchQueryChange: (searchQuery: string) => void,
  showDeleted: boolean,
  handleDeletedChange: (deleted: boolean) => void,
  filter: Filter,
  handleFilterChange: (filter: Filter) => void
}


export default function SelectionComponent({
    defaultValue,
    categoryOptions, 
    handleCategoryChange, 
    handleSearchQueryChange, 
    showDeleted, 
    handleDeletedChange,
    filter,
    handleFilterChange
  }:SelectionComponentProps) {
  
  const { t } = useTranslation("admin");

  return (
  <Grid container columnSpacing="40" rowSpacing="20" sx={{marginBottom: "20px", marginTop: "20px"}}>
      <Grid item><SearchBar handleSearchQueryChange={handleSearchQueryChange} defaultValue={defaultValue}/></Grid>
      <Grid item><CategoryBar categoryOptions={categoryOptions} handleCategoryChange={handleCategoryChange}/></Grid>
      <Grid item><FilterComponent handleFilterChange={handleFilterChange} filter={filter}/></Grid>
      <Grid item><ToggleDeleted showDeleted={showDeleted} handleDeletedChange={handleDeletedChange}/></Grid>
  </Grid>
  );
}
