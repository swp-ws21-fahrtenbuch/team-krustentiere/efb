import * as React from "react";
import { useTranslation } from "next-i18next";
import { Switch, FormControlLabel } from "@mui/material";

declare type ToggleDeletedProps = {
  showDeleted: boolean,
  handleDeletedChange: (deleted: boolean) =>void
}

export default function ToggleDeleted({showDeleted, handleDeletedChange}:ToggleDeletedProps) {
  
  const { t } = useTranslation("admin");

  return (
  <div>
    <FormControlLabel
      //@ts-ignore
      label={t("Show Deleted Boats")}
      control={<Switch checked={showDeleted} onChange={()=>{handleDeletedChange(!showDeleted)}} name="Show broken Boats"/>}
    />  
  </div>
  );
}