import { InferGetStaticPropsType } from "next";
import * as React from "react";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { Container, Stack, Typography } from "@mui/material";
import { useOperation, useOperationMethod } from "react-openapi-client";
import ReactDOM from "react-dom";
import { VegaLite } from "react-vega";
import { useTranslation } from "next-i18next";



export default function CountByTypeName(stat: any) {
  const { t } = useTranslation("admin");
  const spec = {
    title: { text: t("Rentals per Boat Type"), fontSize: 18 },
    data: { name: 'table' },
    width: 600,
    height: 300,
    mark: "bar",
    
    encoding: {
      x: { field: "boat_type_name", type: "ordinal",title: t("Boat types") },
      y: { field: "boat_type_name", aggregate: "count", title: t("Count of Records") }, 
    },
  };
  return (
    <div>
        {//@ts-ignore
      <VegaLite spec={spec} data={{table: stat.stat}} />
    }
    </div>
  );
}
