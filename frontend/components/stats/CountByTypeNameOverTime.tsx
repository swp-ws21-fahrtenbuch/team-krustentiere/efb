import { InferGetStaticPropsType } from "next";
import * as React from "react";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { Container, Stack, Typography } from "@mui/material";
import { useOperation, useOperationMethod } from "react-openapi-client";
import ReactDOM from "react-dom";
import { VegaLite } from "react-vega";
import { useTranslation } from "next-i18next";

export default function CountByTypeNameOverTime(stat: any) {
  const { t } = useTranslation("admin");
  const spec = {
    title: { text: t("Cumulative Boat Type Rentals Over Time"), fontSize: 18 },
    $schema: "https://vega.github.io/schema/vega-lite/v5.json",
    data: { name: "table" },
    width: 900,
    height: 300,
    mark: "bar",
    params: [
      {
        name: "op",
        select: { type: "point", fields: ["boat_type_name"] },
        bind: "legend",
      },
    ],
    encoding: {
      x: {
        field: "begintime",
        type: "temporal",
        axis: { title: "" },
        timeUnit: { unit: "yearmonthdate", step: 7 },
      },
      y: {
        type: "quantitative",
        field: "boat_type_name",
        aggregate: "count",
        axis: { tickMinStep: 1 },
        title: t("Count of Records")
      },
      color: {
        field: "boat_type_name",
        axis: { title: t("Boat Type Name")},
      },
      opacity: {
        condition: { param: "op", value: 1 },
        value: 0.2,
      },
    },
  };
  return (
    <div>
      {
        //@ts-ignore
        <VegaLite spec={spec} data={{ table: stat.stat }} />
      }
    </div>
  );
}
