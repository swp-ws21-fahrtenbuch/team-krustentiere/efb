import { InferGetStaticPropsType } from "next";
import * as React from "react";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { Container, Stack, Typography } from "@mui/material";
import { useOperation, useOperationMethod } from "react-openapi-client";
import ReactDOM from "react-dom";
import { VegaLite } from "react-vega";
import { useTranslation } from "next-i18next";

export default function CountByTypeNameOverTime(stat: any) {
  const { t } = useTranslation("admin");
  const spec = {
    title: { text: t("Cumulative Boat Rentals Over Time"), fontSize: 18 },
    data: { name: "table" },
    width: 900,
    height: 300,
    mark: "bar",
    params: [
      {
        name: "op",
        select: { type: "point", fields: ["boat_name"] },
        bind: "legend",
      },
    ],

    encoding: {
      x: {
        field: "begintime",
        type: "temporal",
        timeUnit: { unit: "yearmonthdate", step: 7 },
        axis: { title: "" },
      },
      y: {
        field: "boat_name",
        aggregate: "count",
        title: t("Count of Records"),
      },
      color: { field: "boat_name", axis: { title: t("Boat Name") } },
      opacity: {
        condition: { param: "op", value: 1 },
        value: 0.2,
      },
    },
  };
  return (
    <div>
      {
        //@ts-ignore
        <VegaLite spec={spec} data={{ table: stat.stat }} />
      }
    </div>
  );
}
