import * as React from "react";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select, { SelectChangeEvent } from "@mui/material/Select";
import { useTranslation } from "next-i18next";

declare type Boat = {
  id: number,
  name: string,
}

declare type BoatSelectProps ={
  handleBoatChange: ((id: number) => void),
  selectedBoat: number | undefined ,
  boats: Boat[] 
}

export default function BoatsSelect({handleBoatChange, selectedBoat, boats}: BoatSelectProps) {

  const { t } = useTranslation();

  return (
    <FormControl sx={{ m: 0, minWidth: 80 }}>
      <InputLabel required id="demo-simple-select-autowidth-label">{t("boat")}</InputLabel>
      <Select
        labelId="demo-simple-select-autowidth-label"
        id="demo-simple-select-autowidth"
        value={selectedBoat?.toString()}
        onChange={(event)=> {handleBoatChange(parseInt(event.target.value))}}
        autoWidth
        label="boat"
        required
      >

        {boats?.map(({ id, name }) => {
        return <MenuItem key={id} value={id}>{name}</MenuItem>   
        })}

      </Select>
    </FormControl>
  );
}