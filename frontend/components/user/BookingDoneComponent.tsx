import React, { FunctionComponent } from "react";
import { useTranslation } from "next-i18next";
import { Container, Stack, Typography, Button } from "@mui/material";
import { useEffect } from "react";
import { isMobile } from "react-device-detect";
import { useRouter } from "next/router";

export default function BookingDoneComponent({begintime}:{begintime: string}){
  const { t } = useTranslation();
  const d = new Date(begintime)
  const router = useRouter()

  useEffect(()=>{
    if (!(isMobile)){
      const timer = setTimeout(()=>{
        router.push("/")
      }, 30000);
      return () => clearTimeout(timer)
    }
  })

  function hidden(){
    if (isMobile){
      return ("none")
    }
    else return ("")
  }
 
  return(
    <div>
      <Container maxWidth="sm">
        <Stack 
          alignItems="center" 
          justifyContent="flex-start" 
          spacing={2}
        >
          <Typography 
          variant="h3"
          component="div"
          >
          {t("good-trip")}
          </Typography>
          
          <Typography
          variant="h6"
          component="div"
          >
          {t("booking-confirmed")}
          </Typography>
        
          <Typography
          variant="body1"
          component="div"
          >

          <p>
          {t("booking-starts-at")}
          {d.toLocaleTimeString("de-DE", {hour: "numeric", minute: "numeric"})}.
          </p>

          <p>
          {t("scan-qr-for-return")}
          </p>

          <p>
          {t("return-email")}
          </p>
          
          </Typography>
          {!isMobile && 
          <Button
          type="button"
          variant="contained"
          size="large"
          color="secondary"
          sx={{ marginBottom: '50px' }}
          onClick={()=>{router.push({pathname: "/"})}}
          > 
          {t("start-page")}
          </Button>}

        </Stack>
      </Container>
    </div>
  )
}