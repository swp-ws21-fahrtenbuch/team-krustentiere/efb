import * as React from "react";
import Stack from "@mui/material/Stack";
import { Button, Container, Typography } from "@mui/material";
import { SelectChangeEvent } from "@mui/material/Select";
import { useTranslation } from "next-i18next";
import router from "next/router";
import { useOperation } from "react-openapi-client";

export default function SelectDisciplineComponent() {
  const { loading, data, error } = useOperation("get_disciplines");
  const [selectedDiscipline, setSelectedDiscipline] = React.useState("");
  const { t } = useTranslation("common");

  const handleDisciplineChange = (event: SelectChangeEvent) => {
    setSelectedDiscipline(event.target.value);
  };

  return (
    <div>
      <Container maxWidth="sm">

        {/* We put everything into a stack to align it properly */}
        <Stack spacing={2} alignItems={"center"} justifyContent={"flex-start"}>

          {/* Headline */}
          <Typography
            variant="h3"
            component="div"
            align="center" >
            {t("return")}
          </Typography>

          {/* Info */}
          <Typography
            variant="h6"
            component="div"
            align="center"
            sx={{ marginBottom: "50px" }} >
            {t("return-discipline")}
          </Typography>

          {/* Lists all available disciplines */}
          {data && data.map(({ id, name }) => {
            return (
              <Button
                color="secondary"
                type="button"
                key={name}
                id={id}
                style={{maxWidth: '210px', maxHeight: '55px', minWidth: '210px', minHeight: '55px'}}
                variant="contained"
                size="large"
                sx={{ marginBottom: "50px" }}
                onClick={() => router.push({ pathname: "/return-boat", query: {id: id} })} >
                {name}
              </Button>
            )
          })}

        </Stack>
      </Container>
    </div>
  );
}