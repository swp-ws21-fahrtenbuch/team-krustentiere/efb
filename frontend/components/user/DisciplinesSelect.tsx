import * as React from "react";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select, {SelectChangeEvent} from "@mui/material/Select";
import { useTranslation } from "next-i18next";
import { useOperation } from "react-openapi-client";


export type Discipline = {
  id: number,
  name: string,
  deleted: boolean
}

declare type DisciplinesSelectProps ={
  handleDisciplineChange: ((event: SelectChangeEvent) => void),
  selectedDiscipline: string,
}



export default function DisciplinesSelect({handleDisciplineChange, selectedDiscipline}:DisciplinesSelectProps) {
  
  const { loading, data, error} = useOperation("get_disciplines");
  const { t } = useTranslation();
  

  return (
    <FormControl sx={{ m: 1, minWidth: 200}}>
      <InputLabel required id="discipline" >{t("discipline")}</InputLabel>
      <Select
        labelId="discipline"
        id="discipline"
        value={selectedDiscipline}
        onChange={handleDisciplineChange}
        style={{maxWidth: '210px', maxHeight: '55px', minWidth: '210px', minHeight: '55px'}}
        autoWidth
        label="discipline"
      >
        {data && data.map(({ id, name }) => {
          return <MenuItem key={id} value={id} >{name}</MenuItem>
        })}
      </Select>
    </FormControl>
  );
}
