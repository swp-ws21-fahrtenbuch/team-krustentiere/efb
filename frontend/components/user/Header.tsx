import * as React from 'react';
import { FunctionComponent } from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import LocaleSelect from './LocaleSelect';
import {useRouter} from "next/router";
import { isMobile } from "react-device-detect";
import { Grid} from '@mui/material';

const Header: FunctionComponent = () =>{
  const router = useRouter();
  
  function home(){
    if (!isMobile) router.push({pathname:"/"})
  }

  return(
    <Box sx={{ marginBottom: '50px' }}>
      <AppBar
      position="fixed"
      >
        <Toolbar>
          <Grid
          container
          direction="row"
          justifyContent="space-between"
          alignItems="center"
          item
          >
            <Grid item>
              <Typography
              variant="h6"
              component="div"
              sx={{ cursor: 'pointer' }}
              onClick={home}
              >
                WSZ Fahrtenbuch
              </Typography>
            </Grid>
      
            <Grid item>
              <LocaleSelect/>
            </Grid>

          </Grid>
        </Toolbar>
      </AppBar>
      <Toolbar/>
    </Box>
  )
}

export default Header