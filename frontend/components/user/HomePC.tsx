import * as React from "react";
import Stack from "@mui/material/Stack";
import { Button, Container,Typography } from "@mui/material";
import { useTranslation } from "next-i18next";
import {useRouter } from 'next/router';

function HomePC() {
  const router = useRouter();
  const { t } = useTranslation('common');

  return (
    <div>    
      <Container maxWidth="lg" >
        <Stack
          alignItems={"center"}
          justifyContent={"flex-start"}
          spacing={2} >

          <Typography
            variant="h3" 
            component="div" 
            align="center" >
            {t("at-wsz")}
          </Typography>

          <Typography
            variant="h6"
            component="div"
            sx={{ marginBottom: '50px' }} >
            {t("choose-rent-return")}
          </Typography>
          
          <Button
            type="button"
            color="secondary"
            style={{ maxWidth: '210px', maxHeight: '55px', minWidth: '210px', minHeight: '55px' }}
            sx={{ marginBottom: '50px' }}
            variant="contained"
            size="large"
            onClick={() => router.push('/select-discipline')} >
            {t("rent boat")}
          </Button>
                  
          <Button 
            color="secondary"
            style={{ maxWidth: '210px', maxHeight: '55px', minWidth: '210px', minHeight: '55px' }}
            sx={{ marginBottom: '50px' }}
            type="button"
            variant="contained"
            size="large"
            onClick={() => router.push("/select-discipline-return")} >
            {t("return boat")}
          </Button>

          </Stack>
      </Container>
    </div>
  );
}

export default HomePC