import * as React from 'react';
import { FunctionComponent } from 'react';
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select, {SelectChangeEvent} from "@mui/material/Select";
import { useRouter } from 'next/dist/client/router';
import LanguageIcon from '@mui/icons-material/Language';

const LocaleSelect: FunctionComponent = () => {

  const router = useRouter()
  const {locale, locales, pathname, asPath, query} = router;

  const handleLocaleChange = (event: SelectChangeEvent<string>) => {
    router.push({ pathname, query }, asPath, { locale: event.target.value})
  }

  return(
    <FormControl
      variant='standard'
      sx={{ m: 1, maxWidth: 32}}
      color="primary" >

      <Select
        disableUnderline
        labelId="demo-simple-select-autowidth-label"
        id="demo-simple-select-autowidth"
        value={locale?.toLocaleUpperCase()}
        onChange={handleLocaleChange}
        autoWidth
        IconComponent={() =>
          <LanguageIcon 
            htmlColor="white" 
            sx={{ position: "absolute", left: ".5rem",cursor:'pointer',zIndex:-1 }}
          />
        }>
          
        {locales?.map((l ) => {
          return <MenuItem key={l} value={l}>
            {l.toLocaleUpperCase()}</MenuItem>;
        })}
        
      </Select>
    </FormControl>
  )
}

export default LocaleSelect