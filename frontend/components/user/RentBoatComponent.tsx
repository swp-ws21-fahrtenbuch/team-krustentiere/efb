import { Stack, Container, Typography } from "@mui/material";
import RentForm from "./RentForm";
import { useTranslation } from "next-i18next";
import { useOperation } from "react-openapi-client";

export default function RentBoatComponent({disciplineId}: {disciplineId: number}){
  const {t} = useTranslation("common")
  const {
    loading: disciplineLoading,
    data: disciplineData,
    error: disciplineError,
  } = useOperation("get_disciplines");
  
  return (
    <div>
      <Container maxWidth="sm" sx={{ marginBottom: '30px' }}>
        <Stack
        spacing={2}
        alignItems={"center"}
        justifyContent={"flex-start"}
        >

          <Typography
          variant="h3"
          component="div"
          >
          {t("rent")}
          </Typography>

          <Typography
          align="center"
          variant="h6"
          component="div"
          >
          {t("enter-personal-data")}
          {disciplineData && disciplineData.find((d)=> {
            return d.id == disciplineId
            }).name}
  
          </Typography>

          <RentForm disciplineId={disciplineId}/>
        </Stack>
      </Container>
    </div>
  );
  
}