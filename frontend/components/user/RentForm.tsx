import * as React from "react";
import TextField from "@mui/material/TextField";
import Stack from "@mui/material/Stack";
import {Button, Typography, CircularProgress, Dialog, DialogTitle, Alert, Collapse, IconButton, Box} from "@mui/material";
import BoatsSelect from "./BoatsSelect";
import {useTranslation} from "next-i18next";
import {useOperation, useOperationMethod} from "react-openapi-client";
import router from "next/router";
import {useCookies} from "react-cookie";
import {isMobile} from "react-device-detect";
import useMobileLocalStorage from "../../hooks/useMobileLocalStorage";
import validator from "email-validator";


export default function RentForm(this: any, {disciplineId}: {disciplineId: number}) {
  const [selectedBoat, setSelectedBoat] = React.useState<number>();
  const [passengerCount, setPassengerCount] = React.useState<number>();
  const [passengerMinCount, setPassengerMinCount] = React.useState<number>();
  const [firstName, setFirstName] = useMobileLocalStorage("first_name");
  const [lastName, setLastName] = useMobileLocalStorage("last_name");
  const [destination, setDestination] = useMobileLocalStorage("destination");
  const [email, setEmail] = useMobileLocalStorage("email");
  const [mailHelp, setMailHelp] = React.useState<string>("");
  const [isEmailCorrect, setIsEmailCorrect] = React.useState<boolean>(true);
  const [passengers, setPassengers] = React.useState<string[]>([]);
  const [, setCookie ] = useCookies(["entry_id"])
  const [getBoats , {data:boats}] = useOperationMethod("get_boats" );
  const {data:boat_types} = useOperation("get_boat_types", disciplineId );
  const [confirm_send, {
        loading:confirm_loading,
        error:confirm_error,
        data:confirm_data,
        response:confirm_response}] = useOperationMethod("add_borrow_entry");
  const { t } = useTranslation('common');

  const [afterErrorSate, setErrorState] = React.useState(false);

  const errorState = !confirm_loading && confirm_error && !afterErrorSate

  const [open, setOpen] = React.useState(true);


React.useEffect(()=>{
  getBoats(disciplineId)
},[disciplineId])

  // Returns max number of passengers
  function get_passenger_count(boat_id :Number){
    const boat_type_id = boats?.filter((boat: any)=> (boat.id === boat_id))[0]?.boat_type_id
    const boat_type = boat_types?.filter((boat_type: any)=> (boat_type.id === boat_type_id))[0]
    return {maxP:boat_type?.max_persons,minP: boat_type?.min_persons}
  }

  const handleBoatChange = (id:number) => {
    setSelectedBoat(id);
    const {maxP , minP} = get_passenger_count(id);
    setPassengerCount(maxP);
    setPassengerMinCount(minP);
    const max = maxP - 1
    setPassengers( max > passengers.length ?  passengers.concat(Array<string>(max - passengers.length).fill("")) : passengers )
  };

//Sets state of confirm button depending on filled text fields
  function isDisabled() {
    const  enoughPass = passengers.slice(0,passengerMinCount-1).filter((p)=>{return p == ""})
    return (!firstName || !lastName || !destination || !selectedBoat || (email?.length > 0 && !isEmailCorrect) || confirm_loading || enoughPass.length != 0)
  }
  
 
  if (confirm_error?.message == "Request failed with status code XXX") {
    setMailHelp(t("mail-format"))
  }

  function mailInput(e:any) {
    if (!validator.validate(e.target.value)) {
      setIsEmailCorrect(false)
      setMailHelp(t("mail-format"))
    }
    else {
      setIsEmailCorrect(true)
      setMailHelp("")
    }
    setEmail(e.target.value)
  }

  function confirm(){
    let d = new Date()
    const rent = {
      "begintime": d.toJSON(),
      "destination": destination,
      "boat_id": selectedBoat,
      "discipline_id": disciplineId,
      "boat_users":
      [
        {
          "surname": lastName,
          "name": firstName,
          "email": email,
        },
        ...passengers.filter((p)=>{return p != ""}).map((passenger)=> {return {"surname": passenger, "name":"", "email":""}})
      ]
    }
    confirm_send(undefined, rent)
  }

  if (confirm_data) {
    if (isMobile){
      let d = new Date()
      d.setHours(24,0,0,0)
      setCookie("entry_id", confirm_data.id, {expires: d , sameSite: true, path:"/"})
    }
    router.push({pathname:"/booking-confirmed", query:{ time: confirm_data.begintime }})
  }


  return (
  <>
    <div>
      <Stack spacing={2}>
 
        <BoatsSelect
          handleBoatChange={handleBoatChange}
          selectedBoat={selectedBoat}
          boats={boats} />

        <TextField required
          id="first-name"
          label={t("first-name")}
          onChange={(e) => { setFirstName(e.target.value); } }
          value={firstName} />

        <TextField required
          id="last-name"
          label={t("last-name")}
          onChange={(e) => { setLastName(e.target.value); } }
          value={lastName} />

        <TextField required
          id="destination"
          label={t("destination")}
          onChange={(e) => { setDestination(e.target.value); } }
          value={destination} />

        <TextField
          id="email"
          label={t("email")}
          onChange={mailInput}
          value={email}
          type="email"
          error={!isEmailCorrect}
          helperText={mailHelp} />



        {selectedBoat && passengers.slice(0, passengerCount - 1).map((passenger, index) => {
          return (
            <TextField
              required={passengerMinCount - 1 > index}
              key=""
              id="passenger"
              label={t("passenger")}
              onChange={(event) => { setPassengers(passengers.map((entry, i) => { return i == index ? event.target.value : entry; })); } }
              value={passenger} />
          );
        })}
      </Stack>
      </div>

      <Typography
        variant="body1"
        align="center" >

        <Alert
          icon={false}
          sx={{ mb: 2 }} >
          {t("data-consent")}
        </Alert>
      </Typography>

      <div>
      <Stack>
        {confirm_loading ?
          <CircularProgress sx={{ alignSelf: "center" }} />
          : <Button
            color="secondary"
            type="button"
            variant="contained"
            size="large"
            sx={{ marginBottom: '50px', mt:-1 }}
            onClick={confirm}
            disabled={isDisabled()} >
            {t("confirm")}
          </Button>}

        {/* Legend for the * (aka what is required) */}
        <Typography
          variant="body1"
          align="center"
          sx={{mt:-3}} >
          {t("* required")}
        </Typography>

      </Stack>
    </div>

    <Dialog
      open={errorState}
      onClose={()=> {
      setErrorState(true)
      getBoats(disciplineId)
      setSelectedBoat(undefined) }}>
      <DialogTitle>
        {t("rent-error")}
      </DialogTitle>
    </Dialog></>
  );
}