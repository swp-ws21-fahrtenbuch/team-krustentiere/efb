import React, { useEffect } from "react";
import { useTranslation } from "next-i18next";
import { Container, Stack, Typography } from "@mui/material";
import ReportDamageForm from "./ReportDamageForm";
import { useOperation } from "react-openapi-client";
import { useCookies } from "react-cookie";
import { useRouter } from "next/router";
import { isMobile } from "react-device-detect";

export default function  ReportDamageComponent({id}: {id: string}){
  const { t } = useTranslation();
  const [cookies,, removeCookie] = useCookies(["entry_id"])
  const {loading, data, error} = useOperation("check_entry_for_return",id);
  const router = useRouter()

  useEffect(()=>{
    if (data === false){
      cookies.entry_id && removeCookie("entry_id", {path: "/"})
    }
    if (!(isMobile)){
      const timer = setTimeout(()=>{
        router.push("/")
      }, 30000);
      return () => clearTimeout(timer)
    }
  }, [data])


  return(
    <div>
      <Container maxWidth="sm">
        <Stack 
          alignItems="center" 
          justifyContent="flex-start" 
          spacing={2}
        >
          <Typography
          variant="h3"
          component="div">
            {t("return")}
          </Typography>

          <Typography
          variant="h6"
          component="div">
            {t("enter-damage")}
          </Typography>


          {data && <ReportDamageForm id={id} /> }
          {(data === false) && <Typography color="error" variant="h4" align="center">{t("already-returned")}</Typography> }
        </Stack>
      </Container>

    </div>
  )
}

