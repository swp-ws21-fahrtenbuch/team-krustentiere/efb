import {useEffect, useState} from "react";
import TextField from "@mui/material/TextField";
import Stack from "@mui/material/Stack";
import {Button, CircularProgress, Container} from "@mui/material";
import {useTranslation} from "next-i18next";
import {useOperationMethod} from "react-openapi-client";
import {useRouter} from "next/router";
import {useCookies} from "react-cookie";

export default function ReportDamageForm({id}: {id: string}) {

  const { t } = useTranslation('common');
  const [damageReport, setDamageReport] = useState("");
  const router = useRouter()

  // setCookie is needed for matching the right function
  const [cookies,, removeCookie] = useCookies(["entry_id"])

  const [returnBoat, {loading, data, error}] = useOperationMethod("patch_borrow_entry");


  function buttonClicked(){

    let d = new Date
    returnBoat(id, {
      "endtime": d.toJSON(),
      "damage_report": damageReport
    });


  }
  // needs to be testet if fix works
  useEffect(() =>{
    if (data){
      cookies.entry_id && removeCookie("entry_id", {path: "/"})
      router.push("/return-confirmed")
    }
  }, [data])

  return (
    <div>
      <Container
      maxWidth="sm"
      >
        <Stack spacing={2}>
          <TextField
          onChange={(e) => {setDamageReport(e.target.value)}}
          multiline minRows = {3} id = "destination"
          label = {t("damage")}>
          </TextField>

          {/* Confirm button */}
          <Button
          type="button"
          color="secondary"
          variant="contained"
          size="large"
          disabled={loading}
          onClick={buttonClicked}>
            {t("confirm")}
          </Button>
          {loading && <CircularProgress/>}
          {error && <p>{JSON.stringify(error)}</p>}
        </Stack>
      </Container>
    </div>
  );
}