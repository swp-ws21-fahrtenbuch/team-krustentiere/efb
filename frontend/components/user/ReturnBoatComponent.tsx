import React from "react";
import {useTranslation} from "next-i18next";
import {Container, Stack, Typography} from "@mui/material";
import ReturnBoatForm from "./ReturnBoatForm";

export default function ReturnBoatComponent({disciplineId}: {disciplineId: number}){
  const { t } = useTranslation("common");
  return(
    <div>
      <Container maxWidth="sm">

        <Stack 
          alignItems="center" 
          justifyContent="flex-start" 
          spacing={2} >
          
          <Typography
            variant="h3"
            component="div" >
            {t("return")}
          </Typography>

          <Typography
            variant="h6"
            component="div" >
            {t("choose-boat-return")}
          </Typography>

          <ReturnBoatForm disciplineId={disciplineId}/>

        </Stack>
      </Container>
    </div>
  )
}