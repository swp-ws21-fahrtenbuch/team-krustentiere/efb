import Stack from "@mui/material/Stack";
import {Button, Container, Typography} from "@mui/material";
import {useTranslation} from "next-i18next";
import {useOperation} from "react-openapi-client";
import {useRouter} from "next/router";

export default function ReturnBoatForm(this: any, {disciplineId}: {disciplineId: number}) {
  const { t } = useTranslation('common');
  const router = useRouter();
  const {loading: entries_loading, data: entries_data,  error: entries_error} = useOperation("get_entries");
 
  return (
    <div>
      <Container maxWidth="sm">
        <Stack>
          
       
        {entries_data && 
        entries_data.filter(
          ({discipline_id})=> disciplineId == discipline_id )
          .map(({ id, boat_name, discipline_id }) => {
          return ( 
            <Stack key={id}>

            <Typography
              sx={{ marginBottom: '40px', alignItems:"center" }}
              align="center"
              variant="body2" >
              {boat_name}
            </Typography>
            
          <Button 
            color="secondary"
            sx={{ marginBottom: '40px', marginTop: '-30px', alignItems:"center" }}
            variant="contained"
            size="large"
            onClick={() => {router.push({pathname:"/report-damage/[id]", query:{id:id}})}}>{t("return-this")}
          </Button>
          </Stack>
          );
        })}

        </Stack>
      </Container>
    </div>
  );
}