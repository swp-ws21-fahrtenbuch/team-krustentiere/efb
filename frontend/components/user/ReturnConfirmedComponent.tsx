import React, { FunctionComponent } from "react";
import { useTranslation } from "next-i18next";
import { Container, Button, Stack, Typography } from "@mui/material";
import { isMobile } from "react-device-detect";
import { useRouter } from "next/router";
import { useEffect } from "react";


const ReturnConfirmedComponent: FunctionComponent = () =>{
  const { t } = useTranslation();
  const router = useRouter();
  
  useEffect(()=>{
    if (!(isMobile)){
      const timer = setTimeout(()=>{
        router.push("/")
      }, 30000);
      return () => clearTimeout(timer)
    }
  })

  return(
    <div>
      <Container maxWidth="sm">
        <Stack 
          alignItems="center" 
          justifyContent="flex-start" 
          spacing={2}
        >
          <Typography
          variant="h3"
          component="div">
            {t("goodbye")}
          </Typography>
          
          <Typography variant="h6" component="div">
            {t("return-confirmed")}
          </Typography>
          
          <Typography variant="body1" component="div" >
          <p>
            {t("thanks-for-visit")}
          </p>
          <p>
            {t("see-you-again")}
          </p>
          </Typography>

          {!isMobile &&
          <Button
          type="button"
          color="secondary"
          variant="contained"
          size="large"
          sx={{ marginBottom: '50px' }}
          onClick={()=>{router.push({pathname: "/"})}}
          > 
          {t("start-page")}
          </Button>}

        </Stack>
      </Container>

    </div>
  )
}

export default ReturnConfirmedComponent