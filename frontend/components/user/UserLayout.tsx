import Header from "./Header";
import {Container} from "@mui/material";
const Layout = ({children}: any) =>{
  return(
    <>
      <Header/>
      <main><Container maxWidth="xl">{children}</Container></main>
    </>
  )
}

export default Layout