import { isMobile } from "react-device-detect";
import useLocalStorage from "@rehooks/local-storage";
import { useState } from "react";

export default function useMobileLocalStorage(startValue: string) {

  if(isMobile){
    return useLocalStorage(startValue)
  }else{
    return useState("")
  }

}