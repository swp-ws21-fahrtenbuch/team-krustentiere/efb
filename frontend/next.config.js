const { i18n } = require('./next-i18next.config');

/** @type {import('next').NextConfig} */
module.exports = {
  reactStrictMode: true,
  i18n: {
    locales: [ 'en', 'de'],
    defaultLocale: 'en',
  },
  async redirects(){
    return [
      {
        source: "/",
        has: [
          {
            type: "cookie",
            key: "entry_id",
            value: "(?<id>.*)"
          },
        ],
        permanent: false,
        destination: "/report-damage/:id"

      },
      {
        source: "/",
        has: [
          {
            type: "header",
            key: "User-Agent",
            value: "(.*iPhone.*)|(.*Android.*)|(.*iPad.*)"
          },
        ],
        permanent: false,
        destination: "/select-discipline"
      },
      {
        source: "/admin",
        permanent: true,
        destination: "/admin/boat-list"
      }
    ]
  }
}


