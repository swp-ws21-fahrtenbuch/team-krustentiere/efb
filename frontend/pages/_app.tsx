import Layout from '../components/user/UserLayout'
import { appWithTranslation } from 'next-i18next'
import { OpenAPIProvider } from 'react-openapi-client';
import type { ReactElement, ReactNode } from 'react'
import type { NextPage } from 'next'
import type { AppProps } from 'next/app'
import UserLayout from "../components/user/UserLayout"
import useLocalStorage from "@rehooks/local-storage";

import { createTheme, ThemeProvider, styled } from '@mui/material/styles';

type NextPageWithLayout = NextPage & {
  getLayout?: (page: ReactElement) => ReactNode
}

type AppPropsWithLayout = AppProps & {
  Component: NextPageWithLayout
}


function getUserLayout(page: ReactElement){
  return (
    <UserLayout>
      {page}
    </UserLayout>
  )
}



function MyApp({ Component, pageProps }: AppPropsWithLayout) {
  const openApiURL = `/api/openapi.json`
  const getLayout = Component.getLayout ?? ((page) => getUserLayout(page))
  const [authHeaderValue, setAuthHeaderValue] = useLocalStorage("AuthHeaderValue", "notoken")

  return (
    <OpenAPIProvider definition={openApiURL} axiosConfigDefaults={{headers: {'Authentication': `Bearer ${authHeaderValue}`}}}>
     
      <ThemeProvider theme={createTheme(
        {palette:
          {
          primary: {
            main: "#6B9E1E"
          },
          secondary: {
            main: "#164574"
          },
        }
      }
      )}>

        {getLayout(<Component {...pageProps} />)}
      </ThemeProvider>
    </OpenAPIProvider>
  )
}
//@ts-ignore 
export default appWithTranslation(MyApp)

