import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import BoatListComponent from '../../components/lists/BoatListComponent'
import Layout from '../../components/admin_wrapper/LayoutAdmin'
import { ReactElement } from 'react'
import AuthPage from "../../components/AuthPage";

export default function BoatList({query = {q: ''}}: {query: {q:string}} ) {
  return(
    <AuthPage>
      <BoatListComponent query={query.q} />
    </AuthPage>
  )
}

export async function getServerSideProps({ locale, query }: {locale: string, query: {q:string}} ) {
  return {
    props: {
      ...await serverSideTranslations(locale, ['admin']),
      query,
  }}
}

BoatList.getLayout = function getLayout(page: ReactElement) {
  return (
    <Layout>
      {page}
    </Layout>
  )
}
