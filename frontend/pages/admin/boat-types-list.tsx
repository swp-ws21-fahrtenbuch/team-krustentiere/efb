import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import BoatTypesComponent from "../../components/lists/BoatTypesList"
import Layout from '../../components/admin_wrapper/LayoutAdmin'
import { ReactElement } from 'react'
import BoatTypesList from "../../components/lists/BoatTypesList"
import AuthPage from '../../components/AuthPage';

export default function BoatTypesListPage() {

  return(
  <AuthPage>
    <BoatTypesList/>
  </AuthPage>
  )
} 


export async function getServerSideProps({ locale }: {locale: string} ) {
  return {
    props: {
      ...await serverSideTranslations(locale, ['admin']),
  }}
}

BoatTypesListPage.getLayout = function getLayout(page: ReactElement) {
  return (
      <Layout>
        {page}
      </Layout>
  )
}
