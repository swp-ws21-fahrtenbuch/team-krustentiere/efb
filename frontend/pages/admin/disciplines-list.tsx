import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import DisciplinesComponent from "../../components/lists/DisciplinesList"
import Layout from '../../components/admin_wrapper/LayoutAdmin'
import { ReactElement } from 'react'
import DisciplinesList from "../../components/lists/DisciplinesList"
import AuthPage from '../../components/AuthPage';

export default function DisciplinesListPage() {

  return(
  <AuthPage>
    <DisciplinesList/>
  </AuthPage>
  )
}


export async function getServerSideProps({ locale }: {locale: string} ) {
  return {
    props: {
      ...await serverSideTranslations(locale, ['admin']),
  }}
}

DisciplinesListPage.getLayout = function getLayout(page: ReactElement) {
  return (
      <Layout>
        {page}
      </Layout>
  )
}
