import AuthPage from "../../components/AuthPage";
import {
  Avatar,
  Button,
  Checkbox,
  Container,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormControlLabel,
  FormGroup,
  List,
  ListItem,
  ListItemAvatar,
  Skeleton,
  TextField,
} from "@mui/material";
import PersonList from "../../components/admin/management/PersonList";
import { useOperationMethod } from "react-openapi-client";
import { ReactElement, useEffect, useState } from "react";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useTranslation } from "next-i18next";
import Layout from "../../components/admin_wrapper/LayoutAdmin";
import useLocalStorage from "@rehooks/local-storage";
import { decodePayload } from "../../components/AuthPage"


type accountRole = 1 | 2;
type accountDialogAction = "add-account" | "edit-account";
export type accountEntry = {
  id: number;
  surname: string;
  name: string;
  username: string;
  email: string;
  password: string;
  role_flag: accountRole;
};
export type accountDialog = accountEntry & { action: accountDialogAction };

function Management() {
  const [getAccount, getAccountState] = useOperationMethod("get_accounts");
  const [deleteAccount, deleteAccountState] = useOperationMethod(
    "delete_account"
  );
  const [changeAccount, changeAccountState] = useOperationMethod(
    "change_account_details"
  );
  const [createAccount, createAccountState] = useOperationMethod(
    "create_account"
  );

  const [waiting, setWaiting] = useState(false)

  useEffect(() => {
    getAccount();
  }, []);

  if (
    waiting && 
    !deleteAccountState.loading && 
    !changeAccountState.loading && 
    !createAccountState.loading
    ) {
    setWaiting(false)
    getAccount();  
  }
  

  const [authHeaderValue, setAuthHeaderValue] = useLocalStorage("AuthHeaderValue", "")
  const [admin, setAdmin] = useState(false)
  useEffect(()=>{
      const payload = decodePayload(authHeaderValue);
      if(payload.roles.includes(1)){
          setAdmin(true);
      }
      console.log(payload)
  },[authHeaderValue,setAdmin])

  const [open, setOpen] = useState(false);
  const [dialog, setDialog] = useState({
    action: "add-account",
    id: 0,
    surname: "",
    name: "",
    username: "",
    email: "",
    password: "",
    role_flag: 1,
  } as accountDialog);

  const { t } = useTranslation();

  const setDialogValue = (
    key: keyof accountEntry,
    value: accountEntry[keyof accountEntry]
  ) => {
    let newDialog = { ...dialog, [key]: value };
    setDialog(newDialog);
  };
  const openEdit = (entry: accountEntry) => {
    setDialog({
      action: "edit-account",
      ...entry,
    } as accountDialog);
    setOpen(true);
  };
  const openNew = () => {
    setDialog({
      action: "add-account",
      id: 0,
      surname: "",
      name: "",
      username: "",
      email: "",
      password: "",
      role_flag: 1,
    });
    setOpen(true);
  };
  const confirmDialog = () => {
    const { ["action"]: action, ["id"]: id, ...dto } = dialog;

    if (action == "add-account") {
      createAccount({}, dto);
    } else if (action == "edit-account") {
      changeAccount(id, dto);
    }
    setWaiting(true)
  };
  if (getAccountState.loading) {
    return (
      <Container>
        <List>
          <ListItem>
            <ListItemAvatar>
              <Avatar>
                <Skeleton variant="circular" width={40} height={40} />
              </Avatar>
            </ListItemAvatar>
            <Skeleton variant="text" />
          </ListItem>
        </List>
      </Container>
    );
  }
  return (
    <div>
      <Dialog open={open}>
        <DialogTitle>{t(dialog.action)}</DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            margin="dense"
            id="first-name"
            label={t("first-name")}
            fullWidth
            variant="standard"
            value={dialog.name}
            onChange={(e) => {
              setDialogValue("name", e.target.value);
            }}
          />
          <TextField
            autoFocus
            margin="dense"
            id="last-name"
            label={t("last-name")}
            fullWidth
            variant="standard"
            value={dialog.surname}
            onChange={(e) => {
              setDialogValue("surname", e.target.value);
            }}
          />
          <TextField
            autoFocus
            margin="dense"
            id="username"
            label={t("username")}
            fullWidth
            variant="standard"
            value={dialog.username}
            onChange={(e) => {
              setDialogValue("username", e.target.value);
            }}
          />
          <TextField
            autoFocus
            margin="dense"
            id="email"
            label={t("email")}
            type="email"
            fullWidth
            variant="standard"
            value={dialog.email}
            onChange={(e) => {
              setDialogValue("email", e.target.value);
            }}
          />
          <TextField
            autoFocus
            margin="dense"
            id="password"
            label={t("password")}
            type="password"
            fullWidth
            variant="standard"
            value={dialog.password}
            onChange={(e) => {
              setDialogValue("password", e.target.value);
            }}
          />
          {admin && <FormGroup>
            <FormControlLabel
              control={
                <Checkbox
                  checked={dialog.role_flag == 1}
                  onChange={(e) =>
                    setDialogValue("role_flag", e.target.checked ? 1 : 2)
                  }
                />
              }
            // @ts-ignore
              label={t("admin")}
            />
          </FormGroup>
}
        </DialogContent>
        <DialogActions>
          <Button
            color="primary"
            onClick={() => {
              confirmDialog();
              setOpen(false);
            }}
          >
            {t("confirm")}
          </Button>
          <Button color="secondary" onClick={() => setOpen(false)}>
            Close
          </Button>
        </DialogActions>
      </Dialog>
      <PersonList
        {...{
          data: getAccountState.data,
          deleteAction: (id: number)=>{
            deleteAccount(id) 
            setWaiting(true)},
          editAction: openEdit,
          createAction: openNew,
          confirmAction: confirmDialog,
        }}
      />
    </div>
  );
}

export const getServerSideProps = async ({ locale }: { locale: string }) => ({
  props: {
    ...(await serverSideTranslations(locale, ["common", "admin"])),
  },
});

const ManagementPage = () => (
  <AuthPage>
    <Management />
  </AuthPage>
);

ManagementPage.getLayout = function getLayout(page: ReactElement) {
  return <Layout>{page}</Layout>;
};

export default ManagementPage;
