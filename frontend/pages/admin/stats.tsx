import * as React from "react";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import CountByName from "../../components/stats/CountByName"
import CountByTypeName from "../../components/stats/CountByTypeName"
import CountByTypeNameOverTime from "../../components/stats/CountByTypeNameOverTime"
import CountByTypeNameOverWeek from "../../components/stats/CountByTypeNameOverWeek"
import { useOperation } from "react-openapi-client";
import {ReactElement} from "react";
import Layout from "../../components/admin_wrapper/LayoutAdmin";
import {Grid, Typography} from "@mui/material";
import AuthPage from "../../components/AuthPage";
import { useTranslation } from "next-i18next";


export default function Stats() {
  const { t } = useTranslation("admin");
  const stat = useOperation('get_statistics')

  if(stat.loading){
    return (
      <AuthPage>Loading</AuthPage>
    )
  }
  if(stat.error){
    return (
      <AuthPage>Error</AuthPage>
    )
  }

  return (<>
    <Typography variant="h3" marginBottom={4}>{t("Statistics")}</Typography>
    <Grid spacing={5} container>
        <Grid item><CountByName stat={stat.data}/></Grid>
        <Grid item><CountByTypeName stat={stat.data}/></Grid>
        <Grid item><CountByTypeNameOverWeek stat={stat.data}/></Grid>
        <Grid item><CountByTypeNameOverTime stat={stat.data}/></Grid>
    </Grid>
    </>
  );
}

export async function getStaticProps({ locale }: { locale: string }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["admin"])),
    },
  };
}

Stats.getLayout = function getLayout(page: ReactElement) {
    return (
        <Layout>
            {page}
        </Layout>
    )
}
