import { NextPage } from "next";
import * as React from "react";
import BookingDoneComponent from "../components/user/BookingDoneComponent";
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import { useRouter } from "next/router";
export default function BookingConfirmed(){

  const {query} = useRouter()

  return(
    <div>  {//@ts-ignore
      <BookingDoneComponent begintime={query.time} />
}
    </div>
    )
}


export const getServerSideProps = async ({ locale }: {locale: string} ) => ({
  props: {
    ...await serverSideTranslations(locale, ['common']),
  },
})

