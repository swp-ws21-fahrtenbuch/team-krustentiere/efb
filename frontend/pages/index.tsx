import {InferGetStaticPropsType} from "next";
import * as React from "react";
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import HomePC from "../components/user/HomePC";


export default function Home(){

  return(
    <div>
       <HomePC/>
    </div>
  );
};


export async function getStaticProps({ locale }: {locale: string} ) {
  return {
    props: {
      ...await serverSideTranslations(locale, ['common']),
  }}
}

