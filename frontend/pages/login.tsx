
import * as React from "react";
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'

import TextField from "@mui/material/TextField";
import Stack from "@mui/material/Stack";
import { Box, Button, Container, Typography } from "@mui/material";

import { useTranslation } from "next-i18next";
import { OpenAPIContext, useOperationMethod } from "react-openapi-client";
import { useRouter } from "next/router";
import useLocalStorage from "@rehooks/local-storage";

export default function Login() {
    const { t } = useTranslation('common');
    const [username, setUsername] = React.useState("");
    const [password, setPassword] = React.useState("");
    const [sendLogin, sendLoginState] = useOperationMethod("auth_account_login")
    const [authHeaderValue, setAuthHeaderValue] = useLocalStorage("AuthHeaderValue", "")

    const { api } = React.useContext(OpenAPIContext)
    const router = useRouter()

    React.useEffect(() => {
        if (sendLoginState.data==null) {
            return
        }
        const token = sendLoginState.data.access_token
        let config = api.axiosConfigDefaults
        // overwrite Authentication header
        const auth_header = {
            ...config.headers,
            'Authentication': `Bearer ${token}`
        }
        config.headers = auth_header
        setAuthHeaderValue(token)
        router.back()
    },[sendLoginState.data])


    return (
        <Box component="form" onSubmit={(event: { preventDefault: () => void; }) => {
            event.preventDefault()
            sendLogin({}, {username: username, password: password})
        }}>
        <Container maxWidth="sm">
            <Typography variant="h2" component="div" align="center">
                {t("login")}
            </Typography>
        <Stack spacing={2}>
            {sendLoginState.error && sendLoginState.error.message == "Request failed with status code 401" && t("failed login")}
            <p></p>
        <TextField required  
          id="username" 
          label={t("username")}  
          onChange={(e)=>{setUsername(e.target.value)}}
          value={username}/>
        <TextField required 
          id="password" 
          label={t("password")} 
          onChange={(e)=>{setPassword(e.target.value)}}
          type="password"
          value={password}/>
        <Button variant="contained" color="secondary" disabled={sendLoginState.loading} type="submit"> 
            {t("confirm")}
        </Button>        
        </Stack>
        </Container>
        </Box>
    )
}

export async function getStaticProps({ locale }: {locale: string} ) {
    return {
      props: {
        ...await serverSideTranslations(locale, ['common']),
    }}
}