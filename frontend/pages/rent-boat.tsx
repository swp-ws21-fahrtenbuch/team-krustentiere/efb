import * as React from "react";
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import { useRouter } from "next/router";
import RentBoatComponent from "../components/user/RentBoatComponent";


export default function RentBoat() {
  const {query} = useRouter()
  return (
    <div>
      {//@ts-ignore
      <RentBoatComponent disciplineId={parseInt(query.id)}/>
      }
    </div>
  );
};


export async function getStaticProps({ locale }: {locale: string} ) {
  return {
    props: {
      ...await serverSideTranslations(locale, ['common']),
  }}
}


