import { NextPage } from "next";
import * as React from "react";
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import ReportDamageComponent from "../../components/user/ReportDamageComponent";
import {useRouter} from "next/router";


const ReportDamage: NextPage = () => {
  const router = useRouter()
  const {id} = router.query
  return(
    <div>
      {
        //@ts-ignore
        <ReportDamageComponent id={id}/>
      }
      
    </div>
    )
}


export const getServerSideProps = async ({ locale }: {locale: string} ) => ({
  props: {
    ...await serverSideTranslations(locale, ['common']),
  },
})

export default ReportDamage