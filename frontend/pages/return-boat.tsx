import * as React from "react";
import {serverSideTranslations} from 'next-i18next/serverSideTranslations'
import {useRouter} from 'next/router';
import ReturnBoatComponent from "../components/user/ReturnBoatComponent";

export default function ReturnBoat() {
  const {query} = useRouter()
  return (
    <div>
      {//@ts-ignore
      <ReturnBoatComponent disciplineId={parseInt(query.id)}/>
      }
    </div>
  );
};

export async function getStaticProps({ locale }: {locale: string} ) {
  return {
    props: {
      ...await serverSideTranslations(locale, ['common'])
    }
  }
}