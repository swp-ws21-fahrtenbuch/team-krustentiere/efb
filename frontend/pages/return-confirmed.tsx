import { NextPage } from "next";
import * as React from "react";
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import ReturnConfirmedComponent from "../components/user/ReturnConfirmedComponent";


export default function ReturnConfirmed(){
  return(
    <div>
      <ReturnConfirmedComponent/>
    </div>
    )
}


export const getStaticProps = async ({ locale }: {locale: string} ) => ({
  props: {
    ...await serverSideTranslations(locale, ['common']),
  },
})

