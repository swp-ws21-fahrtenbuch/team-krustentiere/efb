import * as React from "react";
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import DisciplinesOnReturn from "../components/user/DisciplinesOnReturn";

export default function SelectDiscipline() {
  return (
    <div> <DisciplinesOnReturn/> </div>
  );
};

export async function getStaticProps({ locale }: {locale: string} ) {
  return {
    props: {
      ...await serverSideTranslations(locale, ['common']),
  }}
}