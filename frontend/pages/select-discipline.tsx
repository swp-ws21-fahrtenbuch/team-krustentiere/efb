import * as React from "react";
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import SelectDisciplineComponent from "../components/user/SelectDisciplineComponent";

export default function SelectDiscipline() {

    
  return (
    <div>
      <SelectDisciplineComponent />
    </div>
  );
};

export async function getStaticProps({ locale }: {locale: string} ) {
  return {
    props: {
      ...await serverSideTranslations(locale, ['common']),
  }}
}
