import random

dis = ["Windsurfen", "Stand-Up-Paddling", "Segeln", "Rudern", "Kanu", "Kanupolo"]

boote = [  "Maxi2000", "Maxi1", "Der Erste", "GummiPaddel", "Der einsame Windfang", "RadialSystem", "Rudi2", "FiddelKatzo"]

for i in range(100):
    month = random.randint(1,12)
    day = random.randint(1,27)
    random_char = random.randint(100,1000)
    boot = boote[i % len(boote) -1 ]
    discipline = dis[i % len(dis) -1]
    print(f"INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT \'2022-{month}-{day} 04:00:01+01\',\'2022-{month}-{day} 05:00:01+01\', \'{random_char}\', (SELECT id FROM boat WHERE name = \'{boot}\'), (SELECT id FROM discipline WHERE name = \'{discipline}\') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = \'{random_char}\'));\n")



# keine zukünftigen Ausleihen

for i in range(12):
    month = 1
    day = random.randint(1,31)
    random_char = random.randint(100,1000)
    boot = boote[i % len(boote) -1 ]
    discipline = dis[i % len(dis) -1]
    print(f"INSERT INTO borrow_entry(begintime, endtime, destination, boat_id, discipline_id) (SELECT \'2022-{month}-{day} 04:00:01+01\',\'2022-{month}-{day} 05:00:01+01\', \'{random_char}\', (SELECT id FROM boat WHERE name = \'{boot}\'), (SELECT id FROM discipline WHERE name = \'{discipline}\') WHERE NOT EXISTS (SELECT id FROM borrow_entry WHERE destination = \'{random_char}\'));\n")




